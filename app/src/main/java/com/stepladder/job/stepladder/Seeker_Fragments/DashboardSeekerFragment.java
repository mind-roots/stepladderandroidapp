package com.stepladder.job.stepladder.Seeker_Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.ActivityTransition.TransitionHelper;
import com.stepladder.job.stepladder.Adapters.SwipeAdapter;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Expand;
import com.stepladder.job.stepladder.Swipe.FlingCardListener;
import com.stepladder.job.stepladder.Swipe.SwipeFlingAdapterView;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class DashboardSeekerFragment extends Fragment implements FlingCardListener.ActionDownInterface {
    View rootView;
    public static TextView txt_limit, textview2;
    private SwipeFlingAdapterView flingContainer;
    SwipeAdapter adapter;
    LinearLayout ll_swipe;
    public static ImageView left, right, expand_activities_button, img_limit;
    public static RelativeLayout rl_limit, rl_cards;
    int listsize;
    ProgressBar progressBar;
    SharedPreferences prefs;
    String company_name, job_title, salary, job_length, area, skills, desc, image, job_id, set_status, status;
    String pound = "\u00a3";
    public static boolean accept = false, reject = false;
    RestClient client;
    public static int width, height;
    ConnectionDetector con;
    Tracker t;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.dashboard_seeker_fragment, container, false);

        init();

        DashBoard_Seeker.isHome = true;
        //DashBoard_Seeker.open = true;

        Log.i("List Size: ", "" + Utils.get_job.size());
        if (Utils.get_job.size() > 0) {
            rl_cards.setVisibility(View.VISIBLE);
            rl_limit.setVisibility(View.GONE);
            adapter = new SwipeAdapter(getActivity(), Utils.get_job);
            flingContainer.setAdapter(adapter);
            listsize = Utils.get_job.size();
        } else {
            rl_limit.setVisibility(View.VISIBLE);
            rl_cards.setVisibility(View.GONE);
            if (DashBoard_Seeker.quota == DashBoard_Seeker.current_jobs) {
                txt_limit.setText("You have reached limit of your applications that you can make today");
                img_limit.setImageResource(R.drawable.ic_calander);
            } else {

            }
        }

        if (DashBoard_Seeker.match_jobs == 0) {
            DashBoard_Seeker.ic_noti_grey.setVisibility(View.VISIBLE);
            DashBoard_Seeker.ll_badge.setVisibility(View.GONE);
        } else {
            DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
            DashBoard_Seeker.ll_badge.setVisibility(View.VISIBLE);
        }

        clickevents();

        return rootView;
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectLeft();
                Log.i("Size", "::" + listsize);

            }
        });

        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    left.setBackgroundResource(R.drawable.ic_cross_red);
                }

                return false;
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectRight();
                Log.i("Size", "::" + listsize);

            }
        });

        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    right.setBackgroundResource(R.drawable.ic_tick_green);
                }
                return false;
            }
        });

        expand_activities_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("getTop: ", "" + flingContainer.getTop());
                job_id = Utils.get_job.get(flingContainer.getTop()).job_id;
                image = Utils.get_job.get(flingContainer.getTop()).job_image;
                company_name = Utils.get_job.get(flingContainer.getTop()).company_name;
                job_title = Utils.get_job.get(flingContainer.getTop()).job_title;
                if (Utils.get_job.get(flingContainer.getTop()).minamount.equals("")) {
                    if (Utils.get_job.get(flingContainer.getTop()).job_type.equals("Per Annum")) {
                        salary = "Not disclosed (Yearly)";
                    } else if (Utils.get_job.get(flingContainer.getTop()).job_type.equals("Hourly")) {
                        salary = "Not disclosed (Hourly)";
                    }
                } else {
                    if (Utils.get_job.get(flingContainer.getTop()).maxamount.equals("")) {
                        salary = priceformat(Utils.get_job.get(flingContainer.getTop()).minamount) + " per hour";
                    } else {
                        salary = priceformat(Utils.get_job.get(flingContainer.getTop()).minamount) + " - " + priceformat(Utils.get_job.get(0).maxamount) + " per year";
                    }
                }
                job_length = Utils.get_job.get(flingContainer.getTop()).job_length;
                area = Utils.get_job.get(flingContainer.getTop()).location;
                skills = Utils.get_job.get(flingContainer.getTop()).skills;
                desc = Utils.get_job.get(flingContainer.getTop()).job_desc;

                Intent in = new Intent(getActivity(), Expand.class);
                in.putExtra("image", image);
                in.putExtra("company_name", company_name);
                in.putExtra("job_title", job_title);
                in.putExtra("salary", salary);
                in.putExtra("job_length", job_length);
                in.putExtra("area", area);
                in.putExtra("skills", skills);
                in.putExtra("desc", desc);

                transitionTo(in);
            }
        });

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                job_id = Utils.get_job.get(flingContainer.getTop()).job_id;
                Utils.get_job.remove(flingContainer.getTop());
                adapter.notifyDataSetChanged();
                left();
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                job_id = Utils.get_job.get(flingContainer.getTop()).job_id;
                Utils.get_job.remove(flingContainer.getTop());
                adapter.notifyDataSetChanged();
                right();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                if (scrollProgressPercent < 0) {
                    left.setBackgroundResource(R.drawable.ic_cross_red);
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                } else {
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                }
                if (scrollProgressPercent > 0) {
                    right.setBackgroundResource(R.drawable.ic_tick_green);
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                } else {
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                }
            }
        });

    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        t = ((OneSignalClass) getActivity().getApplication()).getDefaultTracker();
        con = new ConnectionDetector(getActivity());
        //Getting screen resolution
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        flingContainer = (SwipeFlingAdapterView) rootView.findViewById(R.id.frame);
        left = (ImageView) rootView.findViewById(R.id.left);
        right = (ImageView) rootView.findViewById(R.id.right);
        expand_activities_button = (ImageView) rootView.findViewById(R.id.expand_activities_button);
        rl_limit = (RelativeLayout) rootView.findViewById(R.id.rl_limit);
        rl_cards = (RelativeLayout) rootView.findViewById(R.id.rl_cards);
        ll_swipe = (LinearLayout) rootView.findViewById(R.id.ll_swipe);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        txt_limit = (TextView) rootView.findViewById(R.id.txt_limit);
        textview2 = (TextView) rootView.findViewById(R.id.textview2);
        img_limit = (ImageView) rootView.findViewById(R.id.img_limit);
        txt_limit.setTypeface(Utils.setfontlight(getActivity()));
        textview2.setTypeface(Utils.setfontlight(getActivity()));
    }

    @Override
    public void onActionDownPerform() {

    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            //amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }


    //*************************************Async Task Class*****************************************
    class SetStatus extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String response = JobStatus(prefs.getString("auth_code", null), job_id, set_status);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (status.equalsIgnoreCase("true")) {
            } else {

            }
        }
    }

    //***************************************Location Method****************************************
    public String JobStatus(String auth_code, String JobId, String Applier_Status) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("JobId", JobId)
                .appendQueryParameter("Applier_Status", Applier_Status);
        Log.i("Complete", "Url: " + Utils.apply_job + builder);
        client = new RestClient(Utils.apply_job + builder);
        String response = null;
        response = client.executePost();
        return response;
    }

    //**********************************Activity OnResume Method************************************

    @Override
    public void onResume() {
        super.onResume();

        final Handler handler = new Handler();

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    public void run() {
                        if (accept) {
                            flingContainer.getTopCardListener().selectRight();
                            accept = false;
                        } else if (reject) {
                            flingContainer.getTopCardListener().selectLeft();
                            reject = false;
                        }
                    }
                });
            }
        }).start();
    }

    public void left() {

        //Track event on google analytics
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Job Seeker")
                .setAction("Swipe Job Rejected")
                .setLabel("Swipe Job Rejected")
                .build());

        set_status = "not interested";
        DashboardSeekerFragment.right.setBackgroundResource(R.drawable.ic_tick_grey);
        DashboardSeekerFragment.left.setBackgroundResource(R.drawable.ic_cross_grey);
        new SetStatus().execute();
        listsize = Utils.get_job.size();
        Log.i("LLL:", "" + listsize);
        if (listsize == 0) {
            DashboardSeekerFragment.rl_limit.setVisibility(View.VISIBLE);
            DashboardSeekerFragment.rl_cards.setVisibility(View.GONE);
            if (DashBoard_Seeker.quota > listsize) {
                DashboardSeekerFragment.txt_limit.setText("You have swiped through all available jobs in your area today");
                DashboardSeekerFragment.img_limit.setImageResource(R.drawable.ic_thumb);
            } else if (DashBoard_Seeker.quota == DashBoard_Seeker.current_jobs) {
                DashboardSeekerFragment.txt_limit.setText("You have reached limit of your applications that you can make today");
                DashboardSeekerFragment.img_limit.setImageResource(R.drawable.ic_calander);
            } else {

            }
        }
    }

    public void right() {

        //Track event on google analytics
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Job Seeker")
                .setAction("Swipe Job Accepted")
                .setLabel("Swipe Job Accepted")
                .build());

        set_status = "interested";
        DashboardSeekerFragment.right.setBackgroundResource(R.drawable.ic_tick_grey);
        DashboardSeekerFragment.left.setBackgroundResource(R.drawable.ic_cross_grey);
        new SetStatus().execute();
        listsize = Utils.get_job.size();
        Log.i("LLR", "" + listsize);
        if (listsize == 0) {
            DashboardSeekerFragment.rl_limit.setVisibility(View.VISIBLE);
            DashboardSeekerFragment.rl_cards.setVisibility(View.GONE);
            if (DashBoard_Seeker.quota > listsize) {
                DashboardSeekerFragment.txt_limit.setText("You have swiped through all available jobs in your area today");
                DashboardSeekerFragment.img_limit.setImageResource(R.drawable.ic_thumb);
            } else if (DashBoard_Seeker.quota == DashBoard_Seeker.current_jobs) {
                DashboardSeekerFragment.txt_limit.setText("You have reached limit of your applications that you can make today");
                DashboardSeekerFragment.img_limit.setImageResource(R.drawable.ic_calander);
            } else {

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        t.setScreenName("Edit Employee Profile");
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }


    //***************************************transitionTo Method************************************
    @SuppressLint("NewApi")
    protected void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }
}

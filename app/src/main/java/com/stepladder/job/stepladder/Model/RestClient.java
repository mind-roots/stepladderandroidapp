package com.stepladder.job.stepladder.Model;

import android.util.Log;

import com.stepladder.job.stepladder.Seeker.Expand;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by user on 10/31/2015.
 */
public class RestClient {
    String url;
    URI uri;
    public static int res ;
    public static HttpPost httpPost;
    String result;

    public RestClient(String s) {

        url = s;
    }

    public String executeGet() {
        BufferedReader in = null;
        try {
            System.out.println("yrl=" + url);
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            //	HttpClient httpClient = new DefaultHttpClient();
            uri = new URI(url.replace(" ", "%20"));
            HttpGet httpget = new HttpGet(uri);


            HttpResponse response = httpClient.execute(httpget);
            res = response.getStatusLine().getStatusCode();

            in = new BufferedReader(new InputStreamReader(response.getEntity()
                    .getContent()));

            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            result = sb.toString();
            System.out.println("result=" + result);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return result;
    }
    public String postimage(String img_path) {
        BufferedReader in = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            //	HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            uri = new URI(url.replace(" ", "%20"));
            HttpPost httpPost = new HttpPost(uri);
            try {
                MultipartEntity entity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);

                entity.addPart("profile_image", new FileBody(new File(img_path)));

                httpPost.setEntity(entity);

                HttpResponse response = httpClient.execute(httpPost,
                        localContext);
                res = response.getStatusLine().getStatusCode();
                System.out.println("image res htp=" + res);
                in = new BufferedReader(new InputStreamReader(response.getEntity()
                        .getContent()));

                StringBuffer sb = new StringBuffer("");
                String l = "";
                String nl = System.getProperty("line.separator");
                while ((l = in.readLine()) != null) {
                    sb.append(l + nl);
                }
                in.close();
                result = sb.toString();
                System.out.println("result=" + result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
        return result;
    }
    public String postcompanyimage(String img_path) {
        BufferedReader in = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            //	HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            uri = new URI(url.replace(" ", "%20"));
            HttpPost httpPost = new HttpPost(uri);
            try {
                MultipartEntity entity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);

                entity.addPart("Image", new FileBody(new File(img_path)));

                httpPost.setEntity(entity);

                HttpResponse response = httpClient.execute(httpPost,
                        localContext);
                res = response.getStatusLine().getStatusCode();
                System.out.println("image res htp=" + res);
                in = new BufferedReader(new InputStreamReader(response.getEntity()
                        .getContent()));

                StringBuffer sb = new StringBuffer("");
                String l = "";
                String nl = System.getProperty("line.separator");
                while ((l = in.readLine()) != null) {
                    sb.append(l + nl);
                }
                in.close();
                result = sb.toString();
                System.out.println("result=" + result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
        return result;
    }
    public String executePost() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            //	HttpClient httpClient = new DefaultHttpClient();
            uri = new URI(url.replace(" ", "%20"));
            httpPost = new HttpPost(uri);
            HttpResponse response = null;
            response = httpClient.execute(httpPost);
            res = response.getStatusLine().getStatusCode();
            System.out.println("res htp=" + res);
            HttpEntity entity1 = response.getEntity();
            result = EntityUtils.toString(entity1);
            return result;
            // Toast.makeText(MainPage.this, result, Toast.LENGTH_LONG).show();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return result;

    }

    public String executePutMethod() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            //	HttpClient httpclient = new DefaultHttpClient();
            uri = new URI(url.replace(" ", "%20"));
            HttpPut httpPUT = new HttpPut(uri);

            HttpResponse httpResponse = httpClient.execute(httpPUT);
            res = httpResponse.getStatusLine().getStatusCode();
            System.out.println("put res htp=" + res);
            HttpEntity entity1 = httpResponse.getEntity();
            result = EntityUtils.toString(entity1);
            return result;
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
}

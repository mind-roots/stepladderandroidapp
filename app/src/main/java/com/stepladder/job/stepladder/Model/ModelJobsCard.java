package com.stepladder.job.stepladder.Model;

import android.graphics.Bitmap;

/**
 * Created by user on 12/8/2015.
 */
public class ModelJobsCard {
    public String job_id;
    public String job_title;
    public String job_desc;
    public String skills;
    public String job_length;
    public String job_type;
    public String minamount;
    public String maxamount;
    public String location;
    public String sector;
    public String lat;
    public String lon;
    public String job_status;
    public String job_image;
    public String company_name;
    public String firstname;
    public String lastname;
    public String phone;
    public String profile_image;
    public Bitmap image;

}

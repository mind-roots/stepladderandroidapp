package com.stepladder.job.stepladder.Seeker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.ActivityTransition.TransitionHelper;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.CropActivityP;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.ModelUIcomponents;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundImageShadow;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step1Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

public class Confirm_Profile extends ActionBarActivity {
    LinearLayout lay_confirm_profile, ll_past_jobs, ll_past_line;
    RelativeLayout lay_seemore_btn;
    ImageView img_confirm_profile;
    LayoutInflater inflater;
    TextView txt_name, txt_address, txt_seemore, title, txt_skills, txt_bio, title_skills, title_bio, title_past;
    public static TextView btn_txt;
    ProgressBar progress;
    ImageView image_pick;
    String current_role, profile_image,confirmed;
    public static String status, edit,response;
    SharedPreferences prefs;
    Menu newmenu;
    public static boolean change_image = false;
    ConnectionDetector con;
    ProgressBar progressBar;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_profile);

        //Get Intent
        Intent intent = getIntent();
        edit = intent.getStringExtra("edit");

        init();

        if (edit.equals("true")) {
            lay_confirm_profile.setVisibility(View.INVISIBLE);
            btn_txt.setText("Update Profile");
            DashBoard_Seeker.open = true;
        }

        setFont();

        //Set Profile Image
        if (MainActivity.profile != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(MainActivity.profile).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        } else if (Utils.pimageUri != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        } else if (prefs.getString("profile_image", null) != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        }

        //Set Prefs Values
        if (prefs.getString("first_name", null) != null && prefs.getString("last_name", null) != null) {
            txt_name.setText(prefs.getString("first_name", null) + " " + prefs.getString("last_name", null));
        }
        if (prefs.getString("area", null) != null) {
            txt_address.setText(prefs.getString("area", null));
        }
        if (prefs.getString("skills", null) != null) {
            txt_skills.setText(prefs.getString("skills", null));
        }
        if (prefs.getString("bio", null) != null) {
            txt_bio.setText(prefs.getString("bio", null));
        }

        //Inflate Past Jobs
        if (Utils.final_ui_data.size() != 0) {
            title_past.setVisibility(View.VISIBLE);
            ll_past_line.setVisibility(View.VISIBLE);
            for (int i = 0; i < Utils.final_ui_data.size(); i++) {
                Log.i("Current Position", "" + Utils.final_ui_data.get(i).CurrentPosition);
                if (Utils.final_ui_data.get(i).CurrentPosition.equals("1")) {
                    current_role = "Current Role Since: "+Utils.final_ui_data.get(i).StartDate;
                    add_view(Utils.final_ui_data.get(i).JobTitle, Utils.final_ui_data.get(i).Company
                            , current_role, Utils.final_ui_data.get(i).City);
                } else if (Utils.final_ui_data.get(i).CurrentPosition.equals("0")) {
                    current_role = Utils.final_ui_data.get(i).StartDate + " - " + Utils.final_ui_data.get(i).EndDate;
                    add_view(Utils.final_ui_data.get(i).JobTitle, Utils.final_ui_data.get(i).Company
                            , current_role, Utils.final_ui_data.get(i).City);
                }

            }

        } else {
            title_past.setVisibility(View.INVISIBLE);
            ll_past_line.setVisibility(View.INVISIBLE);
        }

        clickevents();

    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Confirm Profile");

        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        progress = (ProgressBar) findViewById(R.id.progress);
        lay_confirm_profile = (LinearLayout) findViewById(R.id.lay_confirm_profile);
        lay_seemore_btn = (RelativeLayout) findViewById(R.id.lay_seemore_btn);
        image_pick = (ImageView) findViewById(R.id.image_pick);
        img_confirm_profile = (ImageView) findViewById(R.id.img_confirm_profile);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_address = (TextView) findViewById(R.id.txt_address);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        txt_seemore = (TextView) findViewById(R.id.txt_seemore);
        ll_past_jobs = (LinearLayout) findViewById(R.id.ll_past_jobs);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        txt_bio = (TextView) findViewById(R.id.txt_bio);
        title_past = (TextView) findViewById(R.id.title_past);
        title_bio = (TextView) findViewById(R.id.title_bio);
        title_skills = (TextView) findViewById(R.id.title_skills);
        ll_past_line = (LinearLayout) findViewById(R.id.ll_past_line);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Seeker Confirm Profile");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Confirm_Profile.this), Typeface.BOLD);
        txt_name.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_address.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_seemore.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        btn_txt.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        title_bio.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        title_skills.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        title_past.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_skills.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_bio.setTypeface(Utils.setfontlight(Confirm_Profile.this));

    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        image_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        img_confirm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        lay_confirm_profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                if (change_image) {

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Confirm_Profile.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new Change_Image().execute();
                    }
            }
        });

        lay_seemore_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Confirm_Profile.this, See_More_Seeker.class);
                intent.putExtra("edit", "false");
                ActivityStack.activity.add(Confirm_Profile.this);
                transitionTo(intent);
            }
        });
    }

    //****************************************Transition Methods************************************
    @SuppressLint("NewApi")
    protected void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);

        startActivity(i, transitionActivityOptions.toBundle());
    }

    //***************************************Add View Method****************************************
    public void add_view(String job_title, String company, String current, String location) {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.past_job_info, null);
        final TextView txt_job_title = (TextView) view.findViewById(R.id.txt_job_title);
        final TextView txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);
        final TextView txt_current_role = (TextView) view.findViewById(R.id.txt_current_role);
        final TextView txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_job_title.setTypeface(Utils.setfontlight(Confirm_Profile.this), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_current_role.setTypeface(Utils.setfontlight(Confirm_Profile.this));
        txt_location.setTypeface(Utils.setfontlight(Confirm_Profile.this));

        final ModelUIPastJobs UIModel = new ModelUIPastJobs();
        UIModel.txt_job_title = txt_job_title;
        UIModel.txt_company_name = txt_company_name;
        UIModel.txt_current_role = txt_current_role;
        UIModel.txt_location = txt_location;

        UIModel.txt_job_title.setText(job_title);
        UIModel.txt_company_name.setText(company);
        UIModel.txt_current_role.setText(current);
        UIModel.txt_location.setText(location);

        ll_past_jobs.addView(view);

    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(true);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_expand) {
            Intent intent = new Intent(Confirm_Profile.this, See_More_Seeker.class);
            intent.putExtra("edit", "false");
            ActivityStack.activity.add(Confirm_Profile.this);
            transitionTo(intent);
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.pimageUri != null) {
            lay_confirm_profile.setVisibility(View.VISIBLE);
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        }
    }

    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Profile_photo.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Profile_photo.selected_image);
            Intent in = new Intent(Confirm_Profile.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            MainActivity.profile = null;
            change_image = true;
            lay_confirm_profile.setVisibility(View.VISIBLE);
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    // **************************************Change Profile Photo***********************************
    class Change_Image extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.confirm_profile + "AuthCode=" + prefs.getString("auth_code", null));
           // RestClient client = new RestClient(Utils.confirm_profile);
            if(change_image){
                Log.i("Image", "Path:" + Utils.pimageUri);
                String image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
                response = client.postimage(image_path);
                change_image = false;
            }else {
                response = client.executePost();
            }

            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.optString("profile_image");
                confirmed = data.optString("Confirmed");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {
                if (edit.equals("true")) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("type", "job-seeker");
                    editor.putString("profile_image", profile_image);
                    editor.putString("confirmed",confirmed);
                    editor.commit();
                    dialog("Success!", "Your employee profile has been updated successfully");
                    edit = "false";
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("type", "job-seeker");
                    editor.putString("profile_image", profile_image);
                    editor.putString("confirmed",confirmed);
                    editor.commit();
                    Intent i = new Intent(Confirm_Profile.this, Step1Seeker.class);
                    i.putExtra("parse", "false");
                    ActivityStack.activity.add(Confirm_Profile.this);
                    startActivity(i);
                }
            } else {
                dialog("Alert!", "Error in uploading image!");
            }
        }
    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(Confirm_Profile.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(Confirm_Profile.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(Confirm_Profile.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

package com.stepladder.job.stepladder.Seeker;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.SwipeAdapter;
import com.stepladder.job.stepladder.Employeer_Fragments.PostJobFragment;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker_Fragments.ContactUsFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.DashboardSeekerFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.EditEmployeeProfileFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.NotificationFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.SettingsFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.ShareStepLadderFragment;
import com.stepladder.job.stepladder.Seeker_Fragments.ViewMatchesFragment;
import com.stepladder.job.stepladder.SlideInSeeker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class DashBoard_Seeker extends ActionBarActivity {
    ActionBar actionBar;
    View actionview;
    public static ImageView ic_drawer, ic_noti_grey, ic_add_job;
    public static TextView action_title, counter;
    Fragment fragment;
    String status, email_noti, app_noti, parse;
    public static int quota, match_jobs, current_jobs;
    public static Menu newmenu;
    RestClient client;
    public static RelativeLayout ll_badge;
    public static FragmentManager fragmentManager;
    public static Bitmap job_image;
    public static boolean isHome = false;
    SharedPreferences prefs;
    ConnectionDetector con;
    public static ProgressDialog dia;
    public static boolean open = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_seeker);

        ActionBar();

        init();


        Intent intent = getIntent();
        parse = intent.getStringExtra("parse");

        if (parse.equals("true")) {
            newmenu.findItem(R.id.action_update).setVisible(false);
            action_title.setText("Matches");
            fragment = new ViewMatchesFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            parse = "false";
        } else {

        }


        clickevents();
    }

    private void init() {
        con = new ConnectionDetector(DashBoard_Seeker.this);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
    }

    private void clickevents() {
        // TODO Auto-generated method stub
        ic_drawer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(DashBoard_Seeker.this, SlideInSeeker.class);
                startActivityForResult(in, 1);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });

        ll_badge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Matches");
                fragment = new ViewMatchesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            }
        });

        ic_noti_grey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Matches");
                fragment = new ViewMatchesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            }
        });

    }

    private void ActionBar() {
        // TODO Auto-generated method stub
        dia = new ProgressDialog(DashBoard_Seeker.this);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionview = inflater.inflate(R.layout.action_bar, null);
        ic_drawer = (ImageView) actionview.findViewById(R.id.ic_drawer);
        ic_add_job = (ImageView) actionview.findViewById(R.id.ic_add_job);
        ic_noti_grey = (ImageView) actionview.findViewById(R.id.ic_noti_grey);
        action_title = (TextView) actionview.findViewById(R.id.action_title);
        ll_badge = (RelativeLayout) actionview.findViewById(R.id.ll_badge);
        counter = (TextView) actionview.findViewById(R.id.counter);
        action_title.setTypeface(Utils.setfontlight(DashBoard_Seeker.this), Typeface.BOLD);
        counter.setTypeface(Utils.setfontlight(DashBoard_Seeker.this));
        ic_add_job.setVisibility(View.GONE);
        ic_noti_grey.setVisibility(View.VISIBLE);
        actionBar.setCustomView(actionview,
                new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String fragment_name = data.getStringExtra("fragment_name");
            System.out.println("fragment_name: " + fragment_name);

            if (fragment_name.equals("view_jobs")) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Matches");
                fragment = new ViewMatchesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("notification")) {
                newmenu.findItem(R.id.action_update).setVisible(true);
                action_title.setText("Notifications");
                fragment = new NotificationFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("edit_profile")) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Edit Employee Profile");
                fragment = new EditEmployeeProfileFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("settings")) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Settings");
                fragment = new SettingsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("share_ladder")) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Step Ladder App");
                String sAux = "\nCheck out the Step Ladder App. It is like Tinder, but for jobs! Download it for your mobile here: https://stepladderapp.com/";
                //  sAux = sAux + coupon_url;
                sharingIntent.putExtra(Intent.EXTRA_TEXT, sAux);
                //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"Check out the Step Ladder App. It is like Tinder, but for jobs! Download it for your mobile here: http://onelink.to/xv8we3");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else if (fragment_name.equals("contact_us")) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                startActivity(Intent.createChooser(emailIntent, "Contact us via"));
            } else if (fragment_name.equals("home")) {
                open = false;
                fragment = new PostJobFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
//                if (!con.isConnectingToInternet()) {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(
//                            DashBoard_Seeker.this);
//
//                    alert.setTitle("Internet connection unavailable.");
//                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
//                    alert.setPositiveButton("OK",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int whichButton) {
//                                    startActivity(new Intent(
//                                            Settings.ACTION_WIRELESS_SETTINGS));
//                                }
//                            });
//
//                    alert.show();
//                } else {
//                    //Get Jobs Cards
//                    new getJobs().execute();
//                }
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Step Ladder");
            }

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(false);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        newmenu.findItem(R.id.action_done).setVisible(false);
        newmenu.findItem(R.id.action_update).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_update) {
            if (!con.isConnectingToInternet()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        DashBoard_Seeker.this);

                alert.setTitle("Internet connection unavailable.");
                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                alert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                alert.show();
            } else {
                new update_noti().execute();
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (open) {
            open = false;
        } else {
            if (!con.isConnectingToInternet()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        DashBoard_Seeker.this);

                alert.setTitle("Internet connection unavailable.");
                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                alert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                alert.show();
            } else {
                //Get Jobs Cards
                new getJobs().execute();
            }
        }

        try {
            ContentValues cv = new ContentValues();
            cv.put("badgecount", 0);
            getContentResolver().update(Uri.parse("content://com.sec.badge/apps"), cv, "package=?", new String[]{getPackageName()});

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notifManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //*************************************Async Task Class*****************************************
    class update_noti extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(DashBoard_Seeker.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating notification....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values: ", "Authcode: " + prefs.getString("auth_code", null)
                    + "Email: " + NotificationFragment.email_noti + "App: " + NotificationFragment.app_noti);
            String response = update(prefs.getString("auth_code", null), NotificationFragment.email_noti, NotificationFragment.app_noti);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = obj.getJSONObject("data");
                    email_noti = data.getString("Match_Email_Notification");
                    app_noti = data.getString("Match_App_Notification");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("email_noti", email_noti);
                editor.putString("app_noti", app_noti);
                editor.commit();
            } else {
                dialog("Alert!", "Couldn't update notification");
            }
        }
    }

    class getJobs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
            Utils.get_job.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.get_jobs + "AuthCode=" + prefs.getString("auth_code", null));
            String response = client.executeGet();
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                Log.i("Status: ", status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = obj.optJSONObject("data");
                    quota = data.optInt("quota");
                    match_jobs = data.optInt("matchjobs");
                    JSONArray jobs = data.optJSONArray("jobs");
                    Log.i("jobs array length: ", "" + jobs.length());
                    if (jobs != null) {
                        current_jobs = jobs.length();
                        for (int i = 0; i < jobs.length(); i++) {
                            JSONObject job_detail = jobs.getJSONObject(i);
                            ModelJobsCard cards = new ModelJobsCard();
                            cards.job_id = job_detail.getString("JobId");
                            cards.job_title = job_detail.getString("JobTitle");
                            cards.job_desc = job_detail.getString("JobDescription");
                            cards.skills = job_detail.getString("JobSkills");
                            cards.job_length = job_detail.getString("JobDuration");
                            cards.job_type = job_detail.getString("Typed");
                            cards.minamount = job_detail.getString("MinAmount");
                            cards.maxamount = job_detail.getString("MaxAmount");
                            cards.job_status = job_detail.getString("Status");
                            cards.job_image = job_detail.getString("Image");
                            Log.i("Image: ", "" + cards.job_image);
                            URL url = new URL(job_detail.getString("Image"));
                            cards.image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            JSONObject companyInfo = job_detail.getJSONObject("CompanyInfo");
                            cards.company_name = companyInfo.getString("CompanyName");
                            cards.firstname = companyInfo.getString("FirstName");
                            cards.lastname = companyInfo.getString("LastName");
                            cards.phone = companyInfo.getString("Phone_number");
                            cards.profile_image = companyInfo.getString("profile_image");
                            cards.sector = companyInfo.getString("Sector");
                            cards.location = companyInfo.getString("LocationName");
                            cards.lat = companyInfo.getString("Lat");
                            cards.lon = companyInfo.getString("Long");
                            Utils.get_job.add(cards);
                        }
                        Log.i("List Size kkkkk: ", "" + Utils.get_job.size());
                    } else {
                        current_jobs = 0;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equals("true")) {
                fragment = new DashboardSeekerFragment();
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                Log.i("Match Jobs: ", "" + match_jobs);
                if (match_jobs == 0) {
                    ll_badge.setVisibility(View.GONE);
                    ic_noti_grey.setVisibility(View.VISIBLE);
                } else {
                    ll_badge.setVisibility(View.VISIBLE);
                    counter.setText(String.valueOf(match_jobs));
                    ic_noti_grey.setVisibility(View.GONE);
                }
            } else {
                dialog("Alert!", "Authcode Mismatch");
            }
        }
    }

    //***********************************Update Method**********************************************
    public String update(String authcode, String email_noti, String app_noti) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", authcode)
                .appendQueryParameter("Match_Email_Notification", email_noti)
                .appendQueryParameter("Match_App_Notification", app_noti);
        Log.i("Complete", "Url: " + Utils.update_notification + builder);
        client = new RestClient(Utils.update_notification + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }


    @Override
    public void onBackPressed() {
        exitdialog();
    }

    //**********************Back Dialog********************
    private void exitdialog() {
        // TODO Auto-generated method stub

        AlertDialog.Builder alert = new AlertDialog.Builder(DashBoard_Seeker.this);
        alert.setTitle("Alert!");
        alert.setMessage("Are you sure you want to exit from this app?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                System.exit(0);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        alert.show();

    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, final String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                if (msg.equals("Authcode Mismatch")) {
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.commit();
                                    Utils.cimageUri = null;
                                    MainActivity.profile = null;
                                    for (int i = 0; i < ActivityStack.activity
                                            .size(); i++) {
                                        (ActivityStack.activity.elementAt(i))
                                                .finish();
                                        Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                                    }
                                    Intent in = new Intent(DashBoard_Seeker.this, MainActivity.class);
                                    startActivity(in);
                                    finish();
                                } else {
                                    dialog.dismiss();
                                }
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }
}

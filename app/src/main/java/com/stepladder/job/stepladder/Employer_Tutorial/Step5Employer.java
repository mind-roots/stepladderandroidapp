package com.stepladder.job.stepladder.Employer_Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 1/18/2016.
 */
public class Step5Employer extends Activity implements Animation.AnimationListener{
    RelativeLayout rl_got_it;
    TextView txt;
    Animation animZoomIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step5employer);

        rl_got_it = (RelativeLayout)findViewById(R.id.rl_got_it);
        txt = (TextView)findViewById(R.id.txt);
        txt.setTypeface(Utils.setfontlight(Step5Employer.this));

        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_in);
        animZoomIn.setAnimationListener(this);
        rl_got_it.startAnimation(animZoomIn);

        rl_got_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Step1Employer.replay.equals("yes")){
                    finish();
                    Step1Employer.replay = "no";
                }else {
                    Intent i = new Intent(Step5Employer.this,
                            DashBoard_Employeer.class);
                    startActivity(i);
                    finish();
                }
            }
        });

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

package com.stepladder.job.stepladder;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Utils.Utils;

import java.io.PrintWriter;
import java.io.StringWriter;


public class ParseApplication extends MultiDexApplication {
    public static String objectId;
    public static boolean IS_APP_RUNNING = false;
    SharedPreferences prefs;
    public static Context context;
    String error;

    @Override
    public void onCreate() {
        super.onCreate();

        crashHandler();

        // appid+clientid
        // Add your initialization code here
        Parse.initialize(this, "FpRoheoTwKCFfh7Vnp5ilTNTEQAFoC3aODnsr43x",
                "PxOSAPz9NI3qky594GDQ4thEvJhuLyT0tJE0RyyC");

        ParseUser.enableAutomaticUser();

        String android_id = Settings.Secure.getString(getApplicationContext()
                .getContentResolver(), Settings.Secure.ANDROID_ID);
        prefs = getSharedPreferences("user_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("objectId", android_id);
        editor.commit();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

       // NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher);
        //PushService.subscribe(this, "ChannelName", DashBoard.class, R.drawable.ic_add_b);
        ParseInstallation installation = ParseInstallation
                .getCurrentInstallation();
        installation.put("UniqueId", android_id);

        installation.saveInBackground();
        final ParseObject po = new ParseObject("Installation");
        po.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    objectId = (String) ParseInstallation
                            .getCurrentInstallation().get("UniqueId");
                } else {
                    // Failure!
                }
            }
        });

    }

    private void crashHandler() {
        ParseApplication.context = getApplicationContext();
        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                handleUncaughtException (thread, e);
            }
        });
    }
    public void handleUncaughtException (Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        error =  e.getCause().toString();
        Log.i("Crash","True"+e.getCause());
        new sendError().execute();
    }

    class sendError extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Url:", "" + Utils.set_error + "Error=" + error+"&Type=android");
            RestClient client = new RestClient(Utils.set_error + "Error=" + error+"&Type=android");
            String response = client.executePost();
            Log.i("Response:", "" + response);
            return null;
        }
    }

}

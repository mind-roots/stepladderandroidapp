package com.stepladder.job.stepladder.Seeker_Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 1/18/2016.
 */
public class Step4Seeker extends Activity implements Animation.AnimationListener{
    RelativeLayout rl_got_it;
    TextView textView1;
    Animation animZoomIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step4seeker);

        rl_got_it = (RelativeLayout)findViewById(R.id.rl_got_it);
        textView1 = (TextView)findViewById(R.id.textView1);
        textView1.setTypeface(Utils.setfontlight(Step4Seeker.this));

        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_in);
        animZoomIn.setAnimationListener(this);
        rl_got_it.startAnimation(animZoomIn);

        rl_got_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Step1Seeker.replay.equals("yes")){
                    finish();
                    Step1Seeker.replay = "no";
                    DashBoard_Seeker.open = true;
                }else {
                    Intent intent = new Intent(Step4Seeker.this, DashBoard_Seeker.class);
                    intent.putExtra("parse", "false");
                    startActivity(intent);
                    finish();
                }
            }
        });

    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
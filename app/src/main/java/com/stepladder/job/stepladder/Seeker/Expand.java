package com.stepladder.job.stepladder.Seeker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker_Fragments.DashboardSeekerFragment;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 10/28/2015.
 */
public class Expand extends ActionBarActivity {
    ActionBar actionBar;
    View actionview;
    ImageView img_back, img_cross, img_tick, img_company;
    TextView txt_company_name, txt_desc, title_desc, txt_skills, title_skills, txt_area, title_area,
            txt_length, title_length, txt_salary, title_salary, txt_job_title, title_title;
    String company_name, job_title, salary, job_length, area, skills, desc, image;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand);

        init();

        DashBoard_Seeker.open = true;

        setFont();

        ActionBar();

        if (Build.VERSION.SDK_INT < 21) {

        } else {
            setupWindowAnimations();
        }

        //Get Intent Values
        Intent intent = getIntent();
        try {
            company_name = intent.getStringExtra("company_name");
            txt_company_name.setText(company_name);
            job_title = intent.getStringExtra("job_title");
            txt_job_title.setText(job_title);
            salary = intent.getStringExtra("salary");
            txt_salary.setText(salary);
            job_length = intent.getStringExtra("job_length");
            txt_length.setText(job_length);
            area = intent.getStringExtra("area");
            txt_area.setText(area);
            skills = intent.getStringExtra("skills");
            txt_skills.setText(skills);
            desc = intent.getStringExtra("desc");
            txt_desc.setText(desc);
            image = intent.getStringExtra("image");
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(image).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                    .into(img_company, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        clickevents();
    }

    //***************************************UI Initialization**************************************
    private void init() {
        img_company = (ImageView) findViewById(R.id.img_company);
        txt_company_name = (TextView) findViewById(R.id.txt_company_name);
        title_title = (TextView) findViewById(R.id.title_title);
        txt_job_title = (TextView) findViewById(R.id.txt_job_title);
        title_salary = (TextView) findViewById(R.id.title_salary);
        txt_salary = (TextView) findViewById(R.id.txt_salary);
        title_length = (TextView) findViewById(R.id.title_length);
        txt_length = (TextView) findViewById(R.id.txt_length);
        title_area = (TextView) findViewById(R.id.title_area);
        txt_area = (TextView) findViewById(R.id.txt_area);
        title_skills = (TextView) findViewById(R.id.title_skills);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        title_desc = (TextView) findViewById(R.id.title_desc);
        txt_desc = (TextView) findViewById(R.id.txt_desc);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }


    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        title_title.setTypeface(Utils.setfontlight(Expand.this));
        txt_job_title.setTypeface(Utils.setfontlight(Expand.this));
        title_area.setTypeface(Utils.setfontlight(Expand.this));
        txt_area.setTypeface(Utils.setfontlight(Expand.this));
        title_desc.setTypeface(Utils.setfontlight(Expand.this));
        title_skills.setTypeface(Utils.setfontlight(Expand.this));
        title_length.setTypeface(Utils.setfontlight(Expand.this));
        title_salary.setTypeface(Utils.setfontlight(Expand.this));
        txt_desc.setTypeface(Utils.setfontlight(Expand.this));
        txt_company_name.setTypeface(Utils.setfontlight(Expand.this));
        txt_length.setTypeface(Utils.setfontlight(Expand.this));
        txt_salary.setTypeface(Utils.setfontlight(Expand.this));
        txt_skills.setTypeface(Utils.setfontlight(Expand.this));
    }

    //***************************************Click Events*******************************************
    private void clickevents() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardSeekerFragment.reject = true;
                finish();
            }
        });

        img_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardSeekerFragment.accept = true;
                finish();
            }
        });
    }

    //***************************************Custom ActionBar***************************************
    private void ActionBar() {
        // TODO Auto-generated method stub
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionview = inflater.inflate(R.layout.action_bar_expand, null);
        img_back = (ImageView) actionview.findViewById(R.id.img_back);
        img_cross = (ImageView) actionview.findViewById(R.id.img_cross);
        img_tick = (ImageView) actionview.findViewById(R.id.img_tick);

        actionBar.setCustomView(actionview,
                new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    //***************************************transitionTo Method************************************
    @SuppressLint("NewApi")
    private void setupWindowAnimations() {
        Transition transition;
        transition = buildEnterTransition();
        getWindow().setEnterTransition(transition);
    }

    @SuppressLint("NewApi")
    private Transition buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }
}

package com.stepladder.job.stepladder.Employeer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Profile_photo;
import com.stepladder.job.stepladder.Seeker.Sign_In;
import com.stepladder.job.stepladder.SplashScreen;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class Employeer_Registration extends ActionBarActivity {
    Menu newmenu;
    EditText edt_companyname, edt_firstname, edt_lastname, edt_dob, edt_phone_no, edt_email,
            edt_password;
    TextView txt_signin_userregi, detail_txt, firstjob_txt,useragreement, txt_agree, policy, btn_txt, title;
    LayoutInflater inflater;
    RelativeLayout lay_user_joinnow;
    SharedPreferences prefs;
    RestClient client;
    String error, status, company_name, first_name, last_name, phone_no, email, password, auth_code, profile_image,android_id;
    boolean isEmail = false, check = false;
    ImageView tick;
    ConnectionDetector con;
    ProgressBar progressEmail;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employeer_registration);

        init();

        //----------------get player id from ONESIGNAL fo notification--------
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                android_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + android_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });

        setFont();

        //Check Email
        edt_email.setOnFocusChangeListener(EmailFocus);
        edt_email.addTextChangedListener(EmailWatcher);

        clickevents();

    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Employeer_Registration.this, PrivacyPolicy.class);
                ActivityStack.activity.add(Employeer_Registration.this);
                startActivity(intent);
            }
        });

        useragreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Employeer_Registration.this, User_Agreement.class);
                ActivityStack.activity.add(Employeer_Registration.this);
                startActivity(intent);
            }
        });

        lay_user_joinnow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (edt_companyname.length() == 0) {
                    edt_companyname.setError("Required field");
                } else if (edt_firstname.length() == 0) {
                    edt_firstname.setError("Required Field");
                } else if (edt_lastname.length() == 0) {
                    edt_lastname.setError("Required Field");
                } else if (edt_phone_no.length() == 0) {
                    edt_phone_no.setError("Required Field");
                } else if (!isEmail) {
                    check = true;
                    new CheckEmailService().execute();
                    //edt_email.setError("Enter valid email address",null);
                } else if (edt_password.length() == 0) {
                    edt_password.setError("Required field");
                } else if (edt_password.length() < 5) {
                    Toast.makeText(getApplicationContext(), "Password should contain at least 5 characters", Toast.LENGTH_SHORT).show();
                } else {
                    company_name = edt_companyname.getText().toString();
                    first_name = edt_firstname.getText().toString();
                    last_name = edt_lastname.getText().toString();
                    phone_no = edt_phone_no.getText().toString();
                    email = edt_email.getText().toString();
                    password = edt_password.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Employeer_Registration.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new sign_up().execute();
                    }
                }
            }
        });

        txt_signin_userregi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Employeer_Registration.this, Sign_In.class);
                ActivityStack.activity.add(Employeer_Registration.this);
                intent.putExtra("login", "employeer");
                startActivity(intent);
                finish();
            }
        });
    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Employer Registration");
        tick = (ImageView) findViewById(R.id.tick);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        txt_signin_userregi = (TextView) findViewById(R.id.txt_signin_userregi);
        lay_user_joinnow = (RelativeLayout) findViewById(R.id.lay_user_joinnow);
        edt_companyname = (EditText) findViewById(R.id.edt_companyname);
        edt_firstname = (EditText) findViewById(R.id.edt_first_name);
        edt_lastname = (EditText) findViewById(R.id.edt_lastname);
        edt_phone_no = (EditText) findViewById(R.id.edt_phone_no);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        detail_txt = (TextView) findViewById(R.id.detail_txt);
        firstjob_txt = (TextView) findViewById(R.id.firstjob_txt);
        policy = (TextView) findViewById(R.id.policy);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        txt_agree = (TextView) findViewById(R.id.txt_agree);
        useragreement = (TextView)findViewById(R.id.useragreement);
        progressEmail = (ProgressBar) findViewById(R.id.progressEmail);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Register Company");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //***********************Setting Fonts for UI components******************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Employeer_Registration.this), Typeface.BOLD);
        detail_txt.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        firstjob_txt.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        policy.setTypeface(Utils.setfontlight(Employeer_Registration.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        txt_agree.setTypeface(Utils.setfontlight(Employeer_Registration.this), Typeface.BOLD);
        txt_signin_userregi.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_companyname.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_firstname.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_lastname.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_phone_no.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_email.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        edt_password.setTypeface(Utils.setfontlight(Employeer_Registration.this));
        useragreement.setTypeface(Utils.setfontlight(Employeer_Registration.this), Typeface.BOLD);
    }

    //***********************************SignUp Async Class*****************************************
    class sign_up extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Employeer_Registration.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Signing up...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = signup(password, company_name, first_name, last_name, phone_no, email, android_id, "android");
            Log.i("Response", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    auth_code = data.getString("AuthCode");
                    Log.i("AuthCode", ":" + auth_code);
                    profile_image = data.optString("profile_image");
                } else {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    error = "";
                    if (data.has("Phone_number")) {
                        String phone = data.getString("Phone_number");
                        error = error + phone;
                    }
                    if (data.has("Password")) {
                        String pass = data.getString("Password");
                        error = error + pass;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                Intent i = new Intent(Employeer_Registration.this,
                        Company_Details.class);
                ActivityStack.activity.add(Employeer_Registration.this);
                startActivity(i);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("type", "job-offer");
                edit.putString("auth_code", auth_code);
                edit.putString("company_name", company_name);
                edit.putString("first_name", first_name);
                edit.putString("last_name", last_name);
                edit.putString("phone_no", phone_no);
                edit.putString("email", email);
                edit.putString("password", password);
                edit.putString("profile_image", profile_image);
                edit.putString("confirmed", "0");
                edit.commit();
            } else {
                if (error != "") {
                    dialog("Alert!", error);
                }
            }
        }
    }

    //****************************************Sign Up Method****************************************
    public String signup(String password, String company, String firstname, String lastname,
                         String phone, String email, String device_id, String type) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "Password", password)
                .appendQueryParameter("CompanyName", company)
                .appendQueryParameter("FirstName", firstname)
                .appendQueryParameter("LastName", lastname)
                .appendQueryParameter("Phone_number", phone)
                .appendQueryParameter("Email", email)
                .appendQueryParameter("DeviceId", device_id)
                .appendQueryParameter("Type", type);
        Log.i("Complete", "Url: " + Utils.sign_up + builder);
        client = new RestClient(Utils.sign_up + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //**************************************Alert Dialog********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    // Check Email exists or Not*****

    private class CheckEmailService extends AsyncTask<Void, Void, Void> {

        String status, message, text;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressEmail.setVisibility(View.VISIBLE);
            text = edt_email.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            String response = EmailCheck(text, Utils.CHECK_MAIL);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");
                Log.i("Status: ", "" + status);
                if (status.equals("true")) {
                    // message = obj.getString("data");
                } else {
                    //message = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {

                if (status.equals("true")) {
                    isEmail = true;
                    tick.setImageResource(R.drawable.green_tick);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt_email.setTextColor(Color.parseColor("#000000"));
                    if (check) {

                        company_name = edt_companyname.getText().toString();
                        first_name = edt_firstname.getText().toString();
                        last_name = edt_lastname.getText().toString();
                        phone_no = edt_phone_no.getText().toString();
                        email = edt_email.getText().toString();
                        password = edt_password.getText().toString();

                        //Connection Detection
                        if (!con.isConnectingToInternet()) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(
                                    Employeer_Registration.this);

                            alert.setTitle("Internet connection unavailable.");
                            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                            alert.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {
                                            startActivity(new Intent(
                                                    Settings.ACTION_WIRELESS_SETTINGS));
                                        }
                                    });

                            alert.show();
                        } else {
                            new sign_up().execute();
                        }

                        check = false;
                    }
                } else {
                    isEmail = false;
                    tick.setImageResource(R.drawable.red_cross);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt_email.setTextColor(Color.parseColor("#e53b36"));
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

    }

    // ************************Email Check Method************************
    private String EmailCheck(String email, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("Email",
                email);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;

    }

    // ***********************Watch email text*****************
    private final TextWatcher EmailWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            edt_email.setTextColor(Color.BLACK);
            tick.setVisibility(View.INVISIBLE);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    // ******************Detect Email Focus change*********************
    private final View.OnFocusChangeListener EmailFocus = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            // TODO Auto-generated method stub

            if (!hasFocus && edt_email.length() > 1) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            Employeer_Registration.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new CheckEmailService().execute();
                }
            }

        }
    };

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

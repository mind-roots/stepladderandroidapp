package com.stepladder.job.stepladder.Employer_Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 1/18/2016.
 */
public class Step4Employer extends Activity implements Animation.AnimationListener,GestureDetector.OnGestureListener{
    TextView txt1,txt2,txt3,txt4,txt5;
    ImageView img_hand,img_arrow,img_cross_red,img_main;
    GestureDetector gd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step4employer);
        init();

        setFonts();

        TranslateAnimation animation = new TranslateAnimation(0, -180, 0, 0);
        animation.setDuration(2000);
        animation.setFillAfter(false);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setAnimationListener(this);

        img_hand.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_left));

        img_arrow.startAnimation(animation);

    }

    private void init() {
        txt1 = (TextView)findViewById(R.id.txt1);
        txt2 = (TextView)findViewById(R.id.txt2);
        txt3 = (TextView)findViewById(R.id.txt3);
        txt4 = (TextView)findViewById(R.id.txt4);
        txt5 = (TextView)findViewById(R.id.txt5);
        img_hand = (ImageView)findViewById(R.id.img_hand);
        img_arrow = (ImageView)findViewById(R.id.img_arrow);
        img_cross_red = (ImageView)findViewById(R.id.img_cross_red);
        img_main = (ImageView)findViewById(R.id.img_main);
        gd = new GestureDetector(this, this);
    }

    private void setFonts(){
        txt1.setTypeface(Utils.setfontlight(Step4Employer.this));
        txt2.setTypeface(Utils.setfontlight(Step4Employer.this));
        txt3.setTypeface(Utils.setfontlight(Step4Employer.this));
        txt4.setTypeface(Utils.setfontlight(Step4Employer.this));
        txt5.setTypeface(Utils.setfontlight(Step4Employer.this));
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // TODO Auto-generated method stub
        //Defining Sensitivity
        float sensitivity = 200;
        if (e1.getY() - e2.getY() > sensitivity) {
            // Toast.makeText(getApplicationContext(), "down", Toast.LENGTH_LONG).show();

            return true;
        } else if (e2.getY() - e1.getY() > sensitivity) {
            // Toast.makeText(getApplicationContext(), "up", Toast.LENGTH_LONG).show();

            return true;
        }
        //Swipe SecondRight Check
        else if (e1.getX() - e2.getX() > sensitivity) {
            // Toast.makeText(getApplicationContext(), "right", Toast.LENGTH_LONG).show();
            //img_cross_red.setVisibility(View.VISIBLE);
            img_main.setImageResource(R.drawable.eleft_screen);
            Intent i = new Intent(Step4Employer.this, Step5Employer.class);
            //ActivityStack.activity.add(Step4Employer.this);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            return true;
        } else if (e2.getX() - e1.getX() > sensitivity) {
            //Toast.makeText(getApplicationContext(), "left", Toast.LENGTH_LONG).show();
            return true;
        } else {
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gd.onTouchEvent(event);
    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

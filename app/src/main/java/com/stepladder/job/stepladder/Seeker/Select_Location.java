package com.stepladder.job.stepladder.Seeker;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stepladder.job.stepladder.CustomMapFragment;
import com.stepladder.job.stepladder.Custom_View.MapWrapperLayout;
import com.stepladder.job.stepladder.Employeer.Company_Details;
import com.stepladder.job.stepladder.Employeer.Job_Details;
import com.stepladder.job.stepladder.Employeer_Fragments.EditEmployeerProfileFragment;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.GpsTracker;
import com.stepladder.job.stepladder.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.stepladder.job.stepladder.Utils.Utils.lat;

/**
 * Created by user on 10/21/2015.
 */
public class Select_Location extends ActionBarActivity implements MapWrapperLayout.OnDragListener,OnMapReadyCallback{
    // Google Map
    private GoogleMap mMap;
    private CustomMapFragment mCustomMapFragment;
    LayoutInflater inflater;
    TextView title;
    public static LatLng MyLocation;
    private View mMarkerParentView;
    private ImageView mMarkerImageView;
    Menu newmenu;
    private int imageParentWidth = -1;
    private int imageParentHeight = -1;
    private int imageHeight = -1;
    private int centerX = -1;
    private int centerY = -1;
    String completeAddress="";
    //private TextView mLocationTextView;
//**********************************************************************************
    private static LatLng CHD = null;
    GpsTracker gps;
    double LAT, LONG;
    Geocoder geocoder;
    List<Address> addresses;
    MarkerOptions options;
    ConnectionDetector con;
   // TextView dist, go_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_location);
       /* SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setOnDragListener(Select_Location.this);
        mapFragment.getMapAsync(this);*/

        CustomMapFragment mCustomMapFragment = ((CustomMapFragment) getFragmentManager()
                .findFragmentById(R.id.map));
        mCustomMapFragment.setOnDragListener(Select_Location.this);
        mCustomMapFragment.getMapAsync(this);

        gps = new GpsTracker(Select_Location.this);

        initializeUI();

        if (gps.canGetLocation()) {
            lat = gps.latitude;
            Utils.lng = gps.longitude;
            Log.i("LAT", "" + LAT);
            Log.i("LAT", "" + LONG);
            MyLocation = new LatLng(lat, Utils.lng);
            try {
                addresses = geocoder.getFromLocation(lat, Utils.lng, 1);

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getSubLocality();
                String knownName = addresses.get(0).getFeatureName();
                Log.i("Suburb: ", "" + postalCode + "" + knownName);
                System.out.println("Location" + state + "," + country);
//                atvPlaces.setText(knownName + ", " + city);
                Utils.selected_loc = knownName+", "+city;
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Unable to get location",
                                Toast.LENGTH_SHORT).show();
                    }
                });

            }
        } else {
            Log.i("No LAT", "no");
        }

        SupportPlaceAutocompleteFragment autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("API", "Place: " + place.getName());
                Utils.selected_loc = String.valueOf(place.getName());
                LAT = place.getLatLng().latitude;
                LONG = place.getLatLng().longitude;
                mMap.clear();
                options.position(place.getLatLng());
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(place.getLatLng())
                        .zoom(12)
                        .build();
                mMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(cameraPosition),
                        2000, null);
                updateLocation(place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("API", "An error occurred: " + status);
            }
        });
    }

    //********************************************UI Initialization*********************************
    private void initializeUI() {
        con = new ConnectionDetector(this);
        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.action_title, null);
            actionBar.setCustomView(view, new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT));
            title = (TextView) view.findViewById(R.id.action_title);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            title.setText("Location");
            title.setTypeface(Utils.setfontlight(Select_Location.this), Typeface.BOLD);
            geocoder = new Geocoder(this, Locale.getDefault());

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
        // mLocationTextView = (TextView) findViewById(R.id.location_text_view);
        mMarkerParentView = findViewById(R.id.marker_view_incl);
        mMarkerImageView = (ImageView) findViewById(R.id.marker_icon_view);
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(false);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        newmenu.findItem(R.id.action_done).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_done) {
            if (getIntent().getStringExtra("id").equals("1")) {
//                if(atvPlaces.getText().toString().equals("")){
//                    finish();
//                }else {
//                    Utils.selected_loc = atvPlaces.getText().toString();
                    Location.edt_location.setText(Utils.selected_loc);
                   // Utils.selected_loc = atvPlaces.getText().toString();
                    finish();
//                }
            } else if (getIntent().getStringExtra("id").equals("2")) {
//                if(atvPlaces.getText().toString().equals("")){
//                    finish();
//                }else {
//                    Utils.selected_loc = atvPlaces.getText().toString();
                    Job_Details.edt_location.setText(Utils.selected_loc);
                    //Utils.selected_loc = atvPlaces.getText().toString();
                    finish();
//                }
            }else if(getIntent().getStringExtra("id").equals("3")){
//                if(atvPlaces.getText().toString().equals("")){
//                    finish();
//                }else {
//                    Utils.selected_loc = atvPlaces.getText().toString();
                    EditEmployeerProfileFragment.edt_location.setText(Utils.selected_loc);
                    //Utils.selected_loc = atvPlaces.getText().toString();
                    finish();
//                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //******************************************onWindowFocusChanged********************************
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
try {
    imageParentWidth = mMarkerParentView.getWidth();
    imageParentHeight = mMarkerParentView.getHeight();
    imageHeight = mMarkerImageView.getHeight();

    centerX = imageParentWidth / 2;
    centerY = (imageParentHeight / 2) + (imageHeight / 2);
}catch (Exception e){

}
    }

    //**************************************************onDrag Method*******************************
    @Override
    public void onDrag(MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Projection projection = (mMap != null && mMap
                    .getProjection() != null) ? mMap.getProjection()
                    : null;
            //
            if (projection != null) {
                LatLng centerLatLng = projection.fromScreenLocation(new Point(
                        centerX, centerY));
                Log.i("centerLatLng: ", ""+centerLatLng);
                updateLocation(centerLatLng);
            }
        }
    }

    //***************************************Update Location****************************************
    private void updateLocation(LatLng centerLatLng) {
        if (centerLatLng != null) {
            Geocoder geocoder = new Geocoder(Select_Location.this,
                    Locale.getDefault());

            List<Address> addresses = new ArrayList<Address>();
            try {
                addresses = geocoder.getFromLocation(centerLatLng.latitude,
                        centerLatLng.longitude, 1);
                lat = centerLatLng.latitude;
                Utils.lng = centerLatLng.longitude;
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {
                Log.i("Complete Address List: ",""+addresses);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getThoroughfare();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getSubLocality();
                String knownName = addresses.get(0).getFeatureName(); //
                Log.i("Suburb: ",""+state+""+city);

                if(city!=null){
                    if(state!=null){
                        completeAddress = state+", "+city;
                    }else {
                        completeAddress = city;
                    }
                }else {
                    if(state!=null){
                        completeAddress = state;
                    }else {
                        completeAddress = address;
                    }
                }

                //completeAddress = address+", "+city;

                if (completeAddress != null) {
//                    atvPlaces.setText(completeAddress);
                    Utils.selected_loc = completeAddress;
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        CHD = new LatLng(lat, Utils.lng);
        options = new MarkerOptions();
        options.position(CHD);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CHD, 13));
    }

}

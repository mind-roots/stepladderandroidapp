package com.stepladder.job.stepladder.Seeker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.GpsTracker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Location extends ActionBarActivity {

    public static EditText edt_distance, edt_location;
    LinearLayout lay_location_edit;
    RelativeLayout rl_past_jobs, rl_set_location;
    ImageView img_location, imageView3;
    //public static double latitude, longitude;
    public static LatLng MyLocation;
    GpsTracker gps;
    Geocoder geocoder;
    TextView title, txt_work, btn_txt;
    LayoutInflater inflater;
    List<Address> addresses;
    RestClient client;
    AlertDialog.Builder builder;
    String selectedItem, city, state, country, status, knownName, edit;
    String[] arr = {"0 miles - 5 miles", "0 miles - 10 miles", "0 miles - 20 miles","0 miles - 30 miles", "30+ miles"};
    int mile = -1;
    Menu newmenu;
    ProgressBar progress;
    ConnectionDetector con;
    SharedPreferences prefs;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);

        //Get Intent
        Intent intent = getIntent();
        edit = intent.getStringExtra("edit");

        init();

        setFont();

        //Set Prefs Data
        if(prefs.getString("distance",null)!=null){
            edt_distance.setText(prefs.getString("distance",null));
            selectedItem = prefs.getString("distance",null);
            DashBoard_Seeker.open = true;
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(
                    Location.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            if (edit.equals("true")) {

            } else {
                new getlocation().execute();
            }
        }

        clickevents();


    }


    //*******************************UI intialization***********************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Location");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        gps = new GpsTracker(Location.this);
        rl_set_location = (RelativeLayout) findViewById(R.id.rl_set_location);
        rl_past_jobs = (RelativeLayout) findViewById(R.id.rl_past_jobs);
        edt_distance = (EditText) findViewById(R.id.edt_distance);
        edt_location = (EditText) findViewById(R.id.edt_location);
        img_location = (ImageView) findViewById(R.id.img_location);
        geocoder = new Geocoder(this, Locale.getDefault());
        txt_work = (TextView) findViewById(R.id.txt_work);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        progress = (ProgressBar) findViewById(R.id.progress);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        if (edit.equals("true")) {
            btn_txt.setText("Update Location");
            imageView3.setVisibility(View.INVISIBLE);
            Log.i("Distance: ", "" + prefs.getString("distance", null));
            edt_distance.setText(prefs.getString("distance", null));
            edt_location.setText(prefs.getString("area", null));
            selectedItem = edt_distance.getText().toString();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Seeker Location");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //***********************Setting Fonts for UI components******************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Location.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Location.this));
        txt_work.setTypeface(Utils.setfontlight(Location.this));
        edt_distance.setTypeface(Utils.setfontlight(Location.this));
        edt_location.setTypeface(Utils.setfontlight(Location.this));
    }

    //***********************************clickevents************************************************
    private void clickevents() {

        edt_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Location.this, Select_Location.class);
                ActivityStack.activity.add(Location.this);
                in.putExtra("id", "1");
                in.putExtra("current_loc",Utils.selected_loc);
                startActivity(in);
            }
        });

        rl_past_jobs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (edt_location.length() == 0) {
                    edt_location.setError("Required Field");
                } else if (edt_distance.length() == 0) {
                    edt_distance.setError("Required Field");
                } else {
                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(
                                Location.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new location().execute();
                    }
                }
            }
        });

        edt_distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogSingle(Location.this, arr, "", edt_distance, mile, "");
            }
        });
    }

    //*************************************Location Async Task Classes******************************
    class getlocation extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //***********************************Get Current Location*******************************
            if (gps.canGetLocation()) {

                Utils.lat = gps.getLatitude();
                Utils.lng = gps.getLongitude();
                MyLocation = new LatLng(Utils.lat, Utils.lng);
                try {
                    addresses = geocoder.getFromLocation(Utils.lat, Utils.lng, 1);

                    String address = addresses.get(0).getAddressLine(0);
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getSubLocality();
                    knownName = addresses.get(0).getFeatureName();
                    Log.i("Suburb: ", "" + postalCode + "" + knownName);
                    System.out.println("Location" + state + "," + country);

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Unable to get location",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.setVisibility(View.GONE);
            if (gps.canGetLocation()) {
                edt_location.setText(knownName + ", " + city);
                Utils.selected_loc = knownName + ", " + city;
            } else {
                Toast.makeText(getApplicationContext(), "Unable to get location",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    class location extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Location.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Sending location...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "Location: " + Utils.selected_loc + "Latitude: " + Utils.lat + "Longtitude: "
                    + Utils.lng + "selectedItem: " + selectedItem);
            String response = Location(prefs.getString("auth_code", null), Utils.selected_loc,
                    Utils.lat, Utils.lng, selectedItem);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                if (edit.equals("true")) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("area", Utils.selected_loc);
                    editor.putString("distance", selectedItem);
                    editor.commit();
                    dialog("Success!", "Your location has been updated successfully");
                } else {
                    Intent i = new Intent(Location.this, Past_Jobs.class);
                    i.putExtra("edit", "false");
                    ActivityStack.activity.add(Location.this);
                    startActivity(i);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("area", Utils.selected_loc);
                    editor.putString("distance", selectedItem);
                    editor.putString("confirmed","0");
                    editor.commit();
                }
            } else {
                dialog("Alert!", "Error in saving location!");
            }
        }
    }

    //***************************************Location Method****************************************
    public String Location(String auth_code, String LocationName, double Lat, double Long, String DistanceLookingFor) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("LocationName", LocationName)
                .appendQueryParameter("Lat", String.valueOf(Lat))
                .appendQueryParameter("Long", String.valueOf(Long))
                .appendQueryParameter("DistanceLookingFor", DistanceLookingFor);
        Log.i("Complete", "Url: " + Utils.location + builder);
        client = new RestClient(Utils.location + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //***********************************************My Dialog**************************************
    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               String title, final EditText select1, final int value,
                               final String string) {

        builder = new AlertDialog.Builder(Location.this, R.style.DialogTheme);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mile = which;
                        selectedItem = charSequences[which];
                        select1.setText(selectedItem);
                        dialog.dismiss();
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }
}

package com.stepladder.job.stepladder.Seeker_Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Past_Jobs;
import com.stepladder.job.stepladder.Seeker.ReportIssue;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step1Seeker;
import com.stepladder.job.stepladder.Seeker_Tutorial.TutorialSeeker;
import com.stepladder.job.stepladder.Utils.Utils;
import com.stepladder.job.stepladder.WelcomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingsFragment extends Fragment {
    View rootView;
    LinearLayout ll_logout, ll_change_name, ll_email_address, ll_phone_number, ll_change_password, ll_delete_account,
            ll_replay_tutorial,ll_report;
    TextView txt_delete, txt_logout, txt_pass, txt_security, txt_phone, txt_email, txt_contact, txt_name,
            txt_details,txt_replay,txt_replay_tut,txt_report_bug,txt_report;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Dialog screenDialog;
    RestClient client;
    String status,error;
    ConnectionDetector con;
    boolean isEmail = false;
    ImageView tick;
    EditText edt;
    ProgressBar progressEmail, progressBar;
    boolean valid = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.employee_settings_fragment, container, false);

        init();

        setFonts();

        clickevents();
        DashBoard_Seeker.isHome = false;
        DashBoard_Seeker.open = true;
        DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
        DashBoard_Seeker.ll_badge.setVisibility(View.GONE);
        //DashBoard_Seeker.newmenu.findItem(R.id.action_update).setVisible(false);

        return rootView;
    }

    //********************************************UI Initialization*********************************
    public void init() {
        ll_logout = (LinearLayout) rootView.findViewById(R.id.ll_logout);
        txt_contact = (TextView) rootView.findViewById(R.id.txt_contact);
        txt_delete = (TextView) rootView.findViewById(R.id.txt_delete);
        txt_logout = (TextView) rootView.findViewById(R.id.txt_logout);
        txt_pass = (TextView) rootView.findViewById(R.id.txt_pass);
        txt_security = (TextView) rootView.findViewById(R.id.txt_security);
        txt_phone = (TextView) rootView.findViewById(R.id.txt_phone);
        txt_email = (TextView) rootView.findViewById(R.id.txt_email);
        txt_name = (TextView) rootView.findViewById(R.id.txt_name);
        txt_details = (TextView) rootView.findViewById(R.id.txt_details);
        txt_report_bug = (TextView) rootView.findViewById(R.id.txt_report_bug);
        txt_report = (TextView) rootView.findViewById(R.id.txt_report);
        ll_change_name = (LinearLayout) rootView.findViewById(R.id.ll_change_name);
        ll_email_address = (LinearLayout) rootView.findViewById(R.id.ll_email_address);
        ll_phone_number = (LinearLayout) rootView.findViewById(R.id.ll_phone_number);
        ll_change_password = (LinearLayout) rootView.findViewById(R.id.ll_change_password);
        ll_delete_account = (LinearLayout) rootView.findViewById(R.id.ll_delete_account);
        ll_replay_tutorial = (LinearLayout) rootView.findViewById(R.id.ll_replay_tutorial);
        ll_report = (LinearLayout) rootView.findViewById(R.id.ll_report);
        txt_replay = (TextView)rootView.findViewById(R.id.txt_replay);
        txt_replay_tut = (TextView)rootView.findViewById(R.id.txt_replay_tut);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
    }

    //********************************************Set Fonts*****************************************
    public void setFonts() {
        con = new ConnectionDetector(getActivity());
        txt_contact.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_delete.setTypeface(Utils.setfontlight(getActivity()));
        txt_details.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_email.setTypeface(Utils.setfontlight(getActivity()));
        txt_logout.setTypeface(Utils.setfontlight(getActivity()));
        txt_name.setTypeface(Utils.setfontlight(getActivity()));
        txt_pass.setTypeface(Utils.setfontlight(getActivity()));
        txt_phone.setTypeface(Utils.setfontlight(getActivity()));
        txt_security.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_replay.setTypeface(Utils.setfontlight(getActivity()));
        txt_replay_tut.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_report_bug.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_report.setTypeface(Utils.setfontlight(getActivity()));
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
    }

    //********************************************Clickevents***************************************
    public void clickevents() {
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new logout(Utils.logout).execute();
                }
            }
        });
        ll_change_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("first_name", null) != null && prefs.getString("last_name", null) != null) {
                    ShowDialog(txt_name.getText().toString(), prefs.getString("first_name", null), prefs.getString("last_name", null));
                }

            }
        });

        ll_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("password", null) != null) {
                    ShowDialog(txt_pass.getText().toString(), null, null);
                }

            }
        });

        ll_email_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("email", null) != null) {
                    ShowDialog(txt_email.getText().toString(), prefs.getString("email", null), null);
                }

            }
        });

        ll_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("phone_no", null) != null) {
                    ShowDialog(txt_phone.getText().toString(), prefs.getString("phone_no", null), null);
                }

            }
        });

        ll_delete_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    delete_dialog();
                }
            }
        });

        ll_replay_tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Step1Seeker.replay = "yes";
                Intent intent = new Intent(getActivity(), Step1Seeker.class);
                startActivity(intent);
            }
        });

        ll_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ReportIssue.class));
            }
        });

    }


    //******************************************Dialog Method***************************************
    public void ShowDialog(final String title, String value, String value2) {
        // TODO Auto-generated method stub
        screenDialog = new Dialog(getActivity(), R.style.DialogTheme);
        screenDialog.show();
        screenDialog.setContentView(R.layout.dialog_layout);
        RelativeLayout lay_update_btn, lay_delete_btn;
        TextView textView_sub = (TextView) screenDialog.findViewById(R.id.textView_sub);
        TextView btn_update_txt = (TextView) screenDialog.findViewById(R.id.btn_update_txt);
        TextView btn_cancel_txt = (TextView) screenDialog.findViewById(R.id.btn_cancel_txt);
        edt = (EditText) screenDialog.findViewById(R.id.edt);
        final EditText edt2 = (EditText) screenDialog.findViewById(R.id.edt_second);
        final EditText edt_confirm = (EditText) screenDialog.findViewById(R.id.edt_confirm);
        progressEmail = (ProgressBar) screenDialog.findViewById(R.id.progressEmail);
        tick = (ImageView) screenDialog.findViewById(R.id.tick);
        edt.setTypeface(Utils.setfontlight(getActivity()));
        edt.setHintTextColor(Color.parseColor("#d4d4d4"));
        edt2.setTypeface(Utils.setfontlight(getActivity()));
        edt2.setHintTextColor(Color.parseColor("#d4d4d4"));
        edt_confirm.setTypeface(Utils.setfontlight(getActivity()));
        edt_confirm.setHintTextColor(Color.parseColor("#d4d4d4"));
        edt.setText(value);
        edt.setSelection(edt.getText().length());
        edt2.setText(value2);
        edt2.setSelection(edt2.getText().length());
        if (title.equals("Change Name")) {
            edt2.setVisibility(View.VISIBLE);
            edt_confirm.setVisibility(View.GONE);
            edt.setHint("First Name");
            edt2.setHint("Last Name");
            edt.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            edt2.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        } else if (title.equals("Change Email Address")) {
            edt2.setVisibility(View.GONE);
            edt_confirm.setVisibility(View.GONE);
            edt.setHint("Email");
            edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            //Check Email
            edt.addTextChangedListener(EmailWatcher);
        } else if (title.equals("Change Phone Number")) {
            edt2.setVisibility(View.GONE);
            edt_confirm.setVisibility(View.GONE);
            edt.setHint("Phone Number");
            edt.setInputType(InputType.TYPE_CLASS_NUMBER);
            isEmail = true;
        } else if (title.equals("Change Password")) {
            edt2.setVisibility(View.VISIBLE);
            edt_confirm.setVisibility(View.VISIBLE);
            edt.setHint("Old Password");
            edt2.setHint("New Password");
            edt_confirm.setHint("Confirm Password");
            edt.setTransformationMethod(PasswordTransformationMethod.getInstance());
            edt2.setTransformationMethod(PasswordTransformationMethod.getInstance());
            edt_confirm.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        lay_delete_btn = (RelativeLayout) screenDialog.findViewById(R.id.lay_delete_btn);
        lay_update_btn = (RelativeLayout) screenDialog.findViewById(R.id.lay_update_btn);
        textView_sub.setTypeface(Utils.setfontlight(getActivity()));
        btn_update_txt.setTypeface(Utils.setfontlight(getActivity()));
        btn_cancel_txt.setTypeface(Utils.setfontlight(getActivity()));

        textView_sub.setText(title);
        lay_delete_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                screenDialog.dismiss();
            }
        });
        lay_update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt.length() == 0) {
                    edt.setError("Required Field");
                } else {
                    if (edt2.getVisibility() == View.VISIBLE) {
                        if (edt2.length() == 0) {
                            edt2.setError("Required Field");
                        } else {
                            if (edt_confirm.getVisibility() == View.VISIBLE) {
                                if (edt_confirm.length() == 0) {
                                    edt_confirm.setError("Required Field");
                                } else if (edt_confirm.length() < 5) {
                                    Toast.makeText(getActivity(), "Password should contain at least 5 characters", Toast.LENGTH_SHORT).show();
                                } else if (!edt2.getText().toString().equals(edt_confirm.getText().toString())) {
                                    Toast.makeText(getActivity(), "Please enter correct password", Toast.LENGTH_SHORT).show();
                                } else {
                                    screenDialog.dismiss();
                                    if (!con.isConnectingToInternet()) {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                                getActivity());

                                        alert.setTitle("Internet connection unavailable.");
                                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                        alert.setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {
                                                        startActivity(new Intent(
                                                                Settings.ACTION_WIRELESS_SETTINGS));
                                                    }
                                                });

                                        alert.show();
                                    } else {
                                        new update_info(title, edt.getText().toString(), edt2.getText().toString()).execute();
                                    }
                                }
                            } else {
                                screenDialog.dismiss();
                                if (!con.isConnectingToInternet()) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(
                                            getActivity());

                                    alert.setTitle("Internet connection unavailable.");
                                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                    alert.setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int whichButton) {
                                                    startActivity(new Intent(
                                                            Settings.ACTION_WIRELESS_SETTINGS));
                                                }
                                            });

                                    alert.show();
                                } else {
                                    new update_info(title, edt.getText().toString(), edt2.getText().toString()).execute();
                                }
                            }
                        }
                    } else if (edt2.getVisibility() == View.GONE) {
                        screenDialog.dismiss();
                        if (isEmail) {
                            if (!con.isConnectingToInternet()) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(
                                        getActivity());

                                alert.setTitle("Internet connection unavailable.");
                                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                alert.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int whichButton) {
                                                startActivity(new Intent(
                                                        Settings.ACTION_WIRELESS_SETTINGS));
                                            }
                                        });

                                alert.show();
                            } else {
                                new update_info(title, edt.getText().toString(), null).execute();
                            }
                            isEmail = false;
                        }

                    }
                }
            }
        });
    }


    //*************************************Update Async Class***************************************
    class update_info extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(getActivity());
        String title, value, value2, response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating...");
            dia.setCancelable(false);
            dia.show();
        }

        public update_info(String title, String value, String value2) {
            this.title = title;
            this.value = value;
            this.value2 = value2;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (value2 != null) {
                if (title.equals("Change Name")) {
                    response = update_name(prefs.getString("auth_code", null), value, value2, Utils.change_employer_name);
                } else if (title.equals("Change Password")) {
                    response = update_pass(prefs.getString("auth_code", null), value, value2, Utils.change_employer_pass);
                }
            } else {
                if (title.equals("Change Email Address")) {
                    response = update(prefs.getString("auth_code", null), "Email", value, Utils.change_employer_email);
                } else if (title.equals("Change Phone Number")) {
                    response = update(prefs.getString("auth_code", null), "Phone_number", value, Utils.change_employer_phone);
                }
            }
            Log.i("Response", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");

                    if (data.has("FirstName")) {
                        String FirstName = data.optString("FirstName");
                        editor = prefs.edit();
                        editor.putString("first_name", FirstName);
                        editor.commit();
                    }
                    if (data.has("LastName")) {
                        String LastName = data.optString("LastName");
                        editor = prefs.edit();
                        editor.putString("last_name", LastName);
                        editor.commit();
                    }
                    if (data.has("Email")) {
                        String Email = data.optString("Email");
                        editor = prefs.edit();
                        editor.putString("email", Email);
                        editor.commit();
                    }
                    if (data.has("Phone_number")) {
                        String phone = data.optString("Phone_number");
                        editor = prefs.edit();
                        editor.putString("phone_no", phone);
                        editor.commit();
                    }
                    if (data.has("Password")) {
                        String password = data.optString("Password");
                        editor = prefs.edit();
                        editor.putString("password", password);
                        editor.commit();
                    }
                }else {
                    error = jobj.optString("data");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                dialog("Success:", "Data updated successfully");
            } else {
                dialog("Alert!", error);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        DashBoard_Seeker.open = true;
    }

    //*********************Async for logout*****************************
    class logout extends AsyncTask<Void, Void, Void> {
        String url;

        public logout(String type) {
            this.url = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("AuthCode: ", ""+prefs.getString("auth_code", null));
            String response = logout(prefs.getString("auth_code", null), url);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                Utils.pimageUri = null;
                MainActivity.profile = null;
                DashBoard_Seeker.open = false;
                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                startActivity(intent);
                getActivity().finish();
            } else {
                dialog("Alert!", "Some problem occurred!");
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                Utils.pimageUri = null;
                MainActivity.profile = null;
                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        }
    }

    //*************************************Update Method********************************************
    public String update(String authcode, String key, String value, String url) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode", authcode)
                .appendQueryParameter(key, value);
        Log.i("Complete", "Url: " + url + builder);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //*************************************Update Method********************************************
    public String update_name(String authcode, String fname, String lname, String url) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode", authcode)
                .appendQueryParameter("FirstName", fname)
                .appendQueryParameter("LastName", lname);
        Log.i("Complete", "Url: " + url + builder);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //*************************************Update Method********************************************
    public String update_pass(String authcode, String oldpass, String newpass, String url) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode", authcode)
                .appendQueryParameter("OldPassword", oldpass)
                .appendQueryParameter("NewPassword", newpass);
        Log.i("Complete", "Url: " + url + builder);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }


    //*************************************Logout Method********************************************
    public String logout(String authcode, String url) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode", authcode);
        Log.i("Complete", "Url: " + url + builder);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executePost();
        return response;
    }


    // Check Email exists or Not*****

    private class CheckEmailService extends AsyncTask<Void, Void, Void> {

        String status, message, text;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressEmail.setVisibility(View.VISIBLE);
            text = edt.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            String response = EmailCheck(text, Utils.CHECK_MAIL);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");
                Log.i("Status: ", "" + status);
                if (status.equals("true")) {
                    // message = obj.getString("data");
                } else {
                    //message = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {

                if (status.equals("true")) {
                    isEmail = true;
                    tick.setImageResource(R.drawable.green_tick);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt.setTextColor(Color.parseColor("#000000"));
                } else {
                    isEmail = false;
                    tick.setImageResource(R.drawable.red_cross);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt.setTextColor(Color.parseColor("#e53b36"));
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

    // ************************Email Check Method************************
    private String EmailCheck(String email, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("Email",
                email);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;

    }


    // ***********************Watch email text*****************
    public final TextWatcher EmailWatcher = new TextWatcher() {
        String email;
        private static final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            if (!con.isConnectingToInternet()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        getActivity());

                alert.setTitle("Internet connection unavailable.");
                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                alert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                alert.show();
            } else {
                new CheckEmailService().execute();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub
            email = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    private void delete_dialog() {
        // TODO Auto-generated method stub

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()
        );
        alert.setTitle("Alert!");
        alert.setMessage("Are you sure you want to delete your profile? This will delete all your information from our server");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                new logout(Utils.delete_account).execute();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        alert.show();

    }

}
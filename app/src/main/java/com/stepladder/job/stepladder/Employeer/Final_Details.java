package com.stepladder.job.stepladder.Employeer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Final_Details extends ActionBarActivity {
    RelativeLayout final_btn;
    LinearLayout ll_fixed, ll_hourly;
    TextView txt_signin_userregi, title;
    TextView jobduration, hour, to, btn_txt, txt_edt_to, txt_edt_from, txt_fixed, jobpos;
    EditText edt_jobtittle, edt_from, edt_to, edt_fixed, edt_job_type, edt_jobpos;
    Menu newmenu;
    AlertDialog.Builder builder;
    RestClient client;
    SharedPreferences prefs;
    String[] arr = {"Full Time", "Part Time"};
    String[] arr_pos = {"Temporary", "Permanent"};
    String[] arr_type = {"Per Annum", "Hourly"};
    int mile = -1;
    int pos = -1;
    int type = -1;
    String jobtittle, jobtype,job_pos, from, eto, status, selectedItem;
    LayoutInflater inflater;
    String euro = "\u20ac";
    String pound = "\u00a3";
    ConnectionDetector con;
    int i = 0;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_details);

        init();

        setFont();

        //Set Prefs Data

        if (prefs.getString("jobduration", null) != null) {
            edt_jobtittle.setText(prefs.getString("jobduration", null));
        }
        if (prefs.getString("job_pos", null) != null) {
            edt_jobpos.setText(prefs.getString("job_pos", null));
        }
        if (prefs.getString("job_type", null) != null) {
            edt_job_type.setText(prefs.getString("job_type", null));
            if (prefs.getString("job_type", null).equals("Hourly")) {
                ll_hourly.setVisibility(View.VISIBLE);
                ll_fixed.setVisibility(View.GONE);
                Log.i("From", "" + prefs.getString("from", null));
                if (prefs.getString("from", null) != "") {
                    edt_fixed.setText(String.valueOf(prefs.getString("from", null)));
                }
            } else if (prefs.getString("job_type", null).equals("Per Annum")) {
                ll_fixed.setVisibility(View.VISIBLE);
                ll_hourly.setVisibility(View.GONE);
                if (prefs.getString("from", null) != "") {
                    edt_from.setText(String.valueOf(prefs.getString("from", null)));
                }
                if (prefs.getString("to", null) != "") {
                    edt_to.setText(String.valueOf(prefs.getString("to", null)));
                }
            }
        } else {

        }

        clickevents();

    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Final Details");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        final_btn = (RelativeLayout) findViewById(R.id.final_btn);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        jobduration = (TextView) findViewById(R.id.jobduration);
        hour = (TextView) findViewById(R.id.hour);
        edt_jobtittle = (EditText) findViewById(R.id.edt_jobtittle);
        edt_from = (EditText) findViewById(R.id.edt_from);
        edt_to = (EditText) findViewById(R.id.edt_to);
        txt_edt_to = (TextView) findViewById(R.id.txt_edt_to);
        txt_edt_from = (TextView) findViewById(R.id.txt_edt_from);
        txt_edt_to.setText(pound);
        txt_edt_from.setText(pound);
        ll_fixed = (LinearLayout) findViewById(R.id.ll_fixed);
        ll_hourly = (LinearLayout) findViewById(R.id.ll_hourly);
        txt_fixed = (TextView) findViewById(R.id.txt_fixed);
        txt_fixed.setText(pound);
        edt_fixed = (EditText) findViewById(R.id.edt_fixed);
        edt_job_type = (EditText) findViewById(R.id.edt_job_type);
        edt_jobpos = (EditText) findViewById(R.id.edt_jobpos);
        jobpos = (TextView) findViewById(R.id.jobpos);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Company Final Details");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*********************************Setting Fonts for UI components******************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Final_Details.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Final_Details.this));
        jobduration.setTypeface(Utils.setfontlight(Final_Details.this));
        hour.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_jobtittle.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_from.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_to.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_fixed.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_job_type.setTypeface(Utils.setfontlight(Final_Details.this));
        edt_jobpos.setTypeface(Utils.setfontlight(Final_Details.this));
        jobpos.setTypeface(Utils.setfontlight(Final_Details.this));
    }

    //****************************************Clickevents Method************************************
    private void clickevents() {
        final_btn.setOnClickListener(new View.OnClickListener() {

                                         @Override
                                         public void onClick(View v) {
                                             // TODO Auto-generated method stub
                                             if (edt_jobtittle.length() == 0) {
                                                 edt_jobtittle.setError("Required field");
                                             } else if (edt_jobpos.length() == 0) {
                                                 edt_jobpos.setError("Required field");
                                             } else if (edt_job_type.length() == 0) {
                                                 edt_job_type.setError("Required field");
                                             } else {
                                                 jobtittle = edt_jobtittle.getText().toString()+" - "+edt_jobpos.getText().toString();
                                                 job_pos = edt_jobpos.getText().toString();
                                                 jobtype = edt_job_type.getText().toString();
                                                 if (jobtype.equals("Hourly")) {
                                                     eto = "";
                                                     if (edt_fixed.length() == 0) {
                                                         from = "";
                                                     } else {
                                                         from = edt_fixed.getText().toString();
                                                     }

                                                     //Connection Detection
                                                     if (!con.isConnectingToInternet()) {
                                                         AlertDialog.Builder alert = new AlertDialog.Builder(
                                                                 Final_Details.this);

                                                         alert.setTitle("Internet connection unavailable.");
                                                         alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                                         alert.setPositiveButton("OK",
                                                                 new DialogInterface.OnClickListener() {
                                                                     public void onClick(DialogInterface dialog,
                                                                                         int whichButton) {
                                                                         startActivity(new Intent(
                                                                                 Settings.ACTION_WIRELESS_SETTINGS));
                                                                     }
                                                                 });

                                                         alert.show();
                                                     } else {
                                                         new final_detail().execute();
                                                     }
                                                 } else if (jobtype.equals("Per Annum")) {
                                                     if (edt_from.length() == 0) {
                                                         from = "";
                                                         eto = "";

                                                         //Connection Detection
                                                         if (!con.isConnectingToInternet()) {
                                                             AlertDialog.Builder alert = new AlertDialog.Builder(
                                                                     Final_Details.this);

                                                             alert.setTitle("Internet connection unavailable.");
                                                             alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                                             alert.setPositiveButton("OK",
                                                                     new DialogInterface.OnClickListener() {
                                                                         public void onClick(DialogInterface dialog,
                                                                                             int whichButton) {
                                                                             startActivity(new Intent(
                                                                                     Settings.ACTION_WIRELESS_SETTINGS));
                                                                         }
                                                                     });

                                                             alert.show();
                                                         } else {
                                                             new final_detail().execute();
                                                         }
                                                     } else {
                                                         from = edt_from.getText().toString();
                                                         if (edt_to.length() == 0) {
                                                             edt_to.setError("Required field");
                                                         } else {
                                                             eto = edt_to.getText().toString();

                                                             //Connection Detection
                                                             if (!con.isConnectingToInternet()) {
                                                                 AlertDialog.Builder alert = new AlertDialog.Builder(
                                                                         Final_Details.this);

                                                                 alert.setTitle("Internet connection unavailable.");
                                                                 alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                                                                 alert.setPositiveButton("OK",
                                                                         new DialogInterface.OnClickListener() {
                                                                             public void onClick(DialogInterface dialog,
                                                                                                 int whichButton) {
                                                                                 startActivity(new Intent(
                                                                                         Settings.ACTION_WIRELESS_SETTINGS));
                                                                             }
                                                                         });

                                                                 alert.show();
                                                             } else {
                                                                 new final_detail().execute();
                                                             }
                                                         }
                                                     }
                                                 }
                                             }
                                         }
                                     }

        );
        edt_jobtittle.setOnClickListener(new View.OnClickListener()

                                         {
                                             @Override
                                             public void onClick(View v) {
                                                 MyDialogSingle(Final_Details.this, arr, "Duration", edt_jobtittle, mile, "");
                                             }
                                         }

        );

        edt_job_type.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick(View v) {
                                                MyDialogSingle(Final_Details.this, arr_type, "Salary", edt_job_type, type, "");
                                            }
                                        }

        );

        edt_jobpos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogSingle(Final_Details.this, arr_pos, "Type", edt_jobpos, pos, "");
            }
        });


        edt_to.setOnFocusChangeListener(new View.OnFocusChangeListener()

                                        {
                                            @Override
                                            public void onFocusChange(View v, boolean hasFocus) {
                                                edt_to.setText(edt_from.getText().toString());
                                            }
                                        }

        );


    }

    //************************************Final_Details AsyncTask Class*****************************
    class final_detail extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Final_Details.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving Info...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "AuthCode: " + prefs.getString("auth_code", null) + "JobId: " + prefs.getString("JobId", null) +
                    "JobTitle " + jobtittle + "Type: " + jobtype + "Min: " + from + "Max: " + eto);
            String response = finaldetail(prefs.getString("auth_code", null), prefs.getString("JobId", null), jobtittle, jobtype, from, eto);
            Log.i("Response final_details", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    String JobId = data.getString("JobId");
                    String message = data.getString("message");
                    Log.i("JobId***", ":" + JobId);
                    Log.i("message***", ":" + message);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                Intent i = new Intent(Final_Details.this,
                        Confirm_Job_Posting.class);
                ActivityStack.activity.add(Final_Details.this);
                startActivity(i);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("jobduration", edt_jobtittle.getText().toString());
                edit.putString("from", from);
                edit.putString("to", eto);
                edit.putString("job_type", jobtype);
                edit.putString("job_pos", job_pos);
                edit.commit();
                Log.i("Job Type: ", "" + prefs.getString("job_type", null));
            } else {
                dialog("Alert!", "Error in saving data.");
            }
        }
    }

    //**************************************finaldetail Method**************************************

    public String finaldetail(String authcode, String jobId, String jobDuration, String typed, String minamount, String maxamount) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", authcode)
                .appendQueryParameter("JobId", jobId)
                .appendQueryParameter("JobDuration", jobDuration)
                .appendQueryParameter("Typed", typed)
                .appendQueryParameter("MinAmount", minamount)
                .appendQueryParameter("MaxAmount", maxamount);
        Log.i("Complete", "Url: " + Utils.final_details + builder);
        client = new RestClient(Utils.final_details + builder);
        String response = null;
        response = client.executePost();
        return response;
    }

    //******************************************Alert Dialog****************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //***********************************************My Dialog**************************************
    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               final String title, final EditText select1, final int value,
                               final String string) {

        builder = new AlertDialog.Builder(Final_Details.this);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (title.equals("Salary")) {
                            type = which;
                            selectedItem = charSequences[which];
                            select1.setText(selectedItem);
                            dialog.dismiss();
                            if (selectedItem.equals("Per Annum")) {
                                ll_fixed.setVisibility(View.VISIBLE);
                                ll_hourly.setVisibility(View.GONE);
                            } else if (selectedItem.equals("Hourly")) {
                                ll_hourly.setVisibility(View.VISIBLE);
                                ll_fixed.setVisibility(View.GONE);
                            }
                        } else if (title.equals("Duration")) {
                            mile = which;
                            selectedItem = charSequences[which];
                            select1.setText(selectedItem);
                            dialog.dismiss();
                        } else if (title.equals("Type")) {
                            pos = which;
                            selectedItem = charSequences[which];
                            select1.setText(selectedItem);
                            dialog.dismiss();
                        }
                    }
                });
        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    public String deformat(String amount) {
        // TODO Auto-generated method stub
        String amt = null;
        try {

            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
            nf.setMinimumFractionDigits(0);
            Number num = nf.parse(amount);
            double dd = num.doubleValue();
            BigDecimal gg = new BigDecimal(dd).setScale(0);
            amt = gg.toString();

        } catch (ParseException p) {
            System.out.println(p);
            amt = amount;
        }
        return amt;
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

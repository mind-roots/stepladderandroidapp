package com.stepladder.job.stepladder.Model;

import java.util.ArrayList;

/**
 * Created by user on 12/16/2015.
 */
public class ModelUserProfile {
    public String name;
    public String bio;
    public String skills;
    public String profile_image;
    public String address;
    public String phone;
    public String email;
    public ArrayList<ModelUIData> past_work = new ArrayList<>();
}

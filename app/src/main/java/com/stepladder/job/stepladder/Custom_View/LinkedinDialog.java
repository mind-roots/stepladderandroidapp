package com.stepladder.job.stepladder.Custom_View;

import java.util.ArrayList;
import java.util.List;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Picture;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;


public class LinkedinDialog extends Dialog {

	public static LinkedInApiClientFactory factory;
	public static LinkedInOAuthService oAuthService;
	public static LinkedInRequestToken liToken;
	ProgressBar progress;



	public LinkedinDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);// must call before super.
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ln_dialog);
		progress = (ProgressBar)findViewById(R.id.progress);
		//setWebView();
        new setDialog().execute();
	}



    class setDialog extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            LinkedinDialog.oAuthService = LinkedInOAuthServiceFactory.getInstance()
                    .createLinkedInOAuthService(Utils.LINKEDIN_CONSUMER_KEY, Utils.LINKEDIN_CONSUMER_SECRET);
            LinkedinDialog.factory = LinkedInApiClientFactory.newInstance(Utils.LINKEDIN_CONSUMER_KEY,
                    Utils.LINKEDIN_CONSUMER_SECRET);
            //
            LinkedinDialog.liToken = LinkedinDialog.oAuthService.getOAuthRequestToken(Utils.OAUTH_CALLBACK_URL);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            WebView mWebView = (WebView) findViewById(R.id.webkitWebView1);
            mWebView.getSettings().setJavaScriptEnabled(true);

            Log.i("LinkedinSample", LinkedinDialog.liToken.getAuthorizationUrl());
            mWebView.loadUrl(LinkedinDialog.liToken.getAuthorizationUrl());
            mWebView.setWebViewClient(new HelloWebViewClient());
            progress.setVisibility(View.GONE);
			progress.setVisibility(View.VISIBLE);
        }
    }

	private void setWebView() {


	}



	class HelloWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.contains(Utils.OAUTH_CALLBACK_URL)) {
				Uri uri = Uri.parse(url);
				String verifier = uri.getQueryParameter("oauth_verifier");

				if(verifier == null){
					cancel();
				}


				for (OnVerifyListener d : listeners) {
					// call listener method
					d.onVerify(verifier);
				}
			} else if (url.contains("x-oauthflow-linkedin://callback?oauth_problem=user_refused")) {
				cancel();
			} else {
				Log.i("LinkedinSample", "url: " + url);
				view.loadUrl(url);
			}

			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			progress.setVisibility(View.GONE);
		}
	}


	private List<OnVerifyListener> listeners = new ArrayList<OnVerifyListener>();


	public void setVerifierListener(OnVerifyListener data) {
		listeners.add(data);
	}

	public interface OnVerifyListener {


		public void onVerify(String verifier);
	}
}

package com.stepladder.job.stepladder.Employeer_Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.code.linkedinapi.schema.Share;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.ContactPickerAdapter;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.Employeer.Company_Details;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Employeer.Job_Details;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Select_Location;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditEmployeerProfileFragment extends Fragment {
    View rootView;
    TextView txt_company, btn_txt, txt_add_logo;
    AutoCompleteTextView sector;
    public static EditText edt_location;
    ImageView img_logo;
    SharedPreferences prefs;
    ConnectionDetector con;
    ProgressBar progress;
    ArrayList<Person> userlist = new ArrayList<Person>();
    int position;
    android.support.v7.app.AlertDialog.Builder builder;
    String[] arr;
    int mile = -1;
    RelativeLayout lay_update_btn;
    String status, response, getsector, profile_image, selectedItem;
    ContactPickerAdapter adapter;
    ProgressBar progressBar;
    private Tracker mTracker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.edit_employeer_profile_fragment, container, false);

        init();

        setFonts();

        DashBoard_Employeer.newmenu.findItem(R.id.action_update).setVisible(false);


        //Set Company Image
        if (prefs.getString("profile_image", null) != null) {
            txt_add_logo.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(prefs.getString("profile_image", null)).noFade().rotate(0f, 0f, 0f)
                    .into(img_logo, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }

        //Get Intent Values
        if (prefs.getString("sector", null) != null) {
            sector.setText(prefs.getString("sector", null));
        }

        if (prefs.getString("area", null) != null) {
            edt_location.setText(prefs.getString("area", null));
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    getActivity());

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get Sectors
            userlist.clear();
            new getSectors().execute();
        }

        clickevents();

        DashBoard_Employeer.ic_add_job.setVisibility(View.GONE);

        return rootView;
    }

    //********************************************UI Initialization*********************************
    private void init() {
        //-------------------------Google Analytics initialization--------------------
        mTracker = ((OneSignalClass) getActivity().getApplication()).getDefaultTracker();
        con = new ConnectionDetector(getActivity());
        txt_company = (TextView) rootView.findViewById(R.id.txt_company);
        btn_txt = (TextView) rootView.findViewById(R.id.btn_txt);
        sector = (AutoCompleteTextView) rootView.findViewById(R.id.sector);
        edt_location = (EditText) rootView.findViewById(R.id.edt_location);
        img_logo = (ImageView) rootView.findViewById(R.id.img_logo);
        txt_add_logo = (TextView) rootView.findViewById(R.id.txt_add_logo);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        lay_update_btn = (RelativeLayout) rootView.findViewById(R.id.lay_update_btn);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        sector.clearFocus();
    }

    //********************************************Set Fonts*****************************************
    private void setFonts() {
        txt_company.setTypeface(Utils.setfontlight(getActivity()));
        btn_txt.setTypeface(Utils.setfontlight(getActivity()));
        sector.setTypeface(Utils.setfontlight(getActivity()));
        edt_location.setTypeface(Utils.setfontlight(getActivity()));
        txt_add_logo.setTypeface(Utils.setfontlight(getActivity()));
    }

    //********************************************Clickevents***************************************
    private void clickevents() {

        sector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogSingle(getActivity(), arr, "", sector, mile, "");
            }
        });

        img_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }

            }
        });

        edt_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Select_Location.class);
                ActivityStack.activity.add(getActivity());
                in.putExtra("id", "3");
                startActivity(in);
            }
        });

        lay_update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sector.length() == 0) {
                    sector.setError("Required Field");
                } else if (edt_location.length() == 0) {
                    edt_location.setError("Required Field");
                } else {
                    getsector = sector.getText().toString();
                    Utils.selected_loc = edt_location.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                getActivity());

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new company_details().execute();
                    }
                }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        mTracker.setScreenName("Edit Employer Profile");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*************************************Async Task Classes***************************************
    class getSectors extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Auth Code:", " " + prefs.getString("auth_code", null));
            RestClient client = new RestClient(Utils.sectors + "AuthCode=" + prefs.getString("auth_code", null) + "&Sector=");
            String response = client.executeGet();
            Log.i("Response_SECTOR:", " " + response);
            String r = null;
            r = client.executeGet();
            Log.i("RRRRRRR***** ", ":" + r);
            try {
                JSONObject jobj = new JSONObject(r);
                status = jobj.getString("status");
                Log.i("status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = jobj.getJSONArray("data");
                    Log.i("Data", ":" + data);
                    if (data.length() > 0) {
                        arr = new String[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jobj1 = data.getJSONObject(i);

                            String Name = jobj1.getString("Name");
                            System.out.println("Name:::Name::::" + Name);
                            String id = "0";
                            Person item = new Person(id, Name);
                            userlist.add(item);
                            arr[i] = Name;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                adapter = new ContactPickerAdapter(getActivity(), android.R.layout.simple_list_item_1, userlist);
                sector.setAdapter(adapter);
                DashBoard_Employeer.replace = true;
            } else {

            }
        }
    }

    class company_details extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating Details....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "AuthCode: " + prefs.getString("auth_code", null) + "Sector: " +
                    getsector + "LocationName: " + Utils.selected_loc + "Lat: " + Utils.lat + "Long: " + Utils.lng);
//            RestClient client = new RestClient(Utils.company_details + "AuthCode=" + prefs.getString("auth_code", null)
//                    + "&Sector=" + getsector + "&LocationName=" + Utils.selected_loc + "&Lat=" + Utils.lat + "&Long=" + Utils.lng);
            RestClient client = new RestClient(Utils.company_details + "AuthCode=" + prefs.getString("auth_code", null)
                    + "&Sector=" + getsector);
            Log.i("Image", "Path:" + Utils.cimageUri);
            if (Utils.cimageUri != null) {
                String image_path = getRealPathFromURI(getActivity(), Utils.cimageUri);
                response = client.postimage(image_path);
            } else {
                response = client.executePost();
            }
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.getString("profile_image");
                Log.i("profile_image:", "" + profile_image);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("sector", getsector);
                edit.putString("area", Utils.selected_loc);
                edit.putString("profile_image", profile_image);
                edit.commit();
                dialog("Success!", "Your employer profile has been updated successfully.");
            } else {
                dialog("Alert!", "Error in updating profile.");
            }
        }
    }

    // *******************************onActivityResult**********************************************
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Company_Details.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Company_Details.selected_image);
            Intent in = new Intent(getActivity(), CropActivity.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Log.i("imageUri: ", "" + Utils.cimageUri.getPath());
            img_logo.setImageURI(Utils.cimageUri);
            txt_add_logo.setVisibility(View.GONE);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }


    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getActivity().getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //***********************************************My Dialog**************************************
    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               String title, final EditText select1, final int value,
                               final String string) {

        builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.DialogTheme);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mile = which;
                        selectedItem = charSequences[which];
                        select1.setText(selectedItem);
                        dialog.dismiss();
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), permissions.toArray(new String[permissions.size()]), 100);
        } else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getActivity(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
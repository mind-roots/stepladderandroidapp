package com.stepladder.job.stepladder.ActivityTransition;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;
import com.stepladder.job.stepladder.Adapters.EmployeerCardAdapter;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Employeer_Fragments.DashboardEmployeerFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.ViewCurrentJobsFragment;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelGetJobs;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker_Fragments.DashboardSeekerFragment;
import com.stepladder.job.stepladder.SplashScreen;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by user on 12/2/2015.
 */
public class ParseReceiver extends ParsePushBroadcastReceiver {
    Intent activityIntent;
    SharedPreferences prefs;
    String Screen, type = "no", status;
    int match_jobs, quota;
    Context con;
    List<ActivityManager.RunningTaskInfo> tasks;
    Intent parentintent;
    Class<? extends Activity> cls;
    ConnectionDetector conn;
    RestClient client;
    public static boolean isPush = false;

    @Override
    public void onPushOpen(Context context, Intent intent) {
        Log.e("Push", "Clicked");
        con = context;
        parentintent = intent;
        conn = new ConnectionDetector(con);
        ParseAnalytics.trackAppOpenedInBackground(intent);

        boolean statust = isApplicationSentToBackground(context);
        prefs = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        Screen = prefs.getString("Screen", null);
        System.out.print("parse res=" + Screen);
        String uriString = null;
        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            Log.i("Response: ", "" + pushData);

            type = pushData.optString("type");
            Log.i("type: ", "" + type);
        } catch (JSONException e) {
            e.printStackTrace();
            type = "no";
        }
        cls = getActivity(context, intent);

        if (uriString != null && !uriString.isEmpty()) {
            activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
        } else {
            if (statust) {
                if (type.equals("employee-match")) {
                    getEmployeeopend();
                } else if (type.equals("employer-match")) {
                    getEmployeropend();
                } else {

                }
            } else {
                SplashScreen.push = true;
                activityIntent = new Intent(context, MainActivity.class);
                if (type.equals("employee-match")) {
                    activityIntent.putExtra("Intent_type", "employee-parse");
                } else if (type.equals("employer-match")) {
                    activityIntent.putExtra("Intent_type", "employer-parse");
                } else {
                    activityIntent.putExtra("Intent_type", "home");
                }
                activityIntent.putExtras(intent.getExtras());
                passIntent();
            }


        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        boolean statust = isApplicationSentToBackground(context);
        prefs = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        conn = new ConnectionDetector(con);
        parentintent = intent;
        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            Log.i("Response: ", "" + pushData);

            type = pushData.optString("type");
            Log.i("type: ", "" + type);
            if (type.equals("employee-match")) {
                if(DashBoard_Seeker.isHome) {
                    new getJobs().execute();
                    DashBoard_Seeker.isHome = false;
                }
            }else if(type.equals("employer-match")){
//                if(statust){
                    isPush = true;
//                }
                new get_jobs().execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            type = "no";
        }
    }

    private void passIntent() {
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(con);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            con.startActivity(activityIntent);
        }


    }

    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }

    private void getEmployeropend() {
        {
            for (ActivityManager.RunningTaskInfo aTask : tasks) {
                if (aTask.topActivity.getClassName().equals("com.stepladder.job.stepladder.Employeer.DashBoard_Employeer")) {
                    activityIntent = new Intent(con, DashBoard_Employeer.class);
                    activityIntent.putExtras(parentintent.getExtras());
                    passIntent();
                }
            }
        }
    }

    private void getEmployeeopend() {
        {
            for (ActivityManager.RunningTaskInfo aTask : tasks) {
                if (aTask.topActivity.getClassName().equals("com.stepladder.job.stepladder.Seeker.DashBoard_Seeker")) {
                    activityIntent = new Intent(con, DashBoard_Seeker.class);
                    activityIntent.putExtras(parentintent.getExtras());
                    activityIntent.putExtra("parse", "true");
                    passIntent();
                }
            }
        }
    }

    class getJobs extends AsyncTask<Void, Void, Void> {
        //ProgressDialog dia = new ProgressDialog(DashBoard_Seeker.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dia.setMessage("Loading....");
//            dia.setCancelable(false);
//            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            client = new RestClient(Utils.get_jobs + "AuthCode=" + prefs.getString("auth_code", null));
            String response = client.executeGet();
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                Log.i("Status: ", status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = obj.optJSONObject("data");
                    quota = data.optInt("quota");
                    match_jobs = data.optInt("matchjobs");
                    JSONArray jobs = data.optJSONArray("jobs");
                    Log.i("jobs array length: ", "" + jobs.length());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            dia.dismiss();
            if (status.equals("true")) {
                Log.i("Match Jobs: ", "" + match_jobs);
                if (match_jobs == 0) {
                    DashBoard_Seeker.ll_badge.setVisibility(View.GONE);
                    DashBoard_Seeker.ic_noti_grey.setVisibility(View.VISIBLE);
                } else {
                    DashBoard_Seeker.ll_badge.setVisibility(View.VISIBLE);
                    DashBoard_Seeker.counter.setText(String.valueOf(match_jobs));
                    DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
                }
            } else {
                //dialog("Alert!", "Authcode Mismatch");
            }
        }
    }

    class get_jobs extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ViewCurrentJobsFragment.job_list.clear();
           // progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = GetJobs(prefs.getString("auth_code", null), Utils.get_current_jobs);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");

                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = obj.getJSONArray("data");
                    Log.i("Data Array: ", "" + data);
                    for (int i = 0; i < data.length(); i++) {

                        JSONObject jobj = data.getJSONObject(i);
                        String job_id = jobj.getString("JobId");
                        String job_title = jobj.getString("JobTitle");
                        String Status = jobj.getString("Status");
                        String leads = jobj.getString("Leads");
                        String company_image = jobj.getString("Image");
                        JSONObject company_info = jobj.getJSONObject("CompanyInfo");
                        String company_name = company_info.getString("CompanyName");

                        //Add Data into Model
                        ModelGetJobs model_jobs = new ModelGetJobs();
                        model_jobs.job_id = job_id;
                        model_jobs.job_title = job_title;
                        model_jobs.job_status = Status;
                        model_jobs.leads = leads;
                        model_jobs.company_image = company_image;
                        model_jobs.company_name = company_name;

                        ViewCurrentJobsFragment.job_list.add(model_jobs);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
           // progressBar.setVisibility(View.GONE);
            if (status.equalsIgnoreCase("true")) {
                ViewCurrentJobsFragment.employeerCardAdapter.notifyDataSetChanged();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("first_job", false);
                editor.commit();
            }
        }
    }

    // ****************************************Get Jobs Method***********************************
    private String GetJobs(String auth_code, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode",
                auth_code);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

}
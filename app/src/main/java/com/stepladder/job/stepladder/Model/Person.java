package com.stepladder.job.stepladder.Model;

import java.io.Serializable;

public class Person implements Serializable {
	private String userid;
	private String username;

	public Person(String id, String n) {
		userid = id;
		username = n;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	

	@Override
	public String toString() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}

package com.stepladder.job.stepladder.Seeker;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.gson.Gson;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.ModelUIcomponents;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.zip.Inflater;

public class Past_Jobs extends ActionBarActivity {
    RelativeLayout lay_pastjobs;
    ScrollView scrollView;
    LinearLayout add_new_role;
    ImageView img;
    TextView txt_add_another, txt_remove_role, btn_txt, where_txt, role_txt, title;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date1;
    DatePickerDialog.OnDateSetListener date2;
    Menu newmenu;
    String currentDate, selectedDate, status, json_string, edit;
    LayoutInflater inflater;
    ArrayList<ModelUIcomponents> ui_arr = new ArrayList<ModelUIcomponents>();
    int i = 0, curent_pos = 0;
    boolean add = false;
    RestClient client;
    SharedPreferences prefs;
    ConnectionDetector con;
    java.util.Date start_date;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.past_jobs);

        //Get Intent
        Intent intent = getIntent();
        edit = intent.getStringExtra("edit");

        init();

        setFont();

        ui_arr.clear();
        add_new_role.removeAllViews();
        Log.i("Size: ", "" + Utils.final_ui_data.size());
        if (edit.equals("true")) {
            DashBoard_Seeker.open = true;
            if (Utils.final_ui_data.size() != 0) {
                for (int i = 0; i < Utils.final_ui_data.size(); i++) {
                    if (Utils.final_ui_data.size() > 1) {
                        sendScroll();
                        txt_remove_role.setVisibility(View.VISIBLE);
                    }

                    add_view(i);
                    if (ui_arr.size() != 0) {
                        for (int j = 0; j < ui_arr.size(); j++) {
                            ui_arr.get(j).job_title.setText(Utils.final_ui_data.get(j).JobTitle);
                            ui_arr.get(j).job_title.setSelection(ui_arr.get(j).job_title.length());
                            ui_arr.get(j).company.setText(Utils.final_ui_data.get(j).Company);
                            ui_arr.get(j).city.setText(Utils.final_ui_data.get(j).City);
                            if (Utils.final_ui_data.get(j).CurrentPosition.equals("0")) {
                                ui_arr.get(j).start_date.setText(Utils.final_ui_data.get(j).StartDate);
                                ui_arr.get(j).end_date.setText(Utils.final_ui_data.get(j).EndDate);
                            } else {
                                ui_arr.get(j).end_date.setVisibility(View.INVISIBLE);
                                ui_arr.get(j).current_pos.setChecked(true);
                                ui_arr.get(j).start_date.setText(Utils.final_ui_data.get(j).StartDate);
                            }
                        }
                    }
                }
            } else {
                add_view(0);
            }
        } else {
            if (Utils.final_ui_data.size() != 0) {
                for (int i = 0; i < Utils.final_ui_data.size(); i++) {
                    if (Utils.final_ui_data.size() > 1) {
                        sendScroll();
                        txt_remove_role.setVisibility(View.VISIBLE);
                    }

                    add_view(i);
                    if (ui_arr.size() != 0) {
                        for (int j = 0; j < ui_arr.size(); j++) {
                            ui_arr.get(j).job_title.setText(Utils.final_ui_data.get(j).JobTitle);
                            ui_arr.get(j).job_title.setSelection(ui_arr.get(j).job_title.length());
                            ui_arr.get(j).company.setText(Utils.final_ui_data.get(j).Company);
                            ui_arr.get(j).city.setText(Utils.final_ui_data.get(j).City);
                            if (Utils.final_ui_data.get(j).CurrentPosition.equals("0")) {
                                ui_arr.get(j).start_date.setText(Utils.final_ui_data.get(j).StartDate);
                                ui_arr.get(j).end_date.setText(Utils.final_ui_data.get(j).EndDate);
                            } else {
                                ui_arr.get(j).end_date.setVisibility(View.INVISIBLE);
                                ui_arr.get(j).current_pos.setChecked(true);
                                ui_arr.get(j).start_date.setText(Utils.final_ui_data.get(j).StartDate);
                            }
                        }
                    }
                }
            } else {
                add_view(0);
            }
        }

        clickevents();

    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Past Jobs");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        lay_pastjobs = (RelativeLayout) findViewById(R.id.lay_pastjobs);
        txt_add_another = (TextView) findViewById(R.id.txt_add_another);
        add_new_role = (LinearLayout) findViewById(R.id.add_new_role);
        txt_remove_role = (TextView) findViewById(R.id.txt_remove_role);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        where_txt = (TextView) findViewById(R.id.where_txt);
        role_txt = (TextView) findViewById(R.id.role_txt);
        img = (ImageView) findViewById(R.id.img);

        if (edit.equals("true")) {
            btn_txt.setText("Update Past Jobs");
            img.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Seeker Past Jobs");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Past_Jobs.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Past_Jobs.this));
        where_txt.setTypeface(Utils.setfontlight(Past_Jobs.this));
        role_txt.setTypeface(Utils.setfontlight(Past_Jobs.this), Typeface.BOLD);
        txt_add_another.setTypeface(Utils.setfontlight(Past_Jobs.this));
        txt_remove_role.setTypeface(Utils.setfontlight(Past_Jobs.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        lay_pastjobs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                for (int i = 0; i < ui_arr.size(); i++) {
                    EditText job_title = (EditText) ui_arr.get(i).job_title;
                    EditText company = (EditText) ui_arr.get(i).company;
                    EditText city = (EditText) ui_arr.get(i).city;
                    EditText start_date = (EditText) ui_arr.get(i).start_date;
                    EditText end_date = (EditText) ui_arr.get(i).end_date;

                    if (end_date.getVisibility() == View.INVISIBLE) {
                        if (job_title.length() > 0 && company.length() > 0 && city.length() > 0
                                && start_date.length() > 0) {
                            add = true;
                        } else {
                            add = false;
                            break;
                        }
                    } else if (end_date.getVisibility() == View.VISIBLE) {
                        if (job_title.length() > 0 && company.length() > 0 && city.length() > 0
                                && start_date.length() > 0 && end_date.length() > 0) {
                            add = true;
                        } else {
                            add = false;
                            break;
                        }
                    }
                }

                if (add) {
                    Utils.final_ui_data.clear();
                    for (int i = 0; i < ui_arr.size(); i++) {
                        EditText job_title = (EditText) ui_arr.get(i).job_title;
                        EditText company = (EditText) ui_arr.get(i).company;
                        EditText city = (EditText) ui_arr.get(i).city;
                        EditText start_date = (EditText) ui_arr.get(i).start_date;
                        EditText end_date = (EditText) ui_arr.get(i).end_date;
                        final Switch current_pos = (Switch) ui_arr.get(i).current_pos;

                        if (current_pos.isChecked()) {
                            curent_pos = 1;
                        } else {
                            curent_pos = 0;
                        }

                        ModelUIData UiData = new ModelUIData();
                        UiData.JobTitle = job_title.getText().toString();
                        UiData.Company = company.getText().toString();
                        UiData.City = city.getText().toString();
                        UiData.CurrentPosition = String.valueOf(curent_pos);
                        UiData.StartDate = start_date.getText().toString();
                        if (end_date.getText().toString().equals(null)) {
                            UiData.EndDate = "";
                        } else {
                            UiData.EndDate = end_date.getText().toString();
                        }
                        Utils.final_ui_data.add(UiData);
                        json_string = new Gson().toJson(Utils.final_ui_data);
                        Log.i("List: ", "" + json_string);
                    }

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Past_Jobs.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new past_work().execute();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Complete the required fields", Toast.LENGTH_SHORT).show();
                }

            }
        });
        txt_add_another.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   sendScroll();
                                                   for (int i = 0; i < ui_arr.size(); i++) {
                                                       EditText job_title = (EditText) ui_arr.get(i).job_title;
                                                       EditText company = (EditText) ui_arr.get(i).company;
                                                       EditText city = (EditText) ui_arr.get(i).city;
                                                       EditText start_date = (EditText) ui_arr.get(i).start_date;
                                                       EditText end_date = (EditText) ui_arr.get(i).end_date;
                                                       if (end_date.getVisibility() == View.INVISIBLE) {
                                                           if (job_title.length() > 0 && company.length() > 0 && city.length() > 0
                                                                   && start_date.length() > 0) {
                                                               add = true;
                                                           } else {
                                                               add = false;
                                                               break;
                                                           }
                                                       } else if (end_date.getVisibility() == View.VISIBLE) {
                                                           if (job_title.length() > 0 && company.length() > 0 && city.length() > 0
                                                                   && start_date.length() > 0 && end_date.length() > 0) {
                                                               add = true;
                                                           } else {
                                                               add = false;
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   if (add) {
                                                       txt_remove_role.setVisibility(View.VISIBLE);
                                                       add_view(ui_arr.size());
                                                   } else {
                                                       Toast.makeText(getApplicationContext(), "Complete the required fields", Toast.LENGTH_SHORT).show();
                                                   }
                                               }
                                           }

        );

        txt_remove_role.setOnClickListener(new View.OnClickListener()

                                           {
                                               @Override
                                               public void onClick(View v) {
                                                   add_new_role.removeViewAt(add_new_role.getChildCount() - 1);
                                                   add_new_role.invalidate();
                                                   ui_arr.remove(ui_arr.size() - 1);
                                                   if (add_new_role.getChildCount() == 1) {
                                                       txt_remove_role.setVisibility(View.GONE);
                                                   }
                                               }
                                           }

        );

    }

    //***************************************Add View Method****************************************
    public void add_view(int pos) {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.job_detail_info, null);
        LinearLayout ll_title = (LinearLayout) view.findViewById(R.id.ll_title);
        if (pos == 0) {
            ll_title.setVisibility(View.GONE);
        } else {
            ll_title.setVisibility(View.INVISIBLE);
        }

        final EditText job_title = (EditText) view.findViewById(R.id.edt_job_title);
        final EditText company = (EditText) view.findViewById(R.id.edt_company);
        final EditText city = (EditText) view.findViewById(R.id.edt_city);
        final EditText start_date = (EditText) view.findViewById(R.id.edt_startdate);
        final EditText end_date = (EditText) view.findViewById(R.id.edt_enddate);
        final Switch current_pos = (Switch) view.findViewById(R.id.current_pos);
        final RelativeLayout rl_current_pos = (RelativeLayout) view.findViewById(R.id.rl_current_pos);
        TextView txt_current = (TextView) view.findViewById(R.id.txt_current);
        txt_current.setTypeface(Utils.setfontlight(Past_Jobs.this), Typeface.BOLD);
        job_title.setTypeface(Utils.setfontlight(Past_Jobs.this));
        company.setTypeface(Utils.setfontlight(Past_Jobs.this));
        city.setTypeface(Utils.setfontlight(Past_Jobs.this));
        start_date.setTypeface(Utils.setfontlight(Past_Jobs.this));
        end_date.setTypeface(Utils.setfontlight(Past_Jobs.this));

        current_pos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    curent_pos = 1;
                    end_date.setVisibility(View.INVISIBLE);
                } else {
                    curent_pos = 0;
                    end_date.setVisibility(View.VISIBLE);
                }
            }
        });

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate(start_date);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDate(end_date);
            }
        });


        final ModelUIcomponents UIModel = new ModelUIcomponents();
        // UIModel.lay_name = "lay" + pos;
        UIModel.job_title = job_title;
        UIModel.company = company;
        UIModel.city = city;
        UIModel.start_date = start_date;
        UIModel.end_date = end_date;
        UIModel.current_pos = current_pos;

        add_new_role.addView(view);
        ui_arr.add(UIModel);

    }

    //*********************************************Scroll to bootom*********************************
    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }

    //*****************************************Select Date Method***********************************
    public void startDate(final EditText editText) {
        myCalendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("d MMM,yy");
        currentDate = df.format(myCalendar.getTime());
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Date date = new Date(year - 1900, monthOfYear, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yy");
                selectedDate = sdf.format(date);
                java.util.Date sdate = null, cdate = null;
                try {
                    sdate = sdf.parse(selectedDate);
                    cdate = sdf.parse(currentDate);
                    Log.i("Selected: ", "" + sdate + "Current: " + cdate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // int selectedyear = myCalendar.get(Calendar.YEAR);
                if (sdate.after(cdate)) {
                    Toast.makeText(getApplicationContext(), "Selected date cannot be future date!", Toast.LENGTH_SHORT).show();
                } else {
                    editText.setText(sdf.format(date));
                    start_date = date;
                }
            }

        };
        new DatePickerDialog(Past_Jobs.this, date1, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();


    }

    public void endDate(final EditText editText) {
        myCalendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("d MMM,yy");
        currentDate = df.format(myCalendar.getTime());
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Date date = new Date(year - 1900, monthOfYear, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yy");
                selectedDate = sdf.format(date);
                java.util.Date sdate = null, cdate = null;
                try {
                    sdate = sdf.parse(selectedDate);
                    cdate = sdf.parse(currentDate);
                    Log.i("Selected: ", "" + sdate + "Current: " + cdate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // int selectedyear = myCalendar.get(Calendar.YEAR);
                if (sdate.after(cdate)) {
                    Toast.makeText(getApplicationContext(), "Selected date cannot be future date!", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        if (sdate.before(start_date)) {
                            Toast.makeText(getApplicationContext(), "End date can't be before to start date", Toast.LENGTH_SHORT).show();
                        } else {
                            editText.setText(sdf.format(date));
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Select Start Date first", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        };
        new DatePickerDialog(Past_Jobs.this, date1, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();


    }

    //*************************************Past Job AsyncTask Class*********************************
    class past_work extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Past_Jobs.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving Info...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = PastJobs(prefs.getString("auth_code", null), json_string);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                if (edit.equals("true")) {
                    dialog("Success!", "Data updated successfully");
                } else {
                    Intent i = new Intent(Past_Jobs.this, Skill_set.class);
                    i.putExtra("edit", "false");
                    ActivityStack.activity.add(Past_Jobs.this);
                    startActivity(i);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("confirmed","0");
                    editor.commit();
                }
            } else {
                dialog("Alert!", "Error in saving data!");
            }
        }
    }


    //***************************************Skill Set Method***************************************
    public String PastJobs(String auth_code, String data) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("data", data);
        Log.i("Complete", "Url: " + Utils.past_jobs + builder);
        client = new RestClient(Utils.past_jobs + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        if (edit.equals("true")) {
            newmenu.findItem(R.id.action_expand).setVisible(false);
            newmenu.findItem(R.id.action_skip).setVisible(false);
        } else {
            newmenu.findItem(R.id.action_expand).setVisible(false);
            newmenu.findItem(R.id.action_skip).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_skip) {
            Utils.final_ui_data.clear();
            Intent i = new Intent(Past_Jobs.this, Skill_set.class);
            i.putExtra("edit", "false");
            ActivityStack.activity.add(Past_Jobs.this);
            startActivity(i);
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }
}

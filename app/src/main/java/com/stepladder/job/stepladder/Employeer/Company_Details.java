package com.stepladder.job.stepladder.Employeer;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.model.LatLng;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.ContactPickerAdapter;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Select_Location;
import com.stepladder.job.stepladder.Utils.GpsTracker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Company_Details extends ActionBarActivity {
    Menu newmenu;
    public static EditText edt_location;
    LinearLayout lay_location_edit, ll_add_logo;
    RelativeLayout lay_location_btn, rl_location;
    ImageView img_location;
    public static ImageView img_logo;
    TextView txt_add_logo, title, textView1, btn_txt;
    LayoutInflater inflater;
    public static double latitude, longitude;
    public static LatLng MyLocation;
    GpsTracker gps;
    ContactPickerAdapter adapter;
    EditText sector;
    ArrayList<Person> userlist = new ArrayList<Person>();
    Geocoder geocoder;
    List<Address> addresses;
    SharedPreferences prefs;
    public static Uri selected_image;
    int position;
    android.support.v7.app.AlertDialog.Builder builder;
    String[] arr;
    int mile = -1;
    String selectedItem, city, state, country, knownName, status, response, getsector,
            profile_image, email_noti, app_noti, confirmed;
    ProgressBar progress;
    ConnectionDetector con;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_detail);

        init();

        setFont();

        //Set Company Image
        if (Utils.cimageUri != null) {
            txt_add_logo.setVisibility(View.GONE);
            img_logo.setImageURI(Utils.cimageUri);
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Company_Details.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get Current Location
            new location().execute();
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Company_Details.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get Sectors
            new getSectors().execute();
        }

        clickevents();


    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
//        actionBar.setHomeButtonEnabled(false);
//        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Company Details");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        lay_location_btn = (RelativeLayout) findViewById(R.id.lay_location_btn);
        ll_add_logo = (LinearLayout) findViewById(R.id.ll_add_logo);
        img_logo = (ImageView) findViewById(R.id.img_logo);
        txt_add_logo = (TextView) findViewById(R.id.txt_add_logo);
        gps = new GpsTracker(Company_Details.this);
        geocoder = new Geocoder(this, Locale.getDefault());
        img_location = (ImageView) findViewById(R.id.img_location);
        sector = (EditText) findViewById(R.id.sector);
        edt_location = (EditText) findViewById(R.id.edt_location);
        progress = (ProgressBar) findViewById(R.id.progress);
        rl_location = (RelativeLayout) findViewById(R.id.rl_set_location);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        textView1 = (TextView) findViewById(R.id.textView1);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Company Details");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //********************************Setting Fonts for UI components*******************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Company_Details.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Company_Details.this));
        textView1.setTypeface(Utils.setfontlight(Company_Details.this));
        sector.setTypeface(Utils.setfontlight(Company_Details.this));
        edt_location.setTypeface(Utils.setfontlight(Company_Details.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        sector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogSingle(Company_Details.this, arr, "", sector, mile, "");
            }
        });


        lay_location_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (sector.length() == 0) {
                    sector.setError("Required Field");
                } else if (edt_location.length() == 0) {
                    edt_location.setError("Required Field");
                } else {
                    getsector = sector.getText().toString();
                    Utils.selected_loc = edt_location.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Company_Details.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new company_details().execute();
                    }
                }
            }
        });

        ll_add_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }

            }
        });

        img_logo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });
        edt_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Company_Details.this, Select_Location.class);
                ActivityStack.activity.add(Company_Details.this);
                in.putExtra("id", "2");
                in.putExtra("current_loc", Utils.selected_loc);
                startActivity(in);
            }
        });
    }

    //*************************************Async Task Classes***************************************
    class location extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //***********************************Get Current Location*******************************
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                MyLocation = new LatLng(latitude, longitude);
                Utils.lat = latitude;
                Utils.lng = longitude;
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                    String address = addresses.get(0).getAddressLine(0);
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();
                    System.out.println("Location" + state + "," + country);

                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Unable to get location",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.setVisibility(View.GONE);
            if (gps.canGetLocation()) {
                edt_location.setText(knownName + ", " + city);
                Utils.selected_loc = knownName + ", " + city;
            } else {
                Toast.makeText(getApplicationContext(), "Unable to get location",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    class company_details extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Company_Details.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving Info....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "AuthCode: " + prefs.getString("auth_code", null) + "Sector: " +
                    getsector + "LocationName: " + Utils.selected_loc + "Lat: " + Utils.lat + "Long: " + Utils.lng);
//            RestClient client = new RestClient(Utils.company_details + "AuthCode=" + prefs.getString("auth_code", null)
//                    + "&Sector=" + getsector + "&LocationName=" + Utils.selected_loc + "&Lat=" + Utils.lat + "&Long=" + Utils.lng);
            RestClient client = new RestClient(Utils.company_details + "AuthCode=" + prefs.getString("auth_code", null)
                    + "&Sector=" + getsector);

            Log.i("Image", "Path:" + Utils.cimageUri);
            if (Utils.cimageUri != null) {
                String image_path = getRealPathFromURI(getApplicationContext(), Utils.cimageUri);
                response = client.postimage(image_path);
            } else {
                response = client.executePost();
            }
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.getString("profile_image");
                Log.i("profile_image:", "" + profile_image);
                email_noti = data.getString("Match_Email_Notification");
                app_noti = data.getString("Match_App_Notification");
                //confirmed = data.optString("Confirmed");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("sector", getsector);
                edit.putString("area", Utils.selected_loc);
                edit.putString("profile_image", profile_image);
                edit.putString("email_noti", email_noti);
                edit.putString("app_noti", app_noti);
                edit.putString("confirmed", "1");
                edit.commit();
                Intent i = new Intent(Company_Details.this, Job_Details.class);
                ActivityStack.activity.add(Company_Details.this);
                startActivity(i);
            } else {
                dialog("Alert!", "Error in saving data.");
            }
        }
    }

    class getSectors extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Company_Details.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Auth Code:", " " + prefs.getString("auth_code", null));
            RestClient client = new RestClient(Utils.sectors + "AuthCode=" + prefs.getString("auth_code", null) + "&Sector=");
            String response = client.executeGet();
            Log.i("Response_SECTOR:", " " + response);
            String r = null;
            r = client.executeGet();
            Log.i("RRRRRRR***** ", ":" + r);
            try {
                JSONObject jobj = new JSONObject(r);
                status = jobj.getString("status");
                Log.i("status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = jobj.getJSONArray("data");
                    Log.i("Data", ":" + data);
                    if (data.length() > 0) {
                        arr = new String[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jobj1 = data.getJSONObject(i);

                            String Name = jobj1.getString("Name");
                            System.out.println("Name:::Name::::" + Name);
                            String id = "0";
                            Person item = new Person(id, Name);
                            userlist.add(item);
                            arr[i] = Name;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
//                adapter = new ContactPickerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, userlist);
//                autoCompleteTextView_sector.setAdapter(adapter);

            } else {
                //dialog("Alert!", error);
            }
        }
    }

    //***********************************************My Dialog**************************************
    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               String title, final EditText select1, final int value,
                               final String string) {

        builder = new android.support.v7.app.AlertDialog.Builder(Company_Details.this, R.style.DialogTheme);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mile = which;
                        selectedItem = charSequences[which];
                        select1.setText(selectedItem);
                        dialog.dismiss();
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + selected_image);
            Intent in = new Intent(Company_Details.this, CropActivity.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Log.i("imageUri: ", "" + selected_image);
            img_logo.setImageURI(Utils.cimageUri);
            txt_add_logo.setVisibility(View.GONE);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

        }

        return false;
    }

    //***************************************Back Press Method**************************************
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(Company_Details.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(Company_Details.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(Company_Details.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

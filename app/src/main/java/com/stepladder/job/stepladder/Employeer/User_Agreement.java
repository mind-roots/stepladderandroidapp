package com.stepladder.job.stepladder.Employeer;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 1/16/2016.
 */
public class User_Agreement extends ActionBarActivity {
    TextView txt_agreement, title;
    Menu newmenu;
    LayoutInflater inflater;
    String agreement_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_agreement);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Terms and Conditions");
        title.setTypeface(Utils.setfontlight(User_Agreement.this), Typeface.BOLD);

        txt_agreement = (TextView) findViewById(R.id.txt_agreement);
        txt_agreement.setTypeface(Utils.setfontlight(User_Agreement.this));

        agreement_text = "StepLadderApp Terms and Conditions"+
                "\n" +
                "\n"+
                "\n"+
                "Each time you access or use our Website (www.stepladderapp.com) and/or our mobile device application (collectively the \"Website\"), you are deemed to accept these terms and conditions.\n" +
                "\n" +
                "\n"+
                "\"we\" means StepLadderApp a trading style of Adam Butler Limited and Tinyyo Limited, (Joint Venture) and \"our\" shall be construed accordingly. \"you\" means the person firm company or organisation browsing and/or using the Website, and \"your\" shall be construed accordingly. “Tinyyo Ltd” and \"Adam Butler Limited\" means all companies connected with us." +
                "\n" +
                "Interruptions and Omissions in Service\n" +
                "\n" +
                "Whilst we try to ensure that the standard of the Website remains high and to maintain the continuity of it, the internet is not an inherently stable medium, and errors, omissions, interruptions of service and delays may occur at any time. We do not accept any liability arising from any such errors, omissions, interruptions or delays or any ongoing obligation or responsibility to operate the Website (or any particular part of it) or to provide the service offered on the Website. We may vary the specification of this site from time to time without notice.\n" +
                "\n" +
                "Links to other Sites\n" +
                "\n" +
                "On this site you may be offered automatic links to other sites which we hope will be of interest to you. We do not accept any responsibility for or liability in respect of the content of those sites, the owners of which do not necessarily have any connection, commercial or otherwise, with us. Using automatic links to gain access to such sites is entirely at your own risk.\n" +
                "\n" +
                "Information on this Site\n" +
                "\n" +
                "Whilst we make every effort to ensure that the information on our Website is accurate and complete, some of the information is supplied to us by third parties and we are not able to check the accuracy or completeness of that information. We do not accept any liability arising from any inaccuracy or omission in any of the information on our Website or any liability in respect of information on the Website supplied by you, any other website user or any other person.\n" +
                "\n" +
                "Your Use of this Site\n" +
                "\n" +
                "You may only use the Website for lawful purposes when seeking employment or help with your career, or when recruiting staff. You must not under any circumstances seek to undermine the security of the Website or any information submitted to or available through it. In particular, but without limitation, you must not seek to access, alter or delete any information to which you do not have authorised access, seek to overload the system via spamming or flooding, take any action or use any device, routine or software to crash, delay, damage or otherwise interfere with the operation of the Website or attempt to decipher, disassemble or modify any of the software, coding or information comprised in the Website.\n" +
                "\n" +
                "You are solely responsible for any information submitted by you to the Website. You are responsible for ensuring that all information supplied by you is true, accurate, up-to-date and not misleading or likely to mislead or deceive and that it is not discriminatory, obscene, offensive, defamatory or otherwise illegal, unlawful or in breach of any applicable legislation, regulations, guidelines or codes of practice or the copyright, trade mark or other intellectual property rights of any person in any jurisdiction. You are also responsible for ensuring that all information, data and files are free of viruses or other routines or engines that may damage or interfere with any system or data prior to being submitted to the Website. We reserve the right to remove any information supplied by you from the Website at our sole discretion, at any time and for any reason without being required to give any explanation." +
                "\n" +
                "Information Submitted by You\n" +
                "\n" +
                "We will and/or Adam Butler Limited/Tinyyo Ltd will use information supplied by you (including, without limitation, sensitive personal data) to aid the recruitment process and associated administrative functions as well as to facilitate the purchase of training courses made through this website. This involves us and/or Adam Butler Limited, amongst other things, processing and storing information (including, without limitation, sensitive personal data) and passing or making available online such information to prospective employers, course providers and clients; information about courses, vacancies and placements will be passed to candidates and potential purchasers of courses and may be posted directly onto the Website. We use third parties to help us process your information as part of the recruitment and sales process. We may collect and aggregate data from the information supplied by you to help us to understand our users as a group so that we can provide you with a better service. We may also share aggregate information with selected third parties, without disclosing individual names or identifying information. You consent to us and Adam Butler Limited using information provided by you (including, without limitation, sensitive personal data) in each of these ways.\n" +
                "Please note that all Third Party recruitment agencies have agreed to our Terms and Conditions and should they be found to be in breach of the Terms and Conditions, they will be prevented from using our services.\n" +
                "We will process any data which you provide in completing the online registration or application forms and any further forms, assessments or personal details which you complete or provide to us when using the Website in accordance with UK data protection legislation. \n" +
                "Transfer outside the EEA: Personal information comprising your CV may be accessed through the database held by Adam Butler Limited/Tinyyo Ltd by third parties outside the European Economic Area (\"EEA\"). This could happen for instance if you apply for a vacancy where the employer is based outside the EEA. By registering and using the Website, you consent to this transfer.\n" +
                "\n" +
                "Terms of Business\n" +
                "\n" +
                "Recruitment: Each employment assignment or placement arising as a result of an introduction made by Adam Butler Limited will be subject to our standard Terms of Business as they are applicable in the circumstances. All prospective employers and clients for whom we arrange assignments or placements will be provided with a copy of the Terms of Business applicable to them at or following registration.\n" +
                "If you are one of our first 50 clients, and are using the app or website to promote your vacancies, then there will be no charge for use during the first 12 months, and no charge for use beyond that timeframe until agreed by both parties.\n" +
                "\n" +
                "Termination\n" +
                "\n" +
                "We may terminate your registration and/or deny you access to the Website or any part of it (including any services, goods or information available on or through the Website) at any time in our absolute discretion and without any explanation or notification.\n" +
                "\n" +
                "Content Rights\n" +
                "\n" +
                "The rights in material on the Website are protected by international copyright, software and trademark laws and you agree to use the Website in a way which does not infringe these rights. You may copy material on the Website for your own private or domestic purposes, but no copying for any commercial or business use is permitted.\n" +
                "\n" +
                "Security and Passwords\n" +
                "\n" +
                "In order to register with the Website and to sign in when you visit the Website, you will need to use a user name and password. You are solely responsible for the security and proper use of your password, which should be kept confidential at all times and not disclosed to any other person. You must notify us immediately if you believe that your password is known to someone else or if it may be used in an unauthorised way. We accept no liability for any unauthorised or improper use or disclosure of any password.\n" +
                "\n" +
                "\n" +
                "Liability\n" +
                "\n" +
                "We accept no liability for any loss (whether direct or indirect, for loss of business, revenue or profits, wasted expenditure, corruption or destruction of data or for any other indirect or consequential loss whatsoever) arising from your use of the Website and we hereby exclude any such liability, whether in contract, tort (including for negligence) or otherwise. We hereby exclude all representations, warranties and conditions relating to the Website and your use of it to the maximum extent permitted by law.\n" +
                "You agree to indemnify us and keep us indemnified against all costs, expenses, claims, losses, liabilities or proceedings arising from use or misuse by you of the Website.\n" +
                "You must notify us immediately if anyone makes or threatens to make any claim against you relating to your use of the Website.\n" +
                "\n" +
                "Choice of Law and Jurisdiction\n" +
                "\n" +
                "The use of the Website and any agreements entered into through the Website are to be governed by and construed in accordance with English law. The courts of England are to have exclusive jurisdiction to settle any dispute arising out of or in connection with the use of the Website or any agreement made through the Website.\n" +
                "Changes to Terms and Conditions and Invalidity\n" +
                "\n" +
                "These terms and conditions may be changed by us at any time. You will be deemed to accept the terms and conditions (as amended) when you next use the Website following any amendment.\n" +
                "If any provision of these terms and conditions is held to be invalid by a court of competent jurisdiction, such invalidity shall not affect the validity of the remaining provisions, which shall remain in full force and effect.\n" +
                "Registered Office\n" +
                "\n" +
                "Adam Butler Limited\n" +
                "\n" +
                "61 Rodney Street, Liverpool, L1 9ER\n" +
                "\n" +
                "Company Number. 5975305    Registered in the UK.\n" +
                "\n" +
                "\n" +
                "Tinyyo Limited\n" +
                "314 Main Road, Parson Drove, Wisbech, Cambridgeshire, England, PE13 4LF\n" +
                "\n" +
                "Company Number 09462584  Registered in the UK.\n";
        //txt_agreement.setText(Html.fromHtml(agreement_text));
     //   txt_agreement.setText(agreement_text);


    }

    //***********************************onCreateOptionsMenu****************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

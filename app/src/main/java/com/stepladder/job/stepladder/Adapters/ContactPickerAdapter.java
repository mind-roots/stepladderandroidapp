package com.stepladder.job.stepladder.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.R;


public class ContactPickerAdapter extends ArrayAdapter<Person> implements
		Filterable {

	private ArrayList<Person> contactList, cloneContactList;
	private LayoutInflater layoutInflater;


	public ContactPickerAdapter(Context context, int textViewResourceId,
			ArrayList<Person> userlist) {
		super(context, textViewResourceId);
		this.contactList = userlist;
		this.cloneContactList = (ArrayList<Person>) this.contactList.clone();
		layoutInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {

		return contactList.size();
	}

	@Override
	public Person getItem(int position) {

		return contactList.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.contact_picker_item,
					null);
			holder = new Holder();
			holder.list_add_txt = (TextView) convertView
					.findViewById(R.id.list_add_txt);
			// holder.phone = (TextView) convertView.findViewById(R.id.phone);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		Person contact = getItem(position);
		holder.list_add_txt.setText(contact.getUsername());
		return convertView;
	}

	@Override
	public Filter getFilter() {
		Filter contactFilter = new Filter() {


			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if (results.values != null) {
					contactList = (ArrayList<Person>) results.values;
					notifyDataSetChanged();
				}

			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				String sortValue = constraint == null ? "" : constraint
						.toString().toLowerCase();
				FilterResults filterResults = new FilterResults();
				if (!TextUtils.isEmpty(sortValue.trim())) {
					ArrayList<Person> sortedContactList = new ArrayList<Person>();
					for (Person contact : cloneContactList) {
						if (contact.getUserid().toLowerCase().contains(sortValue)
								|| contact.getUsername().toLowerCase()
										.contains(sortValue))
							sortedContactList.add(contact);
					}

					filterResults.values = sortedContactList;
					filterResults.count = sortedContactList.size();

				}
				return filterResults;
			}

			@Override
			public CharSequence convertResultToString(Object resultValue) {
				// need to save this to saved contact
				return ((Person) resultValue).getUsername();
			}
		};

		return contactFilter;
	}

	@SuppressWarnings("unchecked")
	public void setContactList(ArrayList<Person> contactList) {
		// this isn't the efficient method
		// need to improvise on this
		this.contactList = contactList;
		this.cloneContactList = (ArrayList<Person>) this.contactList.clone();
		notifyDataSetChanged();
	}

	public static class Holder {
		public TextView list_add_txt;
	}

}

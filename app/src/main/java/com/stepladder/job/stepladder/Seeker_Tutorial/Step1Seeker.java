package com.stepladder.job.stepladder.Seeker_Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.R;

/**
 * Created by user on 1/18/2016.
 */
public class Step1Seeker extends Activity implements Animation.AnimationListener,GestureDetector.OnGestureListener {
    ImageView img_hand,img_arrow,img_tick_green,img_main_bg;
    GestureDetector gd;
    public static String replay = "no";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.step1seeker);

        img_hand = (ImageView)findViewById(R.id.img_hand);
        img_arrow = (ImageView)findViewById(R.id.img_arrow);
        img_main_bg = (ImageView)findViewById(R.id.img_main_bg);
        img_tick_green = (ImageView)findViewById(R.id.img_tick_green);
        gd = new GestureDetector(this, this);

        TranslateAnimation animation = new TranslateAnimation(0, 180, 0, 0);
        animation.setDuration(2000);
        animation.setFillAfter(false);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setAnimationListener(this);

        img_hand.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_right));

        img_arrow.startAnimation(animation);


    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // TODO Auto-generated method stub
        //Defining Sensitivity
        float sensitivity = 200;
        if (e1.getY() - e2.getY() > sensitivity) {
           // Toast.makeText(getApplicationContext(), "down", Toast.LENGTH_LONG).show();

            return true;
        } else if (e2.getY() - e1.getY() > sensitivity) {
           // Toast.makeText(getApplicationContext(), "up", Toast.LENGTH_LONG).show();

            return true;
        }
        //Swipe SecondRight Check
        else if (e1.getX() - e2.getX() > sensitivity) {
           // Toast.makeText(getApplicationContext(), "right", Toast.LENGTH_LONG).show();

            return true;
        } else if (e2.getX() - e1.getX() > sensitivity) {
            //Toast.makeText(getApplicationContext(), "left", Toast.LENGTH_LONG).show();
            //img_tick_green.setVisibility(View.VISIBLE);
            img_main_bg.setImageResource(R.drawable.right_screen);
            Intent i = new Intent(Step1Seeker.this, Step2Seeker.class);
           // ActivityStack.activity.add(Step1Seeker.this);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            return true;
        } else {
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gd.onTouchEvent(event);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        img_hand.clearAnimation();
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(img_hand.getWidth(), img_hand.getHeight());
        lp.setMargins(0, 0, 0, 0);
        img_hand.setLayoutParams(lp);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
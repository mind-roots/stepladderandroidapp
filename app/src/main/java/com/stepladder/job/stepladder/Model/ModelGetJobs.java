package com.stepladder.job.stepladder.Model;

/**
 * Created by user on 12/5/2015.
 */
public class ModelGetJobs {
    public String job_id;
    public String job_title;
    public String company_image;
    public String company_name;
    public String leads;
    public String job_status;
    public String hold_id;
    public String hold_name;
}

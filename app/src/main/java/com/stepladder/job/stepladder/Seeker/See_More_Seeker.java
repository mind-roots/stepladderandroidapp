package com.stepladder.job.stepladder.Seeker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.CropActivityP;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step1Seeker;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step4Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/27/2015.
 */
public class See_More_Seeker extends ActionBarActivity {
    RelativeLayout rl_lets_go;
    ImageView image_pick;
    ImageView img_confirm_profile;
    ProgressBar progress;
    String current_role, status, profile_image,response,confirmed;
    TextView title, btn_txt, txt_name, txt_address, txt_skills, txt_bio, title_skills, title_bio, title_past;
    LayoutInflater inflater;
    //public static ArrayList<ModelJobsCard> get_jobs = new ArrayList<>();
    LinearLayout ll_past_jobs, ll_past_line;
    Menu newmenu;
    SharedPreferences prefs;
    ConnectionDetector con;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.see_more_seeker);
        if (Build.VERSION.SDK_INT < 21) {

        } else {
            setupWindowAnimations();
        }

        //UI Initialization
        init();

        if (Confirm_Profile.edit.equals("true")) {
            btn_txt.setText("Update Profile");
            rl_lets_go.setVisibility(View.INVISIBLE);
            DashBoard_Seeker.open = true;
        } else {
            btn_txt.setText(Confirm_Profile.btn_txt.getText().toString());
        }

        //Set Fonts
        setFont();

        //Set Prefs Values
        if (prefs.getString("first_name", null) != null && prefs.getString("last_name", null) != null) {
            txt_name.setText(prefs.getString("first_name", null) + " " + prefs.getString("last_name", null));
        }
        if (prefs.getString("area", null) != null) {
            txt_address.setText(prefs.getString("area", null));
        }
        if (prefs.getString("skills", null) != null) {
            txt_skills.setText(prefs.getString("skills", null));
        }
        if (prefs.getString("bio", null) != null) {
            txt_bio.setText(prefs.getString("bio", null));
        }

        //Set Profile Image
        if (MainActivity.profile != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(MainActivity.profile).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        } else if (Utils.pimageUri != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().
                    placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        } else if (prefs.getString("profile_image", null) != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        }

        //Inflate Past Jobs
        if (Utils.final_ui_data.size() != 0) {
            title_past.setVisibility(View.VISIBLE);
            ll_past_line.setVisibility(View.VISIBLE);
            for (int i = 0; i < Utils.final_ui_data.size(); i++) {
                Log.i("Current Position", "" + Utils.final_ui_data.get(i).CurrentPosition);
                if (Utils.final_ui_data.get(i).CurrentPosition.equals("1")) {
                    current_role = "Current Role Since: "+Utils.final_ui_data.get(i).StartDate;
                    add_view(Utils.final_ui_data.get(i).JobTitle, Utils.final_ui_data.get(i).Company
                            , current_role, Utils.final_ui_data.get(i).City);
                } else if (Utils.final_ui_data.get(i).CurrentPosition.equals("0")) {
                    current_role = Utils.final_ui_data.get(i).StartDate + " - " + Utils.final_ui_data.get(i).EndDate;
                    add_view(Utils.final_ui_data.get(i).JobTitle, Utils.final_ui_data.get(i).Company
                            , current_role, Utils.final_ui_data.get(i).City);
                }

            }

        } else {
            title_past.setVisibility(View.INVISIBLE);
            ll_past_line.setVisibility(View.INVISIBLE);
        }

        //Click Method
        clickevents();

    }


    //****************************************UI Initializations************************************
    private void init() {
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Confirm Profile");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        rl_lets_go = (RelativeLayout) findViewById(R.id.rl_lets_go);
        image_pick = (ImageView) findViewById(R.id.image_pick);
        img_confirm_profile = (ImageView) findViewById(R.id.img_confirm_profile);
        progress = (ProgressBar) findViewById(R.id.progress);
        txt_name = (TextView) findViewById(R.id.txt_name);
        ll_past_jobs = (LinearLayout) findViewById(R.id.ll_past_jobs);
        txt_address = (TextView) findViewById(R.id.txt_address);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        txt_bio = (TextView) findViewById(R.id.txt_bio);
        title_past = (TextView) findViewById(R.id.title_past);
        title_bio = (TextView) findViewById(R.id.title_bio);
        title_skills = (TextView) findViewById(R.id.title_skills);
        ll_past_line = (LinearLayout) findViewById(R.id.ll_past_line);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
    }

    //*********************************Setting Fonts for UI components******************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(See_More_Seeker.this), Typeface.BOLD);
        txt_name.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        txt_address.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        txt_skills.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        txt_bio.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        title_bio.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        title_skills.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        title_past.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        btn_txt.setTypeface(Utils.setfontlight(See_More_Seeker.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        rl_lets_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (Confirm_Profile.change_image) {

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                See_More_Seeker.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new Change_Image().execute();
                    }
            }
        });

        image_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        img_confirm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

    }

    //****************************************Transition Methods************************************
    @SuppressLint("NewApi")
    private void setupWindowAnimations() {
        Transition transition;
        transition = buildEnterTransition();
        getWindow().setEnterTransition(transition);
    }

    @SuppressLint("NewApi")
    private Transition buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }

    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Profile_photo.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Profile_photo.selected_image);
            Intent in = new Intent(See_More_Seeker.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            MainActivity.profile = null;
            Confirm_Profile.change_image = true;
            rl_lets_go.setVisibility(View.VISIBLE);
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().transform(new RoundedTransformation(2))
                    .placeholder(R.drawable.ic_user_new)
                    .rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    // **************************************Confirm Job Details************************************
    class Change_Image extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.confirm_profile + "AuthCode=" + prefs.getString("auth_code", null));
            //RestClient client = new RestClient(Utils.confirm_profile);
            if (Confirm_Profile.change_image) {
                Log.i("Image", "Path:" + Utils.pimageUri);
                String image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
                response = client.postimage(image_path);
                Confirm_Profile.change_image = false;
            }else {
                response = client.executePost();
            }

            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.optString("profile_image");
                confirmed = data.optString("Confirmed");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {
                if (Confirm_Profile.edit.equals("true")) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("type", "job-seeker");
                    editor.putString("profile_image", profile_image);
                    editor.putString("confirmed",confirmed);
                    editor.commit();
                    dialog("Success!", "Your employee profile has been updated successfully");
                    Confirm_Profile.edit = "false";
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("type", "job-seeker");
                    editor.putString("profile_image", profile_image);
                    editor.putString("confirmed",confirmed);
                    editor.commit();
                    Intent i = new Intent(See_More_Seeker.this, Step1Seeker.class);
                    i.putExtra("parse", "false");
                    ActivityStack.activity.add(See_More_Seeker.this);
                    startActivity(i);
                }
            } else {
                dialog("Alert!", "Error in uploading image!");
            }
        }
    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //***************************************Add View Method****************************************
    public void add_view(String job_title, String company, String current, String location) {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.past_job_info, null);
        final TextView txt_job_title = (TextView) view.findViewById(R.id.txt_job_title);
        final TextView txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);
        final TextView txt_current_role = (TextView) view.findViewById(R.id.txt_current_role);
        final TextView txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_job_title.setTypeface(Utils.setfontlight(See_More_Seeker.this), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        txt_current_role.setTypeface(Utils.setfontlight(See_More_Seeker.this));
        txt_location.setTypeface(Utils.setfontlight(See_More_Seeker.this));

        final ModelUIPastJobs UIModel = new ModelUIPastJobs();
        UIModel.txt_job_title = txt_job_title;
        UIModel.txt_company_name = txt_company_name;
        UIModel.txt_current_role = txt_current_role;
        UIModel.txt_location = txt_location;

        UIModel.txt_job_title.setText(job_title);
        UIModel.txt_company_name.setText(company);
        UIModel.txt_current_role.setText(current);
        UIModel.txt_location.setText(location);

        ll_past_jobs.addView(view);

    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(See_More_Seeker.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(See_More_Seeker.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(See_More_Seeker.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

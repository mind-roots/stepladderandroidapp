package com.stepladder.job.stepladder.Adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Employeer.Job_Status;
import com.stepladder.job.stepladder.Employeer.See_More_Employeer;
import com.stepladder.job.stepladder.Employeer_Fragments.DashboardEmployeerFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.ViewCurrentJobsFragment;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelGetJobs;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 10/20/2015.
 */
public class EmployeerCardAdapter extends RecyclerView.Adapter<EmployeerCardAdapter.MHolder> {
    LayoutInflater inflater;
    Context context;
    ArrayList<ModelGetJobs> job_list;
    ArrayList<ModelGetJobs> hold = new ArrayList<>();
    RestClient client;
    SharedPreferences prefs;
    String status, data;
    Dialog screenDialog;
    ConnectionDetector con;
    MHolder holder;

    public EmployeerCardAdapter(Context ctx, ArrayList<ModelGetJobs> getJobs) {
        this.context = ctx;
        inflater = LayoutInflater.from(ctx);
        this.job_list = getJobs;
        prefs = context.getSharedPreferences("user_data", context.MODE_PRIVATE);
        con = new ConnectionDetector(context);
        hold.clear();
    }

    @Override
    public MHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.employeer_cardview, parent, false);
        holder = new MHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MHolder holder, final int position) {
        final ModelGetJobs jobs = job_list.get(position);

        //check if job status is pending
        if (jobs.job_status.equals("pending")) {
            holder.ll_buttons.setVisibility(View.GONE);
            holder.txt_pending_approval.setVisibility(View.VISIBLE);
            holder.txt_rejected.setVisibility(View.GONE);
        } else if (jobs.job_status.equals("rejected")) {
            holder.ll_buttons.setVisibility(View.GONE);
            holder.txt_pending_approval.setVisibility(View.GONE);
            holder.txt_rejected.setVisibility(View.VISIBLE);
        } else if (jobs.job_status.equals("on_hold")) {
            holder.ll_buttons.setVisibility(View.VISIBLE);
            holder.txt_pending_approval.setVisibility(View.GONE);
            holder.txt_rejected.setVisibility(View.GONE);
            holder.txt_hold.setText("Un Hold");
        } else {
            holder.ll_buttons.setVisibility(View.VISIBLE);
            holder.txt_pending_approval.setVisibility(View.GONE);
            holder.txt_rejected.setVisibility(View.GONE);
            holder.txt_hold.setText("Hold Job");
        }


        //set data in card list
        holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context).load(jobs.company_image).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                .into(holder.img_Employer, new Callback() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub
                        holder.progressBar.setVisibility(View.GONE);
                    }
                });
        holder.txt_company_name.setText(jobs.job_title);
        if (jobs.leads.equals("1")) {
            holder.txt_leads.setText(jobs.leads + " Lead");
        } else {
            holder.txt_leads.setText(jobs.leads + " Leads");
        }

        holder.img_Employer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new get_job_details(jobs.job_id).execute();
            }
        });


        holder.ll_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, Job_Status.class);
                in.putExtra("job_id", jobs.job_id);
                in.putExtra("job_title", jobs.job_title);
                context.startActivity(in);
            }
        });

        holder.ll_jobfilled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            context);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    context.startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    alert_dialog("Job filled", "This will remove your job post and stop any new leads from applying", jobs.job_id, position);
                }
            }
        });

        holder.ll_hold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.txt_hold.getText().toString().equals("Hold Job")) {
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                context);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        context.startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        alert_dialog("Putting job on hold", "Your job will be put on hold and you will not receive any new leads until you take it off hold",
                                jobs.job_id, position);
                    }
                } else if (holder.txt_hold.getText().toString().equals("Un Hold")) {
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                context);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        context.startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        alert_dialog("Taking your job off hold", "Your job will be reactivated and will be live for new applicants to apply", jobs.job_id, position);
                    }
                }

            }
        });

        holder.txt_pending_approval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog("Pending Approval", "Your job is currently pending approval. Check back in a few hours to see it live!");
            }
        });

        holder.txt_rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog("Rejected", "Your job has been rejected. Please check your email for further instructions.");
            }
        });

        holder.rl_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.txt_pending_approval.getVisibility() == View.VISIBLE) {
                    ShowDialog("Pending Approval", "Your job is currently pending approval. Check back in a few hours to see it live!");
                } else if (holder.txt_rejected.getVisibility() == View.VISIBLE) {
                    ShowDialog("Rejected", "Your job has been rejected. Please check your email for further instructions.");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return job_list.size();
    }


    class MHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout ll_leads, ll_jobfilled, ll_hold, ll_buttons;
        TextView txt_hold, txt_job_filled, txt_leads, txt_company_name, txt_pending_approval, txt_rejected;
        RelativeLayout rl_card;
        ImageView img_Employer;
        ProgressBar progressBar;

        public MHolder(View v) {
            super(v);
            ll_leads = (LinearLayout) v.findViewById(R.id.ll_leads);
            ll_jobfilled = (LinearLayout) v.findViewById(R.id.ll_jobfilled);
            ll_hold = (LinearLayout) v.findViewById(R.id.ll_hold);
            img_Employer = (ImageView) v.findViewById(R.id.img_Employer);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            txt_company_name = (TextView) v.findViewById(R.id.txt_company_name);
            txt_hold = (TextView) v.findViewById(R.id.txt_hold);
            txt_job_filled = (TextView) v.findViewById(R.id.txt_job_filled);
            txt_leads = (TextView) v.findViewById(R.id.txt_leads);
            txt_company_name.setTypeface(Utils.setfontlight(context));
            txt_hold.setTypeface(Utils.setfontlight(context));
            txt_job_filled.setTypeface(Utils.setfontlight(context));
            txt_leads.setTypeface(Utils.setfontlight(context));
            rl_card = (RelativeLayout) v.findViewById(R.id.rl_card);
            txt_pending_approval = (TextView) v.findViewById(R.id.txt_pending_approval);
            txt_rejected = (TextView) v.findViewById(R.id.txt_rejected);
            ll_buttons = (LinearLayout) v.findViewById(R.id.ll_buttons);
        }

        @Override
        public void onClick(View v) {

        }
    }

    //*****************************************Async Task Class*************************************
    class job_status extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(context);
        String jobid, job_status;
        int position;

        public job_status(String job_id, String status, int pos) {
            this.jobid = job_id;
            this.job_status = status;
            this.position = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating job status...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = SetStatus(prefs.getString("auth_code", null), jobid, job_status, Utils.job_status);
            Log.i("Response: ", response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.optString("status");
                data = jobj.optString("data");
                Log.i("Status: ", "" + status + "Data: " + data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                if (job_status.equals("closed")) {
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                context);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        context.startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        job_list.remove(position);
                        ViewCurrentJobsFragment.job_list.clear();
                        new get_jobs().execute();
                    }
                } else if (job_status.equals("on_hold")) {
                    job_list.get(position).job_status = "on_hold";
                    notifyDataSetChanged();
                } else if (job_status.equals("active")) {
                    job_list.get(position).job_status = "active";
                    notifyDataSetChanged();
                }
            } else {
                dialog("Alert!", data);
            }
        }
    }

    // ****************************************Get Jobs Method***********************************
    private String SetStatus(String auth_code, String jobid, String status, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode", auth_code)
                .appendQueryParameter("JobId", jobid)
                .appendQueryParameter("Status", status);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //*******************************************Async Task Class***********************************
    class get_jobs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ViewCurrentJobsFragment.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = GetJobs(prefs.getString("auth_code", null), Utils.get_current_jobs);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");

                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = obj.getJSONArray("data");
                    Log.i("Data Array: ", "" + data);
                    for (int i = 0; i < data.length(); i++) {

                        JSONObject jobj = data.getJSONObject(i);
                        String job_id = jobj.getString("JobId");
                        String job_title = jobj.getString("JobTitle");
                        String Status = jobj.getString("Status");
                        String leads = jobj.getString("Leads");
                        String company_image = jobj.getString("Image");
                        JSONObject company_info = jobj.getJSONObject("CompanyInfo");
                        String company_name = company_info.getString("CompanyName");

                        //Add Data into Model
                        ModelGetJobs model_jobs = new ModelGetJobs();
                        model_jobs.job_id = job_id;
                        model_jobs.job_title = job_title;
                        model_jobs.job_status = Status;
                        model_jobs.leads = leads;
                        model_jobs.company_image = company_image;
                        model_jobs.company_name = company_name;

                        ViewCurrentJobsFragment.job_list.add(model_jobs);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ViewCurrentJobsFragment.progressBar.setVisibility(View.GONE);
            if (status.equalsIgnoreCase("true")) {
                notifyDataSetChanged();
                if (job_list.size() == 0) {
                    ViewCurrentJobsFragment.current_jobs.setVisibility(View.GONE);
                    ViewCurrentJobsFragment.ll_no_jobs.setVisibility(View.VISIBLE);
                }
            } else {
                ViewCurrentJobsFragment.current_jobs.setVisibility(View.GONE);
                ViewCurrentJobsFragment.ll_no_jobs.setVisibility(View.VISIBLE);
            }
        }
    }

    // ****************************************Get Jobs Method***********************************
    private String GetJobs(String auth_code, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode",
                auth_code);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }


    /**************************************
     * Get Job Details
     *****************************************/
    class get_job_details extends AsyncTask<Void, Void, Void> {
        String job_id, response;

        public get_job_details(String jobId) {
            this.job_id = jobId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ViewCurrentJobsFragment.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = GetJobDetails(prefs.getString("auth_code", null), job_id);
            Log.i("Response: ", response);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ViewCurrentJobsFragment.progressBar.setVisibility(View.GONE);
            try {
                JSONObject jobj = new JSONObject(response);
                String status = jobj.optString("status");
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.optJSONObject("data");
                    String JobTitle = data.optString("JobTitle");
                    String JobSkills = data.optString("JobSkills");
                    String JobDescription = data.optString("JobDescription");
                    String JobDuration = data.optString("JobDuration");
                    String MinAmount = data.optString("MinAmount");
                    String MaxAmount = data.optString("MaxAmount");
                    String Image = data.optString("Image");
                    String CompanyName = data.optString("CompanyName");
                    String JobType = data.optString("Typed");
                    String LocationName = data.optString("LocationNamej");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("profile_image", Image);
                    editor.putString("company_name", CompanyName);
                    editor.putString("area", LocationName);
                    editor.putString("jobtitle", JobTitle);
                    editor.putString("jobdescription", JobDescription);
                    editor.putString("skills", JobSkills);
                    editor.putString("jobduration", JobDuration);
                    editor.putString("job_type", JobType);
                    editor.putString("from", MinAmount);
                    editor.putString("to", MaxAmount);
                    editor.commit();
                    Intent in = new Intent(context, See_More_Employeer.class);
                    in.putExtra("intent_type", "job_details");
                    context.startActivity(in);
                } else {

                }
            } catch (Exception e) {

            }

        }
    }

    /*************************************
     * Get Job Details Method
     ***********************************/
    private String GetJobDetails(String auth_code, String job_id) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode",
                auth_code)
                .appendQueryParameter("JobId",
                        job_id);
        client = new RestClient(Utils.get_job_details + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    public void ShowDialog(final String title, String value) {
        // TODO Auto-generated method stub
        screenDialog = new Dialog(context, R.style.PauseDialog);
        screenDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme1;
        screenDialog.show();
        screenDialog.setContentView(R.layout.emp_dialog);
        TextView textView_sub = (TextView) screenDialog.findViewById(R.id.textView_sub);
        TextView txt_decs = (TextView) screenDialog.findViewById(R.id.txt_decs);
        RelativeLayout lay_ok_btn = (RelativeLayout) screenDialog.findViewById(R.id.lay_ok_btn);
        TextView btn_ok = (TextView) screenDialog.findViewById(R.id.btn_ok);

        textView_sub.setText(title);
        txt_decs.setText(value);
        textView_sub.setTypeface(Utils.setfontlight(context), Typeface.BOLD);
        txt_decs.setTypeface(Utils.setfontlight(context), Typeface.BOLD);
        btn_ok.setTypeface(Utils.setfontlight(context));

        lay_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenDialog.dismiss();
            }
        });

    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    public void alert_dialog(final String title, final String msg, final String job_id, final int pos) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("");
        alert.setMessage(msg);
        alert.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (title.equals("Job filled")) {
                    dialog.dismiss();
                    new job_status(job_id, "closed", pos).execute();
                } else if (title.equals("Putting job on hold")) {
                    dialog.dismiss();
                    //Change job status n button text
                    new job_status(job_id, "on_hold", pos).execute();
                    holder.txt_hold.setText("Un Hold");
                } else if (title.equals("Taking your job off hold")) {
                    dialog.dismiss();
                    //Change job status n button text
                    new job_status(job_id, "active", pos).execute();
                    holder.txt_hold.setText("Hold Job");
                }

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        alert.show();
    }

}

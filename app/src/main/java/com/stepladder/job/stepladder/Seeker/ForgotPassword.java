package com.stepladder.job.stepladder.Seeker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

/**
 * Created by user on 2/10/2016.
 */
public class ForgotPassword extends ActionBarActivity {
    TextView textView1, textView2, btn_txt, title;
    EditText edt_email;
    RelativeLayout lay_user_login;
    ProgressBar progressBar;
    RestClient client;
    LayoutInflater inflater;
    Menu newmenu;
    ConnectionDetector con;
    String email, status,msg;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);

        init();


        setFont();

        clickevents();

    }

    //****************************************UI Initializations************************************
    private void init() {
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Forgot Password");
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        edt_email = (EditText) findViewById(R.id.edt_email);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        lay_user_login = (RelativeLayout) findViewById(R.id.lay_user_login);
    }

    //**********************************Setting Fonts for UI components*****************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(ForgotPassword.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(ForgotPassword.this));
        textView1.setTypeface(Utils.setfontlight(ForgotPassword.this));
        textView2.setTypeface(Utils.setfontlight(ForgotPassword.this));
        edt_email.setTypeface(Utils.setfontlight(ForgotPassword.this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Forgot Password");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        lay_user_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_email.length() == 0) {
                    edt_email.setError("Please enter your email address");
                } else {
                    email = edt_email.getText().toString();
                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                ForgotPassword.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new forgot_pass().execute();
                    }
                }
            }
        });
    }

    //*******************************************Forgot Async Class*********************************
    public class forgot_pass extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            client = new RestClient(Utils.forgot_pass + "Email=" + email);
            String response = client.executePost();
            Log.i("Response", ":" + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                status = jsonObject.optString("status");
                msg = jsonObject.optString("data");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {
                dialog("Success",msg);
            } else {
                dialog("Error!",msg);
            }
        }
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(final String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                if(title.equals("Success")){
                                    finish();
                                }else {
                                    dialog.dismiss();
                                }
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }


    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

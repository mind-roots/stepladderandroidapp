package com.stepladder.job.stepladder.Seeker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Employeer.Employeer_Registration;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.SplashScreen;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 10/27/2015.
 */
public class Sign_In extends ActionBarActivity {
    RelativeLayout lay_user_login;
    TextView txt_joinnow, btn_txt, textView2, textView1, title, txt_forgot;
    EditText edt_email, edt_password;
    SharedPreferences prefs;
    RestClient client;
    LayoutInflater inflater;
    String error, status, email, password, login, type, company_name, first_name,
            last_name, dob, phone, profile_image, email_address, sector, location,
            email_noti, app_noti, skills, bio, distance, confirmed, android_id;
    Menu newmenu;
    ConnectionDetector con;
    //public static ArrayList<ModelJobsCard> get_jobs = new ArrayList<>();
    ProgressBar progressBar;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        init();

        //----------------get player id from ONESIGNAL fo notification--------
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                android_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + android_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });

        setFont();

        Intent intent = getIntent();
        login = intent.getStringExtra("login");

        clickevents();

    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        lay_user_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_email.length() == 0) {
                    edt_email.setError("Invalid email");
                } else if (edt_password.length() == 0) {
                    edt_password.setError("Invalid password");
                } else {
                    email = edt_email.getText().toString();
                    password = edt_password.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Sign_In.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new log_in().execute();
                    }
                }
            }
        });

        txt_joinnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.equals("employeer")) {
                    finish();
                } else if (login.equals("user")) {
                    finish();
                } else if (login.equals("main")) {
                    finish();
                }
            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Sign_In.this, ForgotPassword.class);
                ActivityStack.activity.add(Sign_In.this);
                startActivity(i);
            }
        });

    }

    //****************************************UI Initializations************************************
    private void init() {
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Sign In");
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        lay_user_login = (RelativeLayout) findViewById(R.id.lay_user_login);
        txt_joinnow = (TextView) findViewById(R.id.txt_joinnow);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txt_forgot = (TextView) findViewById(R.id.txt_forgot);

    }

    //**********************************Setting Fonts for UI components*****************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Sign_In.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Sign_In.this));
        textView1.setTypeface(Utils.setfontlight(Sign_In.this));
        textView2.setTypeface(Utils.setfontlight(Sign_In.this));
        edt_email.setTypeface(Utils.setfontlight(Sign_In.this));
        edt_password.setTypeface(Utils.setfontlight(Sign_In.this));
        txt_joinnow.setTypeface(Utils.setfontlight(Sign_In.this));
        txt_forgot.setTypeface(Utils.setfontlight(Sign_In.this));
    }

    //*******************************************Login Async Class**********************************
    class log_in extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = login(email, password, android_id, "android");
            Log.i("Response", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    String auth_code = data.getString("AuthCode");
                    Log.i("AuthCode", ":" + auth_code);
                    type = data.getString("Type");
                    Log.i("Type", ":" + type);
                    company_name = data.optString("CompanyName");
                    first_name = data.getString("FirstName");
                    last_name = data.getString("LastName");
                    dob = data.getString("Dob");
                    email_address = data.getString("Email");
                    profile_image = data.getString("profile_image");
                    phone = data.getString("Phone_number");
                    sector = data.optString("Sector");
                    location = data.optString("LocationName");
                    confirmed = data.optString("Confirmed");

                    //Get Skills
                    JSONArray skills = data.optJSONArray("Skills");
                    if (skills != null) {
                        for (int i = 0; i < skills.length(); i++) {
                            JSONObject jobj1 = skills.getJSONObject(i);
                            String Id = jobj1.getString("Id");
                            String Name = jobj1.getString("Name");
                            System.out.println("Name:::Name::::" + Name);
                            Person item = new Person(Id, Name);
                            Utils.userlist.add(item);
                        }
                    }
                    bio = data.optString("Biography");
                    distance = data.optString("DistanceLookingFor");

                    //Get Past Work
                    JSONArray Userspastwork = data.optJSONArray("Userspastwork");
                    for (int i = 0; i < Userspastwork.length(); i++) {
                        JSONObject obj = Userspastwork.getJSONObject(i);
                        ModelUIData UIdata = new ModelUIData();
                        UIdata.JobTitle = obj.getString("JobTitle");
                        UIdata.Company = obj.getString("Company");
                        UIdata.City = obj.getString("City");
                        UIdata.CurrentPosition = obj.getString("CurrentPosition");
                        UIdata.StartDate = obj.getString("StartDate");
                        UIdata.EndDate = obj.optString("EndDate");

                        Utils.final_ui_data.add(UIdata);
                        Log.i("Size: ", "" + Utils.final_ui_data.size());
                    }
                    email_noti = data.optString("Match_Email_Notification");
                    app_noti = data.optString("Match_App_Notification");

                    //Add Values to prefs
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putString("auth_code", auth_code);
                    edit.putString("type", type);
                    edit.putString("company_name", company_name);
                    edit.putString("first_name", first_name);
                    edit.putString("last_name", last_name);
                    edit.putString("dob", dob);
                    edit.putString("email", email_address);
                    edit.putString("profile_image", profile_image);
                    edit.putString("phone_no", phone);
                    edit.putString("password", password);
                    edit.putString("sector", sector);
                    edit.putString("area", location);
                    edit.putString("bio", bio);
                    edit.putString("distance", distance);
                    edit.putString("email_noti", email_noti);
                    edit.putString("app_noti", app_noti);
                    edit.putString("confirmed", confirmed);
                    edit.putBoolean("first_job", true);
                    edit.commit();
                } else {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    error = "";
                    if (data.has("Email")) {
                        String email = data.getString("Email");
                        error = error + email;
                    }
                    if (data.has("Password")) {
                        String pass = data.getString("Password");
                        error = error + pass;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (status.equalsIgnoreCase("true")) {
                if (type.equals("job-offer")) {
                    progressBar.setVisibility(View.GONE);
                    Intent i = new Intent(Sign_In.this, DashBoard_Employeer.class);
                    ActivityStack.activity.add(Sign_In.this);
                    startActivity(i);
                } else if (type.equals("job-seeker")) {
                    Intent i = new Intent(Sign_In.this, DashBoard_Seeker.class);
                    i.putExtra("parse", "false");
                    ActivityStack.activity.add(Sign_In.this);
                    startActivity(i);
//                    Utils.get_job.clear();
//                    new getJobs().execute();
                }
            } else {
                progressBar.setVisibility(View.GONE);
                dialog("Alert!", error);
            }
        }
    }


    //*******************************************Login Method***************************************
    public String login(String email, String password, String device_id, String type) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("Email", email)
                .appendQueryParameter(
                        "Password", password).appendQueryParameter(
                        "DeviceId", device_id).appendQueryParameter(
                        "Type", type);
        Log.i("Complete", "Url: " + Utils.login + builder);
        client = new RestClient(Utils.login + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Sign In");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

package com.stepladder.job.stepladder.Seeker;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.CropActivityP;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

public class Profile_photo extends ActionBarActivity {

    ImageView img_profile_photo, imageView3;
    RelativeLayout lay_profile_location;
    LinearLayout ll_looking;
    public static ArrayList<Uri> list = new ArrayList<Uri>();
    TextView f_load, f_load2, textView1, txt_look_good, title;
    LayoutInflater inflater;
    ProgressBar progress;
    public static String status, profile_image;
    public static Uri selected_image;
    SharedPreferences prefs;
    ConnectionDetector con;
    String image_path, edit;
    Menu newmenu;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_photo);

        //Get Intent
        Intent intent = getIntent();
        edit = intent.getStringExtra("edit");

        init();

        setFont();

        //Set Profile Image
        if (MainActivity.profile != null) {

            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(MainActivity.profile).noFade().placeholder(R.drawable.ic_profile).
                    transform(new RoundedTransformation(1)).rotate(0f, 0f, 0f)
                    .into(img_profile_photo, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                            if (edit.equals("true")) {
                                ll_looking.setVisibility(View.INVISIBLE);
                            } else {
                                ll_looking.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                URL url = new URL(MainActivity.profile);
                                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                File file = new File(
                                        Environment.getExternalStorageDirectory().getPath()
                                                + "/profile.jpg");
                                file.createNewFile();
                                FileOutputStream ostream = new FileOutputStream(file);
                                if(bitmap.getWidth()>500){
                                    bitmap = Bitmap.createScaledBitmap(bitmap,500,500,false);
                                }
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                                ostream.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
        } else if (prefs.getString("profile_image", null) != null) {
                progress.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_user_new).
                        transform(new RoundedTransformation(1)).rotate(0f, 0f, 0f)
                        .into(img_profile_photo, new Callback() {

                            @Override
                            public void onSuccess() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                                if (edit.equals("true")) {
                                    ll_looking.setVisibility(View.INVISIBLE);
                                } else {
                                    ll_looking.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                            }
                        });
//            }
        }

        clickevents();

    }


    //*****************************************UI intialization*************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        title.setText("Profile Photo");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        img_profile_photo = (ImageView) findViewById(R.id.img_profile_photo);
        lay_profile_location = (RelativeLayout) findViewById(R.id.lay_profile_location);
        ll_looking = (LinearLayout) findViewById(R.id.ll_looking);
        progress = (ProgressBar) findViewById(R.id.progress);
        textView1 = (TextView) findViewById(R.id.textView1);
        f_load2 = (TextView) findViewById(R.id.f_load2);
        f_load = (TextView) findViewById(R.id.f_load);
        txt_look_good = (TextView) findViewById(R.id.txt_look_good);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        if (edit.equals("true")) {
            lay_profile_location.setVisibility(View.INVISIBLE);
            textView1.setText("Update Profile Photo");
            imageView3.setVisibility(View.INVISIBLE);
            f_load.setText("Tap below to change profile photo.");
            f_load2.setVisibility(View.GONE);
            ll_looking.setVisibility(View.INVISIBLE);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            DashBoard_Seeker.open = true;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Seeker Profile");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //************************************Setting Fonts for UI components***************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Profile_photo.this), Typeface.BOLD);
        txt_look_good.setTypeface(Utils.setfontlight(Profile_photo.this));
        f_load2.setTypeface(Utils.setfontlight(Profile_photo.this));
        textView1.setTypeface(Utils.setfontlight(Profile_photo.this));
        f_load.setTypeface(Utils.setfontlight(Profile_photo.this));
    }

    //********************************************clickevents***************************************
    private void clickevents() {
        img_profile_photo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        lay_profile_location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Utils.pimageUri != null) {
                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Profile_photo.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        if (edit.equals("true")) {
                            new upload_image().execute();
                        } else {
                            Intent i = new Intent(Profile_photo.this, Location.class);
                            i.putExtra("edit", "false");
                            ActivityStack.activity.add(Profile_photo.this);
                            startActivity(i);
                            new upload_image().execute();
                        }
                    }
                } else if (MainActivity.profile != null) {
                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Profile_photo.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        if (edit.equals("true")) {
                            new upload_image().execute();
                        } else {
                            Intent i = new Intent(Profile_photo.this, Location.class);
                            i.putExtra("edit", "false");
                            ActivityStack.activity.add(Profile_photo.this);
                            startActivity(i);
                            new upload_image().execute();
                        }
                    }
                } else {
                    if (edit.equals("true")) {
                        new upload_image().execute();
                    } else {
                        Intent i = new Intent(Profile_photo.this, Location.class);
                        i.putExtra("edit", "false");
                        ActivityStack.activity.add(Profile_photo.this);
                        startActivity(i);
                    }
                }
            }
        });
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        if (edit.equals("true")) {
            newmenu.findItem(R.id.action_expand).setVisible(false);
            newmenu.findItem(R.id.action_skip).setVisible(false);
        } else {
            newmenu.findItem(R.id.action_expand).setVisible(false);
            newmenu.findItem(R.id.action_skip).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_skip) {
            Intent i = new Intent(Profile_photo.this, Location.class);
            i.putExtra("edit", "false");
            ActivityStack.activity.add(Profile_photo.this);
            startActivity(i);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (edit.equals("true")) {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Image Uri: ", "" + Utils.pimageUri);
        if (Utils.pimageUri != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(1)).rotate(0f, 0f, 0f)
                    .into(img_profile_photo, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                            if (edit.equals("true")) {
                                lay_profile_location.setVisibility(View.VISIBLE);
                                ll_looking.setVisibility(View.INVISIBLE);
                            } else {
                                ll_looking.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        }
    }

    //***********************************Upload Image AsyncTask Class*******************************
    class upload_image extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Profile_photo.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (edit.equals("true")) {
                dia.setMessage("Updating Image....");
                dia.setCancelable(false);
                dia.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.profile_image + "AuthCode=" + prefs.getString("auth_code", null));
            if(Utils.pimageUri!=null){
                Log.i("Image", "Path:" + Utils.pimageUri);
                image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
            }else if(MainActivity.profile!=null){
                image_path = Environment.getExternalStorageDirectory().getPath()
                        + "/profile.jpg";
            }
            String response = client.postimage(image_path);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.optString("profile_image");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (edit.equals("true")) {
                dia.dismiss();
            }
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("profile_image", profile_image);
                editor.putString("confirmed","0");
                editor.commit();
                if (edit.equals("true")) {
                    dialog("Success!", "Data updated successfully");
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putString("confirmed","1");
                    editor1.commit();
                }
            } else {
            }
        }
    }


    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + selected_image);
            Intent in = new Intent(Profile_photo.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            MainActivity.profile = null;
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(1)).rotate(0f, 0f, 0f)
                    .into(img_profile_photo, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                            if (edit.equals("true")) {
                                lay_profile_location.setVisibility(View.VISIBLE);
                                ll_looking.setVisibility(View.INVISIBLE);
                            } else {
                                ll_looking.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        } else {
            Utils.pimageUri = null;
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //*************************************Alert Dialog*************************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(Profile_photo.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(Profile_photo.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(Profile_photo.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
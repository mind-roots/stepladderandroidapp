package com.stepladder.job.stepladder.Employeer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.model.LatLng;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Select_Location;
import com.stepladder.job.stepladder.Utils.GpsTracker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class Job_Details extends ActionBarActivity {
    RelativeLayout rl_skills_req;
    TextView title, btn_txt, textView1, txt_count;
    EditText edt_job_description, jobtittle;
    LayoutInflater inflater;
    Menu newmenu;
    RestClient client;
    SharedPreferences prefs;
    String status, JobId, jobtitle, jobdescription, city, knownName, state, country;
    ConnectionDetector con;
    private Tracker mTracker;
    ProgressBar progress;
    public static double latitude, longitude;
    public static LatLng MyLocation;
    GpsTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    public static EditText edt_location;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_details);

        init();

        setFont();
        DashBoard_Employeer.open = true;

        //Set Prefs Data
        if (prefs.getString("jobtitle", null) != null) {
            jobtittle.setText(prefs.getString("jobtitle", null));
            jobtittle.setSelection(jobtittle.length());
        }
      /*  if (prefs.getString("area", null) != null) {
            edt_location.setText(prefs.getString("area", null));
        }*/
        if (prefs.getString("jobdescription", null) != null) {
            edt_job_description.setText(prefs.getString("jobdescription", null));
            edt_job_description.setSelection(edt_job_description.length());
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Job_Details.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get Current Location
            new location().execute();
        }

        clickevents();
    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Job Details");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        rl_skills_req = (RelativeLayout) findViewById(R.id.rl_skills_req);
        edt_job_description = (EditText) findViewById(R.id.edt_job_description);
        jobtittle = (EditText) findViewById(R.id.edt_job_title);
        textView1 = (TextView) findViewById(R.id.textView1);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        txt_count = (TextView) findViewById(R.id.txt_count);
        edt_location = (EditText) findViewById(R.id.edt_location);
        progress = (ProgressBar) findViewById(R.id.progress);
        gps = new GpsTracker(Job_Details.this);
        geocoder = new Geocoder(this, Locale.getDefault());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Company Job Details");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    // ******************************Setting Fonts for UI components********************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Job_Details.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Job_Details.this));
        textView1.setTypeface(Utils.setfontlight(Job_Details.this));
        jobtittle.setTypeface(Utils.setfontlight(Job_Details.this));
        edt_job_description.setTypeface(Utils.setfontlight(Job_Details.this));
        edt_location.setTypeface(Utils.setfontlight(Job_Details.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        edt_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Job_Details.this, Select_Location.class);
                ActivityStack.activity.add(Job_Details.this);
                in.putExtra("id", "2");
                in.putExtra("current_loc", Utils.selected_loc);
                startActivity(in);
            }
        });

        rl_skills_req.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (jobtittle.length() == 0) {
                    jobtittle.setError("Required field");
                } else if (edt_location.length() == 0) {
                    edt_location.setError("Required Field");
                } else if (edt_job_description.length() == 0) {
                    edt_job_description.setError("Required field");
                } else {
                    jobtitle = jobtittle.getText().toString();
                    jobdescription = edt_job_description.getText().toString();
                    Utils.selected_loc = edt_location.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Job_Details.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new job_detail().execute();
                    }
                }
            }
        });

        jobtittle.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                // TODO Auto-generated method stub
                count = s.length();
                txt_count.setText(String.valueOf(s.length()));
                if (count >= 30) {
                    Utils.hideKeyboard(Job_Details.this);
                } else {


                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });
    }

    //*************************************Async Task Classes***************************************
    class location extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //***********************************Get Current Location*******************************
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                MyLocation = new LatLng(latitude, longitude);
                Utils.lat = latitude;
                Utils.lng = longitude;
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                    String address = addresses.get(0).getAddressLine(0);
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();
                    System.out.println("Location" + state + "," + country);

                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Unable to get location",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.setVisibility(View.GONE);
            if (gps.canGetLocation()) {
                edt_location.setText(knownName + ", " + city);
                Utils.selected_loc = knownName + ", " + city;
            } else {
                Toast.makeText(getApplicationContext(), "Unable to get location",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    // ************************************Job_Details AsyncTask Class******************************
    class job_detail extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Job_Details.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving data...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "AuthCode: " + prefs.getString("auth_code", null) + "LocationName: " + Utils.selected_loc + "Lat: " + Utils.lat + "Long: " + Utils.lng);
            String response = jobdetail(prefs.getString("auth_code", null),
                    jobtitle, Utils.selected_loc, String.valueOf(Utils.lat), String.valueOf(Utils.lng), jobdescription);
            Log.i("Response in jobdetails", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    JobId = data.getString("JobId");
                    Log.i("JobId", ":" + JobId);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                Intent i = new Intent(Job_Details.this, Skills_Required.class);
                ActivityStack.activity.add(Job_Details.this);
                startActivity(i);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("JobId", JobId);
                edit.putString("jobtitle", jobtitle);
                edit.putString("area", Utils.selected_loc);
                edit.putString("jobdescription", jobdescription);
                edit.commit();
            } else {
                dialog("Alert!", "Error in saving data.");
            }
        }
    }

    //**************************************Job detail Method***************************************

    public String jobdetail(String authcode, String jobtitle, String location, String lat, String lng,
                            String jobdescription) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("AuthCode", authcode)
                .appendQueryParameter("JobTitle", jobtitle)
                .appendQueryParameter("LocationNamej", location)
                .appendQueryParameter("Latj", lat)
                .appendQueryParameter("Longj", lng)
                .appendQueryParameter("JobDescription", jobdescription);
        Log.i("Complete", "Url: " + Utils.job_details + builder);
        client = new RestClient(Utils.job_details + builder);
        String response = null;
        response = client.executePost();
        return response;
    }

    // *******************************************Alert Dialog**************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    // ***********************************onCreateOptionsMenu***************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    // *************************************Back Press Method***************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

package com.stepladder.job.stepladder.Employeer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 1/15/2016.
 */
public class PrivacyPolicy extends ActionBarActivity{
    TextView txt_privacypolicy,title;
    Menu newmenu;
    LayoutInflater inflater;
    String policy_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Privacy Policy");
        title.setTypeface(Utils.setfontlight(PrivacyPolicy.this), Typeface.BOLD);

        txt_privacypolicy = (TextView)findViewById(R.id.txt_privacypolicy);
        policy_text = "Privacy Policy\n" +
                "Effective Date: 16.10.2015\n" +
                "\n" +
                "Delayd values your privacy, personal information and Content that you store in our Service. Our Privacy Policy explains what information Delayd collect about you and why, what we may do with that information and how we handle your Content.\n" +
                "\n" +
                "What Is The Scope Of This Privacy Policy?\n" +
                "This Privacy Policy is incorporated into the Delayd Terms of Service and applies to the information obtained by us through your use of this website, the Service and other Delayd products and services  \n" +
                "\n" +
                "Will This Privacy Policy Ever Change?\n" +
                "As Delayd evolves, we may need to update this Policy to keep pace with changes in the Service, our business and laws applicable to us and you; we will, however, always maintain our commitment to respect your privacy. We will post any revisions to this Policy, along with their effective date, in an easy to find area of our website, so we recommend that you periodically check back here to stay informed of any changes. We will ask for your consent to such changes if required by applicable law; in other cases, please note that your continuation of your Delayd Service account after any change means that you agree with, and consent to be bound by, the new Privacy Policy. If you disagree with any changes in this Policy and do not wish your information to be subject to the revised Policy, you will need to close your account.  \n" +
                "\n" +
                "1. Information Collection and Use\n" +
                "\n" +
                "Here is a list of the information we require for you to use Delayd.\n" +
                "\n" +
                "Mobile Number: We need your mobile number in order for you to create an account on Delayd.\n" +
                "\n" +
                "Name: We need your name so recipients of SMS text messages sent from Delayd can recognise the sender. This is because SMS that are sent from Delayd are handled by a 3rd Party carrier and not your personal carrier.\n" +
                "\n" +
                "Email Address: This is needed in order for you to schedule recurring or non-recurring emails via Delayd.\n" +
                "\n" +
                "Facebook Integration: This is needed so Delayd can post recurring or non recurring Facebook posts on your behalf.\n" +
                "\n" +
                "Twitter Integration: This is needed so Delayd can post recurring or non recurring Tweets on your behalf. \n" +
                "\n" +
                "Contact List Permissions: This is needed so you can schedule SMS to recipients in your contact list.\n" +
                "\n" +
                "Camera Roll Integration: This is needed if you wish to post images via Facebook Integration.\n" +
                "\n" +
                "What Tracking Information Does Delayd Collect About Me Or My Devices?\n" +
                "Delayd uses cookies, tracking pixels and similar technologies on our Service to collect information that helps us provide our Service to you.  \n" +
                "\n" +
                "What Is Delayd’s Approach to Information Collected From Children?\n" +
                "Delayd does not knowingly collect personal information from children without parental consent. If we learn that we have inadvertently obtained information in violation of applicable laws prohibiting collection of information from children without such consent, we will promptly delete it.  \n" +
                "\n" +
                "Will Delayd Contact Me?\n" +
                "From time to time, we may contact you via email, SMS or mobile notifications with information about product announcements and service offerings that we think may be beneficial to you, as well as software updates and special offers. We also may contact you with information about products and services from our business partners. We consider your acceptance of this Privacy Policy as your acceptance of our offer to send you these emails.    \n" +
                "\n" +
                "2. Information Access and Disclosure\n" +
                "\n" +
                "Does Delayd Share My Personal Information or Content?\n" +
                "Delayd is not in the business of selling or renting your information. \n" +
                "\n" +
                "Outside of actions you take within the Service to communicate via Facebook, Twitter, SMS or Email, or to authorize third-party applications, we only disclose your information - and then only the minimum information necessary - when: \n" +
                "\n" +
                " • We have your explicit consent to share the information. \n" +
                "\n" +
                " • We need to share your information with service providers who process data on our behalf in order to operate the Service, complete your payment transactions, and help us communicate with you as described elsewhere in this policy; these providers are subject to strict data protection requirements in keeping with our commitments. \n" +
                "\n" +
                " • We need to share your information with service providers to fulfil your product or service requests including sales.\n" +
                "\n" +
                " • We believe it is necessary to investigate potential violations of our Terms of Service, to enforce those Terms of Service, or where we believe it is necessary to investigate, prevent or take action regarding illegal activities, suspected fraud or potential threats against persons, property or the systems on which we operate the Service. We do not share your account information or user activity history with third parties for the purpose of enabling them to deliver their advertisements to you. As described on our Cookie Information page, we contract with third-party advertising networks in order to deliver relevant Delayd advertisements to you across the Internet and to manage our communications with you. \n" +
                "\n" +
                " In addition, we may share with partners or service providers a hashed identifier derived from the personal information you have submitted to us (such as your email address) to serve you ads when you visit partners’ and providers’ websites, applications or platforms. We may, for instance, participate in the Twitter Tailored Audience and Facebook Custom Audience services. \n" +
                "\n" +
                "What Information Does Delayd Share When I send SMS Messages, Email posts, Facebook posts and Tweets?\n" +
                "\n" +
                " If you share a Facebook Post or Tweet, then anyone who has access to that account may see your user profile depending on your own privacy settings. If you initiate or respond to an SMS or Email, then the person with whom you are talking to will be able to view SMS or Email. In a group SMS Message where you are a sender of a message, only recipients in that group will see your message. \n" +
                "\n" +
                "What Gets Shared When I Use Third Party Apps With My Delayd Account? \n" +
                "Some third-party applications and services that work with our Service may ask for permission to access your Content or other information about your account. Those applications will provide you with notice and request your consent in order to obtain such access or information. Please consider your selection of such applications and services, and your permissions, carefully. We encourage you to review each party’s contract terms and privacy policy. \n" +
                "\n" +
                "Does Delayd Ever Make Any of My Personal Information or Content Public? \n" +
                "\n" +
                "With SMS Messages and Emails no. However, Facebook Posts and Tweets may be public depending on your Facebook and Twitter Privacy Preferences. Please bear this in mind when using Delayd. \n" +
                "\n" +
                "3. Data Storage and Transfer \n" +
                "\n" +
                "Where Is My Data Stored? \n" +
                "When you use Delayd Software on your computing device, such as by using one of our downloadable clients, Content you save will be stored locally on that device. When you sync your computing device with the Service, that Content will be replicated on our servers, which are located in the United States. Please be aware that Personal Information and Content submitted to Delayd will be transferred to a data center in the United States.\n" +
                "\n" +
                "If you post information to the Delayd sites you are confirming your consent to such information, including Personal Information and Content, being transmitted to, hosted and accessed in the United States. \n" +
                "\n" +
                "How Secure Is My Data? \n" +
                "Delayd is committed to protecting the security of your information and takes reasonable precautions to protect it. We use industry standard encryption to protect your data in transit. This is commonly referred to as transport layer security (“TLS”) or secure socket layer (“SSL”) technology. However, Internet data transmissions, cannot be guaranteed to be 100% secure, and as a result, we cannot ensure the security of information during its transmission between you and us; accordingly, you acknowledge that you do so at your own risk. Once we receive your data, we protect it on our servers using a combination of administrative, physical and logical security safeguards. The security of the information stored locally in the Delayd Software installed on your computing device is dependent upon you making use of the security features on your device. We recommend that you take the appropriate steps to secure all computing devices that you use with our applications and Service. If Delayd learns of a security system breach, we may attempt to notify you and provide information on protective steps, if available, through the email address that you have provided to us or by posting a notice on our web site and/or via other communication platforms. Depending on where you live, you may have a legal right to receive such notices in writing. \n" +
                "\n" +
                " 4. Information Deletion\n" +
                "\n" +
                "What Happens If I Want to Stop Using Delayd? \n" +
                "You can delete Content at any time, and you can stop using the Delayd Service at any time. If you delete your account, the Content in your account will be deleted also. \n" +
                "\n" +
                "If you delete your copy of a SMS message or Email, this action can't be undone. Please note that deleting your copy of a SMS message or Email from your own account won’t delete those messages from the accounts of the people with whom you were chatting. \n" +
                "\n" +
                "What Happens If Delayd Closes My Account? \n" +
                "\n" +
                "If Delayd deactivates your Service account due to a Terms of Service violation, then you may contact Delayd to request deletion of your data, and Delayd will evaluate such requests on a case by case basis, pursuant to our legal obligations.  \n" +
                "\n" +
                "5. Contact Us \n" +
                "\n" +
                "How Can I Contact Delayd? \n" +
                "\n" +
                "Delayd welcomes your feedback regarding this Privacy Policy. If you have questions, comments or concerns about this Policy, please contact us by email at hello@delaydapp.com \n" +
                "\n" +
                "Tinyyo\n" +
                "3 Alexander House\n" +
                "Fore Hill\n" +
                "Ely\n" +
                "Cambridgeshire\n" +
                "CB7 4AF\n" +
                "United Kingdom\n";

        txt_privacypolicy.setText(policy_text);


    }

    //***********************************onCreateOptionsMenu****************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

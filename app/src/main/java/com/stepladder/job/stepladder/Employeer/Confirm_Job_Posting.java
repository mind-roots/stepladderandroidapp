package com.stepladder.job.stepladder.Employeer;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.ActivityTransition.TransitionHelper;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.Custom_View.MyImage;
import com.stepladder.job.stepladder.Employer_Tutorial.Step1Employer;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class Confirm_Job_Posting extends ActionBarActivity {
    LinearLayout lay_confirm_profile, lay_seemore_btn;
    TextView title, txt_desc, title_desc, txt_skills,
            title_skills, txt_area, title_area, txt_length, title_length,
            txt_salary, title_salary, txt_title, title_title, txt_company_name;
    EditText edt_tell_potential;
    ImageView img_get;
    ImageView img_Employer;
    String status, response;
    Menu newmenu;
    RestClient client;
    LayoutInflater inflater;
    SharedPreferences prefs;
    String pound = "\u00a3";
    public static boolean change_image = false;
    ConnectionDetector con;
    ProgressBar progressBar, progress;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_job_posting);

        init();

        setFont();

        //Set Company Image
        if (Utils.cimageUri != null) {
            img_Employer.setImageURI(Utils.cimageUri);
        } else if (prefs.getString("profile_image", null) != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                    .into(img_Employer, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        }

        //Set Prefs Values
        if (prefs.getString("company_name", null) != null) {
            // Log.i("CompanyName: ", "" + prefs.getString("company_name", null));
            txt_company_name.setText(prefs.getString("company_name", null));
        }
        if (prefs.getString("area", null) != null) {
            //Log.i("Area: ", "" + prefs.getString("area", null));
            txt_area.setText(prefs.getString("area", null));
        }
        if (prefs.getString("jobtitle", null) != null) {
            // Log.i("Jobtitle: ", "" + prefs.getString("jobtitle", null));
            txt_title.setText(prefs.getString("jobtitle", null));
        }
        if (prefs.getString("jobdescription", null) != null) {
            // Log.i("Description: ", "" + prefs.getString("jobdescription", null));
            txt_desc.setText(prefs.getString("jobdescription", null));
        }
        if (prefs.getString("skills", null) != null) {
            // Log.i("Skills: ", "" + prefs.getString("skills", null));
            txt_skills.setText(prefs.getString("skills", null));
        }
        if (prefs.getString("jobduration", null) != null) {
            //Log.i("Duration: ", "" + prefs.getString("jobduration", null));
            txt_length.setText(prefs.getString("jobduration", null)+" - "+prefs.getString("job_pos", null));
        }

        if (prefs.getString("job_type", null) != "") {
            if (prefs.getString("job_type", null).equals("Per Annum")) {
                if (prefs.getString("from", null) != "" && prefs.getString("to", null) != "") {
                    txt_salary.setText((priceformat(prefs.getString("from", null))).concat(" - ").concat(priceformat(prefs.getString("to", null))) + " per year");
                } else {
                    txt_salary.setText("Not disclosed (Yearly)");
                }
            } else if (prefs.getString("job_type", null).equals("Hourly")) {
                if (prefs.getString("from", null) != "") {
                    txt_salary.setText((decipriceformat(prefs.getString("from", null))) + " per hour");
                } else {
                    txt_salary.setText("Not disclosed (Hourly)");
                }
            }
        }
        clickevents();

    }

    // *************************************Setting Fonts for UI components*************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this),
                Typeface.BOLD);
        txt_area.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        txt_company_name.setTypeface(Utils
                .setfontlight(Confirm_Job_Posting.this));
        txt_desc.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        txt_length.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        txt_salary.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        txt_skills.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        txt_title.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_area.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_desc.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_length.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_salary.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_skills.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));
        title_title.setTypeface(Utils.setfontlight(Confirm_Job_Posting.this));

    }

    // *******************************************clickevents***************************************
    private void clickevents() {
        lay_confirm_profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                //Connection Detection
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            Confirm_Job_Posting.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new Change_Image().execute();
                }
            }
        });

        img_Employer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        img_get.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        lay_seemore_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Confirm_Job_Posting.this,
                        See_More_Employeer.class);
                ActivityStack.activity.add(Confirm_Job_Posting.this);
                transitionTo(intent);
            }
        });

    }

    //****************************************Transition Methods************************************
    @SuppressLint("NewApi")
    protected void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper
                .createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this, pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Confirm Job Posting");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        lay_confirm_profile = (LinearLayout) findViewById(R.id.lay_confirm_profile);
        edt_tell_potential = (EditText) findViewById(R.id.edt_tell_potential);
        img_Employer = (ImageView) findViewById(R.id.img_Employer);
        img_get = (ImageView) findViewById(R.id.img_get);
        lay_seemore_btn = (LinearLayout) findViewById(R.id.lay_seemore_btn);
        txt_desc = (TextView) findViewById(R.id.txt_desc);
        title_desc = (TextView) findViewById(R.id.title_desc);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        title_skills = (TextView) findViewById(R.id.title_skills);
        txt_area = (TextView) findViewById(R.id.txt_area);
        title_area = (TextView) findViewById(R.id.title_area);
        txt_length = (TextView) findViewById(R.id.txt_length);
        title_length = (TextView) findViewById(R.id.title_length);
        txt_salary = (TextView) findViewById(R.id.txt_salary);
        title_salary = (TextView) findViewById(R.id.title_salary);
        txt_title = (TextView) findViewById(R.id.txt_title);
        title_title = (TextView) findViewById(R.id.title_title);
        txt_company_name = (TextView) findViewById(R.id.txt_company_name);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progress = (ProgressBar) findViewById(R.id.progress);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Company Confirm Job Posting");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    // **************************************Confirm Job Details************************************
    class Change_Image extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.update_image + "AuthCode=" +
                    prefs.getString("auth_code", null) + "&JobId=" + prefs.getString("JobId", null));
            if (change_image) {
                Log.i("Image", "Path:" + getRealPathFromURI(getApplicationContext(), Utils.cimageUri));
                String image_path = getRealPathFromURI(getApplicationContext(), Utils.cimageUri);
                response = client.postcompanyimage(image_path);
                change_image = false;
            } else {
                response = client.executePost();
            }
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("type", "job-offer");
            editor.commit();
            if (status.equals("true")) {
                if (prefs.getBoolean("first_job", false) == false) {
                    Intent i = new Intent(Confirm_Job_Posting.this,
                            Step1Employer.class);
                    ActivityStack.activity.add(Confirm_Job_Posting.this);
                    startActivity(i);
                    editor.putBoolean("first_job", true);
                    editor.commit();
                }else {
                    Intent i = new Intent(Confirm_Job_Posting.this,
                            DashBoard_Employeer.class);
                    ActivityStack.activity.add(Confirm_Job_Posting.this);
                    startActivity(i);
                }
            } else {
                dialog("Alert!", "Error in uploading image.");
            }
        }
    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    public String decipriceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            //amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }


    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(true);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_expand) {
            Intent intent = new Intent(Confirm_Job_Posting.this,
                    See_More_Employeer.class);
            ActivityStack.activity.add(Confirm_Job_Posting.this);
            transitionTo(intent);
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (See_More_Employeer.img) {
            if (Utils.cimageUri != null) {
                img_Employer.setImageURI(Utils.cimageUri);
            }
            See_More_Employeer.img = false;
        }
    }

    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Company_Details.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Company_Details.selected_image);
            Intent in = new Intent(Confirm_Job_Posting.this, CropActivity.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            change_image = true;
            img_Employer.setImageURI(Utils.cimageUri);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(Confirm_Job_Posting.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(Confirm_Job_Posting.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(Confirm_Job_Posting.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

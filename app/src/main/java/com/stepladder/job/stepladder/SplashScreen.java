package com.stepladder.job.stepladder;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.onesignal.OneSignal;
import com.stepladder.job.stepladder.Employeer.Company_Details;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Profile_photo;

/**
 * Created by user on 1/20/2016.
 */
public class SplashScreen extends Activity implements Animation.AnimationListener {
    ImageView img_ladder;
    SharedPreferences prefs;
    public static boolean push = false;
  //  public static String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        img_ladder = (ImageView) findViewById(R.id.img_ladder);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);

        img_ladder.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));

        try{
            NotificationManager notifManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notifManager.cancelAll();
        }catch (Exception e){
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Log.i("AuthCode", ":" + prefs.getString("auth_code", null));
                if (prefs.getString("auth_code", null) != null) {
                    if (prefs.getString("type", null).equals("job-offer") && prefs.getString("confirmed", null).equals("1")) {
                            if (OneSignalClass.type.equals("employer-match")) {
                                Intent i = new Intent(SplashScreen.this, DashBoard_Employeer.class);
                                ActivityStack.activity.add(SplashScreen.this);
                                startActivity(i);
                            } else if (OneSignalClass.type.equals("home")) {
                                Intent i = new Intent(SplashScreen.this, DashBoard_Employeer.class);
                                ActivityStack.activity.add(SplashScreen.this);
                                startActivity(i);
                            }
                    } else if (prefs.getString("type", null).equals("job-seeker") && prefs.getString("confirmed", null).equals("1")) {
                            if (OneSignalClass.type.equals("employee-match")) {
                                Intent i = new Intent(SplashScreen.this, DashBoard_Seeker.class);
                                i.putExtra("parse", "true");
                                ActivityStack.activity.add(SplashScreen.this);
                                startActivity(i);
                            } else if (OneSignalClass.type.equals("home")) {
                                Intent i = new Intent(SplashScreen.this, DashBoard_Seeker.class);
                                i.putExtra("parse", "false");
                                ActivityStack.activity.add(SplashScreen.this);
                                startActivity(i);
                            }
                    } else {
                        if (prefs.getString("type", null).equals("job-offer")) {
                            Intent intent = new Intent(SplashScreen.this, Company_Details.class);
                            ActivityStack.activity.add(SplashScreen.this);
                            startActivity(intent);
                        } else if (prefs.getString("type", null).equals("job-seeker")) {
                            Intent intent = new Intent(SplashScreen.this, Profile_photo.class);
                            intent.putExtra("edit", "false");
                            ActivityStack.activity.add(SplashScreen.this);
                            startActivity(intent);
                        } else {
                            Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                } else {
                    Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 1500);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

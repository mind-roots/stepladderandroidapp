package com.stepladder.job.stepladder.ActivityTransition;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Employeer_Fragments.ViewCurrentJobsFragment;
import com.stepladder.job.stepladder.Model.ModelGetJobs;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class OneSignalBackgroundDataReceiver extends WakefulBroadcastReceiver {
    public static boolean isPush = false;
    SharedPreferences prefs;
    String Screen, type = "no", status;
    int match_jobs, quota;
    Context con;
    RestClient client;


    public void onReceive(Context context, Intent intent) {
        try {
            Bundle dataBundle = intent.getBundleExtra("data");
            con = context;
            prefs = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
            Log.i("OneSignalExample", "Is Your App Active: " + dataBundle.getBoolean("isActive"));
            if (dataBundle.getBoolean("isActive")) {
                Log.i("OneSignalExample", "data additionalData: " + dataBundle.getString("custom"));
                JSONObject customJSON = new JSONObject(dataBundle.getString("custom"));
                if (customJSON.has("a")) {
                    JSONObject additionalData = customJSON.getJSONObject("a");
                    String type = additionalData.optString("type");
                    System.out.println("values in one signal" + type);
                    if (type.equals("employer-match")) {
                        isPush = true;
                        new get_jobs().execute();
                    } else if (type.equals("employee-match")) {
                        if (DashBoard_Seeker.isHome) {
                            new getJobs().execute();
                            DashBoard_Seeker.isHome = false;
                        }
                    }
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    class getJobs extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DashBoard_Seeker.dia.setMessage("Loading....");
            DashBoard_Seeker.dia.setCancelable(false);
            DashBoard_Seeker.dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            client = new RestClient(Utils.get_jobs + "AuthCode=" + prefs.getString("auth_code", null));
            String response = client.executeGet();
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                Log.i("Status: ", status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = obj.optJSONObject("data");
                    quota = data.optInt("quota");
                    match_jobs = data.optInt("matchjobs");
                    JSONArray jobs = data.optJSONArray("jobs");
                    Log.i("jobs array length: ", "" + jobs.length());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DashBoard_Seeker.dia.dismiss();
            if (status.equals("true")) {
                Log.i("Match Jobs: ", "" + match_jobs);
                if (match_jobs == 0) {
                    DashBoard_Seeker.ll_badge.setVisibility(View.GONE);
                    DashBoard_Seeker.ic_noti_grey.setVisibility(View.VISIBLE);
                } else {
                    DashBoard_Seeker.ll_badge.setVisibility(View.VISIBLE);
                    DashBoard_Seeker.counter.setText(String.valueOf(match_jobs));
                    DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
                }
            } else {
                //dialog("Alert!", "Authcode Mismatch");
            }
        }
    }

    class get_jobs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ViewCurrentJobsFragment.job_list.clear();
            DashBoard_Employeer.dia.setMessage("Loading....");
            DashBoard_Employeer.dia.setCancelable(false);
            DashBoard_Employeer.dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = GetJobs(prefs.getString("auth_code", null), Utils.get_current_jobs);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");

                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = obj.getJSONArray("data");
                    Log.i("Data Array: ", "" + data);
                    for (int i = 0; i < data.length(); i++) {

                        JSONObject jobj = data.getJSONObject(i);
                        String job_id = jobj.getString("JobId");
                        String job_title = jobj.getString("JobTitle");
                        String Status = jobj.getString("Status");
                        String leads = jobj.getString("Leads");
                        String company_image = jobj.getString("Image");
                        JSONObject company_info = jobj.getJSONObject("CompanyInfo");
                        String company_name = company_info.getString("CompanyName");

                        //Add Data into Model
                        ModelGetJobs model_jobs = new ModelGetJobs();
                        model_jobs.job_id = job_id;
                        model_jobs.job_title = job_title;
                        model_jobs.job_status = Status;
                        model_jobs.leads = leads;
                        model_jobs.company_image = company_image;
                        model_jobs.company_name = company_name;

                        ViewCurrentJobsFragment.job_list.add(model_jobs);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DashBoard_Employeer.dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                ViewCurrentJobsFragment.employeerCardAdapter.notifyDataSetChanged();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("first_job", false);
                editor.commit();
            }
        }
    }

    // ****************************************Get Jobs Method***********************************
    private String GetJobs(String auth_code, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode",
                auth_code);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

}
package com.stepladder.job.stepladder.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.stepladder.job.stepladder.Seeker_Tutorial.Step1Seeker;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step2Seeker;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step3Seeker;
import com.stepladder.job.stepladder.Seeker_Tutorial.Step4Seeker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 11/20/2015.
 */
public class CustomPagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;
    private List<Fragment> fragments;
    private static int NUM_ITEMS = 3;

    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragments = new ArrayList<Fragment>();
//        fragments.add(new Step1Seeker());
//        fragments.add(new Step2Seeker());
//        fragments.add(new Step3Seeker());
//        fragments.add(new Step4Seeker());
    }

    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}

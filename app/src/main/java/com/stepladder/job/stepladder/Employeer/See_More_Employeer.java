package com.stepladder.job.stepladder.Employeer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.CropActivity;
import com.stepladder.job.stepladder.Custom_View.MyImage;
import com.stepladder.job.stepladder.Employer_Tutorial.Step1Employer;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Confirm_Profile;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 10/23/2015.
 */
public class See_More_Employeer extends ActionBarActivity {
    LinearLayout rl_post_job;
    ImageView img_get;
    ImageView img_Employer;
    TextView title, txt_desc, title_desc, txt_skills, title_skills, txt_area,
            title_area, txt_length, title_length, txt_salary, title_salary, txt_title, title_title, txt_company_name;
    Menu newmenu;
    Intent intent;
    String pound = "\u00a3", status, response, selected_image;
    LayoutInflater inflater;
    SharedPreferences prefs;
    ConnectionDetector con;
    ProgressBar progressBar, progress;
    public static boolean img = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.see_more_employeer);
        if (Build.VERSION.SDK_INT < 21) {

        } else {
            setupWindowAnimations();
        }

        init();

        setFont();

        //Set Company Image
        if (Utils.cimageUri != null) {
            img_Employer.setImageURI(Utils.cimageUri);
        } else if (prefs.getString("profile_image", null) != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                    .into(img_Employer, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        }

        //Set Prefs Values
        if (prefs.getString("company_name", null) != null) {
            txt_company_name.setText(prefs.getString("company_name", null));
        }
        if (prefs.getString("area", null) != null) {
            txt_area.setText(prefs.getString("area", null));
        }
        if (prefs.getString("jobtitle", null) != null) {
            txt_title.setText(prefs.getString("jobtitle", null));
        }
        if (prefs.getString("jobdescription", null) != null) {
            txt_desc.setText(prefs.getString("jobdescription", null));
        }
        if (prefs.getString("skills", null) != null) {
            txt_skills.setText(prefs.getString("skills", null));
        }
        if (prefs.getString("jobduration", null) != null) {
            if (getIntent().getStringExtra("intent_type") != null) {
                if (getIntent().getStringExtra("intent_type").equals("job_details")) {
                    txt_length.setText(prefs.getString("jobduration", null));
                }
            } else {
                txt_length.setText(prefs.getString("jobduration", null) + " - " + prefs.getString("job_pos", null));
            }
        }

        if (prefs.getString("job_type", null) != "") {
            if (prefs.getString("job_type", null).equals("Per Annum")) {
                if (prefs.getString("from", null) != "" && prefs.getString("to", null) != "") {
                    txt_salary.setText((priceformat(prefs.getString("from", null))).concat(" - ").concat(priceformat(prefs.getString("to", null))) + " per year");
                } else {
                    txt_salary.setText("Not disclosed (Yearly)");
                }
            } else if (prefs.getString("job_type", null).equals("Hourly")) {
                if (prefs.getString("from", null) != "") {
                    txt_salary.setText((decipriceformat(prefs.getString("from", null))) + " per hour");
                } else {
                    txt_salary.setText("Not disclosed (Hourly)");
                }
            }
        }

        clickevents();

    }


    //****************************************UI Initializations************************************
    private void init() {
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Confirm Job Posting");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        txt_desc = (TextView) findViewById(R.id.txt_desc);
        title_desc = (TextView) findViewById(R.id.title_desc);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        title_skills = (TextView) findViewById(R.id.title_skills);
        txt_area = (TextView) findViewById(R.id.txt_area);
        title_area = (TextView) findViewById(R.id.title_area);
        txt_length = (TextView) findViewById(R.id.txt_length);
        title_length = (TextView) findViewById(R.id.title_length);
        txt_salary = (TextView) findViewById(R.id.txt_salary);
        title_salary = (TextView) findViewById(R.id.title_salary);
        txt_title = (TextView) findViewById(R.id.txt_title);
        title_title = (TextView) findViewById(R.id.title_title);
        txt_company_name = (TextView) findViewById(R.id.txt_company_name);

        rl_post_job = (LinearLayout) findViewById(R.id.rl_post_job);
        img_get = (ImageView) findViewById(R.id.img_get);
        img_Employer = (ImageView) findViewById(R.id.img_Employer);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progress = (ProgressBar) findViewById(R.id.progress);

        if (getIntent().getStringExtra("intent_type") != null) {
            if (getIntent().getStringExtra("intent_type").equals("job_details")) {
                title.setText("Job Details");
                rl_post_job.setVisibility(View.GONE);
                img_get.setVisibility(View.GONE);
                img_Employer.setEnabled(false);
                DashBoard_Employeer.open = true;
            }
        }

    }

    //****************************************Setting Fonts for UI components***********************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(See_More_Employeer.this), Typeface.BOLD);
        txt_area.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_company_name.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_desc.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_length.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_salary.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_skills.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        txt_title.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_area.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_desc.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_length.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_salary.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_skills.setTypeface(Utils.setfontlight(See_More_Employeer.this));
        title_title.setTypeface(Utils.setfontlight(See_More_Employeer.this));

    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        rl_post_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Connection Detection
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            See_More_Employeer.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new Change_Image().execute();
                }
            }
        });
        img_Employer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });
        img_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }

            }
        });
    }

    //****************************************Transition Methods************************************
    @SuppressLint("NewApi")
    private void setupWindowAnimations() {
        Transition transition;
        transition = buildEnterTransition();
        getWindow().setEnterTransition(transition);
    }

    @SuppressLint("NewApi")
    private Transition buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }

    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Company_Details.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Company_Details.selected_image);
            Intent in = new Intent(See_More_Employeer.this, CropActivity.class);
            startActivityForResult(in, 2);

        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            img = true;
            Confirm_Job_Posting.change_image = true;
            img_Employer.setImageURI(Utils.cimageUri);

        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    public String decipriceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            //amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }


    // **************************************Confirm Job Details************************************
    class Change_Image extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.update_image + "AuthCode=" +
                    prefs.getString("auth_code", null) + "&JobId=" + prefs.getString("JobId", null));
            if (Confirm_Job_Posting.change_image) {
                Log.i("Image", "Path:" + getRealPathFromURI(getApplicationContext(), Utils.cimageUri));
                String image_path = getRealPathFromURI(getApplicationContext(), Utils.cimageUri);
                response = client.postcompanyimage(image_path);
                Confirm_Job_Posting.change_image = false;
            } else {
                response = client.executePost();
            }
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("type", "job-offer");
            editor.commit();
            if (status.equals("true")) {
                if (prefs.getBoolean("first_job", false) == false) {
                    Intent i = new Intent(See_More_Employeer.this,
                            Step1Employer.class);
                    ActivityStack.activity.add(See_More_Employeer.this);
                    startActivity(i);
                    editor.putBoolean("first_job", true);
                    editor.commit();
                } else {
                    Intent i = new Intent(See_More_Employeer.this,
                            DashBoard_Employeer.class);
                    ActivityStack.activity.add(See_More_Employeer.this);
                    startActivity(i);
                }
            } else {
                dialog("Alert!", "Error in uploading image.");
            }
        }
    }


    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    //***********************************onCreateOptionsMenu****************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(See_More_Employeer.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(See_More_Employeer.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(See_More_Employeer.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

package com.stepladder.job.stepladder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.stepladder.job.stepladder.Custom_View.CropImageView;
import com.stepladder.job.stepladder.Employeer.Company_Details;
import com.stepladder.job.stepladder.Employeer.Confirm_Job_Posting;
import com.stepladder.job.stepladder.Utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 11/21/2015.
 */
public class CropActivity extends Activity {

    CropImageView cropImageView;
    FrameLayout btn_cancel,btn_done;

    @SuppressLint("NewApi")
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.crop__activity_crop);
        cropImageView=(CropImageView) findViewById(R.id.crop_image);

        Log.i("Get Image: ",""+Company_Details.selected_image);

        cropImageView.setImageUri(Company_Details.selected_image);
        cropImageView.setAspectRatio(2,1);
        cropImageView.setFixedAspectRatio(true);
        btn_cancel = (FrameLayout)findViewById(R.id.btn_cancel);
        btn_done = (FrameLayout)findViewById(R.id.btn_done);


        btn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Utils.croppedImage = cropImageView.getCroppedImage();
                Utils.cimageUri = getImageUri(getApplicationContext(), Utils.croppedImage);
                Intent in = new Intent();
                setResult(RESULT_OK, in);
                finish();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Confirm_Job_Posting.change_image = false;
                finish();
            }
        });


    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Log.i("Width: ",""+inImage.getWidth());
        if(inImage.getWidth()>933){
            inImage = Bitmap.createScaledBitmap(inImage,933,417,false);
        }
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }
}

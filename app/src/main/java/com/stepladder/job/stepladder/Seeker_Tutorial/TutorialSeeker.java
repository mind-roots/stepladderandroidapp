package com.stepladder.job.stepladder.Seeker_Tutorial;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.stepladder.job.stepladder.Adapters.CustomPagerAdapter;
import com.stepladder.job.stepladder.R;

/**
 * Created by user on 1/18/2016.
 */
public class TutorialSeeker extends FragmentActivity {
    ViewPager viewpager_seeker;
    CustomPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_seeker);

        viewpager_seeker = (ViewPager)findViewById(R.id.viewpager_seeker);

        pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        viewpager_seeker.setAdapter(pagerAdapter);

    }
}

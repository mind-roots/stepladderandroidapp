package com.stepladder.job.stepladder.Seeker;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer.PrivacyPolicy;
import com.stepladder.job.stepladder.Employeer.User_Agreement;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.SplashScreen;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("deprecation")
public class User_Registration extends ActionBarActivity {
    EditText edt_firstname, edt_lastname, edt_dob, edt_phone_no, edt_email,
            edt_password;
    TextView txt_signin_userregi, textView1, textView2, useragreement, txt_privacypolicy, txt_agree, title;
    RelativeLayout lay_user_joinnow;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date1;
    SharedPreferences prefs;
    RestClient client;
    LayoutInflater inflater;
    String error, auth_code, status, email, password, firstname, lastname, phone, dob, currentDate,
            selectedDate, Match_Email_Notification, Match_App_Notification, android_id;
    Menu newmenu;
    boolean isEmail = false, check = false;
    ImageView tick;
    ProgressBar progressEmail;
    ConnectionDetector con;
    public static Bitmap linked;
    private Tracker mTracker;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_registration);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        init();

        //----------------get player id from ONESIGNAL fo notification--------
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                android_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + android_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });

        Intent intent = getIntent();
        try {
            if (intent.getStringExtra("first_name") != null) {
                edt_firstname.setText(intent.getStringExtra("first_name"));
            }
            if (intent.getStringExtra("last_name") != null) {
                edt_lastname.setText(intent.getStringExtra("last_name"));
            }
            if (intent.getStringExtra("email") != null) {
                edt_email.setText(intent.getStringExtra("email"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setFont();
        // *********************************************date picker*********************************
        myCalendar = Calendar.getInstance();
        // final int currentyear = myCalendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        currentDate = df.format(myCalendar.getTime());
        System.out.println("Current Date: " + df.format(myCalendar.getTime()));
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Date date = new Date(year - 1900, monthOfYear, dayOfMonth);
                //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                System.out.println("Selected Date: " + sdf.format(date));
                selectedDate = sdf.format(date);
                java.util.Date sdate = null, cdate = null;
                try {
                    sdate = sdf.parse(selectedDate);
                    cdate = sdf.parse(currentDate);
                    Log.i("Selected: ", "" + sdate + "Current: " + cdate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // int selectedyear = myCalendar.get(Calendar.YEAR);
                if (sdate.equals(cdate)) {
                    Toast.makeText(getApplicationContext(), "You should be at least 16 years of age to sign up!", Toast.LENGTH_SHORT).show();
                } else if (sdate.after(cdate)) {
                    Toast.makeText(getApplicationContext(), "You should be at least 16 years of age to sign up!", Toast.LENGTH_SHORT).show();
                } else if (cdate.getYear() - sdate.getYear() < 16) {
                    Toast.makeText(getApplicationContext(), "You should be at least 16 years of age to sign up!", Toast.LENGTH_SHORT).show();
                } else {
                    edt_dob.setText(sdf.format(date));
                }
            }

        };

        edt_email.setOnFocusChangeListener(EmailFocus);
        edt_email.addTextChangedListener(EmailWatcher);

        clickevents();


    }

    //*********************************************clickevents**************************************
    private void clickevents() {

        txt_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(User_Registration.this, PrivacyPolicy.class);
                ActivityStack.activity.add(User_Registration.this);
                startActivity(intent);
            }
        });

        useragreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(User_Registration.this, User_Agreement.class);
                ActivityStack.activity.add(User_Registration.this);
                startActivity(intent);
            }
        });

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(User_Registration.this, date1, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        lay_user_joinnow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (edt_firstname.length() == 0) {
                    edt_firstname.setError("Required field");
                } else if (edt_lastname.length() == 0) {
                    edt_lastname.setError("Required field");
                } /*else if (edt_dob.length() == 0) {
                    edt_dob.setError("Required field");
                } */
                else if (edt_phone_no.length() == 0) {
                    edt_phone_no.setError("Required field");
                } else if (!isEmail) {
                    check = true;
                    new CheckEmailService().execute();
                    //edt_email.setError("Enter valid email address",null);
                } else if (edt_password.length() == 0) {
                    edt_password.setError("Required field");
                } else if (edt_password.length() < 5) {
                    Toast.makeText(getApplicationContext(), "Password should contain at least 5 characters", Toast.LENGTH_SHORT).show();
                } else {
                    email = edt_email.getText().toString();
                    password = edt_password.getText().toString();
                    firstname = edt_firstname.getText().toString();
                    lastname = edt_lastname.getText().toString();
                    dob = edt_dob.getText().toString();
                    phone = edt_phone_no.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                User_Registration.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new sign_up().execute();
                    }


                }

            }
        });

        txt_signin_userregi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(User_Registration.this, Sign_In.class);
                ActivityStack.activity.add(User_Registration.this);
                intent.putExtra("login", "user");
                startActivity(intent);
                finish();
            }
        });
    }

    //********************************************UI intialization**********************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(getApplicationContext());
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("User Registration");

        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        txt_signin_userregi = (TextView) findViewById(R.id.txt_signin_userregi);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        // txt_joinnow = (TextView) findViewById(R.id.txt_joinnow);
        txt_privacypolicy = (TextView) findViewById(R.id.txt_privacypolicy);
        txt_agree = (TextView) findViewById(R.id.txt_agree);
        lay_user_joinnow = (RelativeLayout) findViewById(R.id.lay_user_joinnow);
        edt_firstname = (EditText) findViewById(R.id.edt_first_name);
        edt_lastname = (EditText) findViewById(R.id.edt_lastname);
        edt_dob = (EditText) findViewById(R.id.edt_dob);
        edt_phone_no = (EditText) findViewById(R.id.edt_phone_no);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        tick = (ImageView) findViewById(R.id.tick);
        useragreement = (TextView) findViewById(R.id.useragreement);
        progressEmail = (ProgressBar) findViewById(R.id.progressEmail);

    }

    //***********************************Setting Fonts for UI components****************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(User_Registration.this), Typeface.BOLD);
        txt_signin_userregi.setTypeface(Utils.setfontlight(User_Registration.this));
        textView1.setTypeface(Utils.setfontlight(User_Registration.this));
        textView2.setTypeface(Utils.setfontlight(User_Registration.this));
        txt_privacypolicy.setTypeface(Utils.setfontlight(User_Registration.this), Typeface.BOLD);
        txt_agree.setTypeface(Utils.setfontlight(User_Registration.this), Typeface.BOLD);
        edt_firstname.setTypeface(Utils.setfontlight(User_Registration.this));
        edt_lastname.setTypeface(Utils.setfontlight(User_Registration.this));
        edt_dob.setTypeface(Utils.setfontlight(User_Registration.this));
        edt_phone_no.setTypeface(Utils.setfontlight(User_Registration.this));
        edt_email.setTypeface(Utils.setfontlight(User_Registration.this));
        edt_password.setTypeface(Utils.setfontlight(User_Registration.this));
        useragreement.setTypeface(Utils.setfontlight(User_Registration.this), Typeface.BOLD);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Register Job Seeker");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //************************************Sign Up AsyncTask Class***********************************
    class sign_up extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(User_Registration.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Signing up...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = signup(password, firstname, lastname, dob, phone, email, android_id, "android");
            Log.i("Response", ":" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                status = jobj.getString("status");
                Log.i("Status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    auth_code = data.getString("AuthCode");
                    Log.i("AuthCode", ":" + auth_code);
                    // profile_image = data.optString("profile_image");
                    Match_Email_Notification = data.getString("Match_Email_Notification");
                    Match_App_Notification = data.getString("Match_App_Notification");
                } else {
                    JSONObject data = jobj.getJSONObject("data");
                    Log.i("Data", ":" + data);
                    error = "";
                    if (data.has("Phone_number")) {
                        String phone = data.getString("Phone_number");
                        error = error + phone;
                    }
                    if (data.has("Password")) {
                        String pass = data.getString("Password");
                        error = error + pass;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                Intent i = new Intent(User_Registration.this,
                        Profile_photo.class);
                i.putExtra("edit", "false");
                ActivityStack.activity.add(User_Registration.this);
                startActivity(i);

                //Add Values to prefs
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("auth_code", auth_code);
                edit.putString("type", "job-seeker");
                edit.putString("first_name", firstname);
                edit.putString("last_name", lastname);
                edit.putString("dob", dob);
                edit.putString("phone_no", phone);
                edit.putString("email", email);
                edit.putString("password", password);
                //edit.putString("profile_image",profile_image);
                edit.putString("email_noti", Match_Email_Notification);
                edit.putString("app_noti", Match_App_Notification);
                edit.putString("confirmed", "0");
                edit.commit();
            } else {
                if (error != "") {
                    dialog("Alert!", error);
                }

            }
        }
    }

    //**************************************Sign Up Method***************************************************

    public String signup(String password, String firstname, String lastname, String dob, String phone, String email, String device_id, String type) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "Password", password)
                .appendQueryParameter("FirstName", firstname)
                .appendQueryParameter("LastName", lastname)
                .appendQueryParameter("Dob", dob)
                .appendQueryParameter("Phone_number", phone)
                .appendQueryParameter("Email", email)
                .appendQueryParameter("DeviceId", device_id)
                .appendQueryParameter("Type", type);
        Log.i("Complete", "Url: " + Utils.sign_up + builder);
        client = new RestClient(Utils.sign_up + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //***********************************Alert Dialog**************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    // ***********************Watch email text*****************
    private final TextWatcher EmailWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            edt_email.setTextColor(Color.BLACK);
            tick.setVisibility(View.INVISIBLE);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    // ******************Detect Email Focus change*********************
    private final View.OnFocusChangeListener EmailFocus = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            // TODO Auto-generated method stub

            if (!hasFocus && edt_email.length() > 1) {
                //Connection Detection
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            User_Registration.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new CheckEmailService().execute();
                }
            }

        }
    };

    // Check Email exists or Not*****

    private class CheckEmailService extends AsyncTask<Void, Void, Void> {

        String status, message, text;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressEmail.setVisibility(View.VISIBLE);
            text = edt_email.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            String response = EmailCheck(text, Utils.CHECK_MAIL);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");
                Log.i("Status: ", "" + status);
                if (status.equals("true")) {
                    // message = obj.getString("data");
                } else {
                    //message = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (status.equals("true")) {
                    isEmail = true;
                    tick.setImageResource(R.drawable.green_tick);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt_email.setTextColor(Color.parseColor("#000000"));

                    if (check) {
                        email = edt_email.getText().toString();
                        password = edt_password.getText().toString();
                        firstname = edt_firstname.getText().toString();
                        lastname = edt_lastname.getText().toString();
                        dob = edt_dob.getText().toString();
                        phone = edt_phone_no.getText().toString();

                        //Connection Detection
                        if (!con.isConnectingToInternet()) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(
                                    User_Registration.this);

                            alert.setTitle("Internet connection unavailable.");
                            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                            alert.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {
                                            startActivity(new Intent(
                                                    Settings.ACTION_WIRELESS_SETTINGS));
                                        }
                                    });

                            alert.show();
                        } else {
                            new sign_up().execute();
                        }

                        check = false;
                    }

                } else {
                    isEmail = false;
                    tick.setImageResource(R.drawable.red_cross);
                    tick.setVisibility(View.VISIBLE);
                    progressEmail.setVisibility(View.INVISIBLE);
                    edt_email.setTextColor(Color.parseColor("#e53b36"));
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

    }

    // ************************Email Check Method************************
    private String EmailCheck(String email, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("Email",
                email);
        // String response = null;
        //response = client.getJSONFromUrl(url, builder);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;

    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

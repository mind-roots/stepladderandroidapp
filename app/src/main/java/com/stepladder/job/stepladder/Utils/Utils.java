package com.stepladder.job.stepladder.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.Person;

import java.util.ArrayList;

public class Utils {

    public static String LINKEDIN_CONSUMER_KEY = "757mamycncj88r";
    public static String LINKEDIN_CONSUMER_SECRET = "q2iOhGweMyRsREGU";
    public static String scopeParams = "rw_nus+r_basicprofile";

    public static String OAUTH_CALLBACK_SCHEME = "x-oauthflow-linkedin";
    public static String OAUTH_CALLBACK_HOST = "callback";
    public static String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://"
            + OAUTH_CALLBACK_HOST;
    //public static String base_url = "http://caclients.net/projects/stepladder/frontend/index.php/web-service/";
    public static String base_url = "https://stepladderapp.com/sladmin/frontend/index.php/web-service/";
    public static String sign_up = base_url + "signup";
    public static String login = base_url + "login";
    public static String profile_image = base_url + "profile-image?";
    public static String location = base_url + "location";
    public static String selected_loc = "";
    public static double lat, lng;
    public static String skillset = base_url + "skillset";
    public static String get_skills = base_url + "getskillss?";
    public static String job_details = base_url + "addjob";
    public static String skills_req = base_url + "addjob-skill";
    public static String past_jobs = base_url + "pastwork";
    public static String get_past_jobs = base_url + "getpastjobs?";
    public static String final_details = base_url + "addjob-final";
    public static String company_details = base_url + "company-detail?";
    public static String update_image = base_url + "updatejobimage?";
    public static String image_url = null;
    public static String company_img = null;
    public static String job_confirm = base_url + "job-confirm";
    public static String CHECK_MAIL = base_url + "checkemail";
    public static String getskills = base_url + "getskills?";
    public static String get_current_jobs = base_url + "current-jobs";
    public static String sectors = base_url + "getsector?";
    public static String job_status = base_url + "updatejob-status";
    public static String change_company_name = base_url + "change-company-name";
    public static String change_employer_name = base_url + "change-name";
    public static String change_employer_pass = base_url + "change-password";
    public static String change_employer_email = base_url + "change-email";
    public static String change_employer_phone = base_url + "change-phone-number";
    public static String logout = base_url + "logout";
    public static String delete_matches = base_url + "delete-job";
    public static String delete_account = base_url + "delete-account";
    public static String update_notification = base_url + "update-notifications";
    public static String get_jobs = base_url + "jobseeker-dashboard?";
    public static String apply_job = base_url + "apply-job";
    public static String all_candidates = base_url + "delivery-driver";
    public static String get_user_profile = base_url + "potential-candidate";
    public static String select_candidate = base_url + "select-candidate";
    public static String view_matches = base_url + "view-matches";
    public static String confirm_profile = base_url + "confirmed-profile?";
    public static String set_error = base_url + "seterrors?";
    public static String forgot_pass = base_url + "forgotpassword?";
    public static String get_job_details = base_url + "getsinglejob";
    public static Uri pimageUri, cimageUri;
    public static ArrayList<ModelJobsCard> get_job = new ArrayList<>();
    public static ArrayList<ModelUIData> final_ui_data = new ArrayList<ModelUIData>();
    public static ArrayList<Person> userlist = new ArrayList<Person>();
    public static Bitmap croppedImage;
    static ConnectionDetector con;

    //***************************************functionNormalize**************************************
    public static float functionNormalize(int max, int min, int value) {
        int intermediateValue = max - min;
        value = intermediateValue;
        float var = Math.abs((float) value / (float) intermediateValue);
        return Math.abs((float) value / (float) intermediateValue);
    }

    //************************************Setting Font in application*******************************
    public static Typeface setfontlight(Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "text_light_font.ttf");
        return font;
    }

    //*****************************************Hide keypad******************************************
    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //*****************************************Internet Check Method********************************
    public static boolean connectionCheck(final Activity activity, final Context context) {
        con = new ConnectionDetector(context);
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    activity);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            context.startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            return true;
        }
        return true;
    }

}

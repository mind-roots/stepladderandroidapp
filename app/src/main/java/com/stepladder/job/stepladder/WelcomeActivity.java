package com.stepladder.job.stepladder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.Employeer.Employeer_Registration;
import com.stepladder.job.stepladder.Seeker.Sign_In;
import com.stepladder.job.stepladder.Utils.Utils;

import static com.stepladder.job.stepladder.R.id.lay_facebook_button;
import static com.stepladder.job.stepladder.R.id.txt_importln;

/**
 * Created by user on 9/27/2016.
 */

public class WelcomeActivity extends Activity {
    TextView txt_swipe, txt_signin_userregi, txt_seeker, txt_employer;
    RelativeLayout lay_jobseeker_button, lay_employer_button;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        init();

        setFonts();

        onClicks();

    }

    private void init() {
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        txt_swipe = (TextView) findViewById(R.id.txt_swipe);
        txt_seeker = (TextView) findViewById(R.id.txt_seeker);
        txt_employer = (TextView) findViewById(R.id.txt_employer);
        txt_signin_userregi = (TextView) findViewById(R.id.txt_signin_userregi);
        lay_jobseeker_button = (RelativeLayout) findViewById(R.id.lay_jobseeker_button);
        lay_employer_button = (RelativeLayout) findViewById(R.id.lay_employer_button);
    }

    private void setFonts() {
        txt_swipe.setTypeface(Utils.setfontlight(WelcomeActivity.this));
        txt_signin_userregi.setTypeface(Utils.setfontlight(WelcomeActivity.this));
        txt_seeker.setTypeface(Utils.setfontlight(WelcomeActivity.this));
        txt_employer.setTypeface(Utils.setfontlight(WelcomeActivity.this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Welcome");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void onClicks() {
        txt_signin_userregi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, Sign_In.class);
                ActivityStack.activity.add(WelcomeActivity.this);
                intent.putExtra("login", "user");
                startActivity(intent);
            }
        });
        lay_jobseeker_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeActivity.this, MainActivity.class);
                ActivityStack.activity.add(WelcomeActivity.this);
                startActivity(i);
            }
        });
        lay_employer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeActivity.this, Employeer_Registration.class);
                ActivityStack.activity.add(WelcomeActivity.this);
                startActivity(i);
            }
        });
    }

}

package com.stepladder.job.stepladder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

//import com.facebook.android.Facebook;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.stepladder.job.stepladder.Employeer.Employeer_Registration;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Seeker.Sign_In;
import com.stepladder.job.stepladder.Seeker.User_Registration;
import com.stepladder.job.stepladder.Utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.stepladder.job.stepladder.R.id.txt_signin;

public class MainActivity extends Activity {
    RelativeLayout lay_email_button, lay_linkedin_button, lay_facebook_button;
    TextView txt_swipe, txt_employeer, postjob, txt_importln, txt_importfb,
            txt_sign_email, title, txt_or,txt_signin_userregi;
    ImageView img_back;
    public static ProgressBar progressBar;
    SharedPreferences prefs;
    public static String profile = null;
    String first_name, last_name, email;
    public static String s, id;
    private static final String host = "api.linkedin.com";
    private static final String topCardUrl = "https://" + host + "/v1/people/~:(email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(Utils.LINKEDIN_CONSUMER_KEY,
            Utils.LINKEDIN_CONSUMER_SECRET);
    LinkedInApiClient client;
    LinkedInAccessToken accessToken = null;
    CallbackManager callbackManager;
    ConnectionDetector con;
    private Tracker mTracker;
    // String abc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        // abc = abc.replace("", "-");
        showHashKey(MainActivity.this);
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        init();

        setFonts();

        Utils.userlist.clear();
        Utils.final_ui_data.clear();

        clickevents();

    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        // TODO Auto-generated method stub

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lay_email_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(MainActivity.this, User_Registration.class);
                ActivityStack.activity.add(MainActivity.this);
                startActivity(i);
            }
        });

        lay_linkedin_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            MainActivity.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    linkedInLogin();
                }
            }
        });

        lay_facebook_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            MainActivity.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    loginToFacebook();
                }
            }
        });

        txt_signin_userregi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, Sign_In.class);
                ActivityStack.activity.add(MainActivity.this);
                intent.putExtra("login", "main");
                startActivity(intent);
            }
        });

        postjob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent k = new Intent(MainActivity.this, Employeer_Registration.class);
                ActivityStack.activity.add(MainActivity.this);
                startActivity(k);
            }
        });
    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(MainActivity.this);
        img_back = (ImageView)findViewById(R.id.img_back);
        lay_email_button = (RelativeLayout) findViewById(R.id.lay_email_button);
        lay_facebook_button = (RelativeLayout) findViewById(R.id.lay_facebook_button);
        lay_linkedin_button = (RelativeLayout) findViewById(R.id.lay_linkedin_button);
        postjob = (TextView) findViewById(R.id.postjob);
        txt_swipe = (TextView) findViewById(R.id.txt_swipe);
        txt_employeer = (TextView) findViewById(R.id.txt_employeer);
        postjob = (TextView) findViewById(R.id.postjob);
        txt_importfb = (TextView) findViewById(R.id.txt_importfb);
        txt_importln = (TextView) findViewById(R.id.txt_importln);
        txt_or = (TextView) findViewById(R.id.txt_or);
        txt_sign_email = (TextView) findViewById(R.id.txt_sign_email);
        txt_signin_userregi = (TextView) findViewById(R.id.txt_signin_userregi);
        // step_title = (TextView)findViewById(R.id.step_title);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        callbackManager = CallbackManager.Factory.create();
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
    }

    //********************************************Set Fonts*************************l****************
    public void setFonts() {
        txt_or.setTypeface(Utils.setfontlight(MainActivity.this), Typeface.BOLD);
        txt_importln.setTypeface(Utils.setfontlight(MainActivity.this));
        txt_importfb.setTypeface(Utils.setfontlight(MainActivity.this));
        txt_sign_email.setTypeface(Utils.setfontlight(MainActivity.this));
        txt_employeer.setTypeface(Utils.setfontlight(MainActivity.this), Typeface.BOLD);
        txt_swipe.setTypeface(Utils.setfontlight(MainActivity.this));
        postjob.setTypeface(Utils.setfontlight(MainActivity.this));
        txt_signin_userregi.setTypeface(Utils.setfontlight(MainActivity.this));
    }

    // ***************************************************LinkedIn Login****************************
    private void linkedInLogin() {
        getUserData();
    }

    // *********************************************Facebook Login**********************************
    public void loginToFacebook() {
//        if (AccessToken.getCurrentAccessToken() != null) {
//            Log.i("Fb Access Token: ", "" + AccessToken.getCurrentAccessToken().getToken());
//            RequestData();
//        }

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        RequestData();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                    }
                });
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                Log.i("JSON: ", "" + json);
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.optString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        Log.i("Response", "data: " + text);
                        JSONObject jobj = json.getJSONObject("picture");
                        JSONObject data = jobj.getJSONObject("data");
                        String url = data.getString("url");
                        Log.i("Image Response", "" + url);
                        first_name = json.getString("name");
                        String arr[] = first_name.split(" ");
                        first_name = arr[0];
                        last_name = arr[1];
                        Log.i("Name", "data: " + first_name + "" + last_name);
                        profile = url;
                       // Utils.image_url = profile;
                        email = json.optString("email");
                        Intent i = new Intent(MainActivity.this, User_Registration.class);
                        i.putExtra("first_name", first_name);
                        i.putExtra("last_name", last_name);
                        i.putExtra("email", email);
                        ActivityStack.activity.add(MainActivity.this);
                        startActivity(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,picture.type(large),email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //*************************************onActivityResult*****************************************
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    }

    public void getUserData() {

        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {

                // Toast.makeText(getApplicationContext(), "success" + LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString(), Toast.LENGTH_LONG).show();

                APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(MainActivity.this, topCardUrl, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse result) {
                        try {
                            Log.i("LinkedIn Success:", "" + result);
                            setUserProfile(result.getResponseDataAsJson());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onApiError(LIApiError error) {
                        Log.i("LinkedIn Error:", "" + error);
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {

                Toast.makeText(getApplicationContext(), "failed " + error.toString(), Toast.LENGTH_LONG).show();
            }
        }, true);


    }


    public void setUserProfile(JSONObject response) {

        try {
            first_name = response.get("formattedName").toString();
            String arr[] = first_name.split(" ");
            first_name = arr[0];
            last_name = arr[1];
            Log.i("Name", "data: " + first_name + "" + last_name);
            email = response.get("emailAddress").toString();
            Log.i("LinkedIn Profile:", "" + response.getString("pictureUrls"));
            JSONObject picture = response.optJSONObject("pictureUrls");
            JSONArray value = picture.optJSONArray("values");
            if(value.length()>0) {
                for (int i = 0; i < value.length(); i++) {
                    profile = value.get(i).toString();
                    Log.i("Linked Profile",""+profile);
                }
            }

            Intent i = new Intent(MainActivity.this, User_Registration.class);
            i.putExtra("first_name", first_name);
            i.putExtra("last_name", last_name);
            i.putExtra("email", email);
            ActivityStack.activity.add(MainActivity.this);
            startActivity(i);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // **********************************************Show Hashkey***********************************
    private void showHashKey(MainActivity mainActivity) {
        // TODO Auto-generated method stub
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.stepladder.job.stepladder", PackageManager.GET_SIGNATURES); // Your
            // here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("KeyHash:: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Jobseeker Welcome");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}

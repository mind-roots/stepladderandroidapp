package com.stepladder.job.stepladder.Employeer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.Adapters.AcceptedListAdapter;
import com.stepladder.job.stepladder.Adapters.PendingListAdapter;
import com.stepladder.job.stepladder.Adapters.RejectedListAdapter;
import com.stepladder.job.stepladder.Model.CandidatesModel;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 10/26/2015.
 */
public class Job_Status extends ActionBarActivity {
    RecyclerView recyclerView, my_recycler_view_pending, my_recycler_view_rejected;
    AcceptedListAdapter acceptedListAdapter;
    PendingListAdapter pendingListAdapter;
    RejectedListAdapter rejectedListAdapter;
    ArrayList<CandidatesModel> all_candidates = new ArrayList<CandidatesModel>();
    ArrayList<CandidatesModel> accepted_candidates = new ArrayList<CandidatesModel>();
    ArrayList<CandidatesModel> rejected_candidates = new ArrayList<CandidatesModel>();
    LinearLayoutManager mLayoutManager, mLayoutManager1, mLayoutManager2;
    RelativeLayout rl_pending, rl_accepted, rl_rejected;
    LinearLayout mainLayout;
    TextView txt_pending, txt_accepted, txt_rejected, title, no_data;
    LayoutInflater inflater;
    public static String job_id, job_title, status;
    Menu newmenu;
    RestClient client;
    public static ProgressBar progressBar;
    SharedPreferences prefs;
    public static boolean accept = false, reject = false,move = false;
    ConnectionDetector con;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_status);

        //Get Intent Values
        Intent intent = getIntent();
        job_id = intent.getStringExtra("job_id");
        job_title = intent.getStringExtra("job_title");
        Log.i("Get Data: ", "Id: " + job_id + "Title: " + job_title);

        init();

        setFont();

        DashBoard_Employeer.open = true;
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Job_Status.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get All Candidates
            all_candidates.clear();
            accepted_candidates.clear();
            rejected_candidates.clear();
            new get_all_candidates().execute();
        }
        clickevents();

    }

    //***********************Setting Fonts for UI components******************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Job_Status.this), Typeface.BOLD);
        txt_accepted.setTypeface(Utils.setfontlight(Job_Status.this));
        txt_pending.setTypeface(Utils.setfontlight(Job_Status.this));
        txt_rejected.setTypeface(Utils.setfontlight(Job_Status.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        rl_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pending List n Adapter
                if (all_candidates.size() > 0) {
                    my_recycler_view_pending.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    my_recycler_view_rejected.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                    pendingListAdapter = new PendingListAdapter(getApplicationContext(), all_candidates);
                    my_recycler_view_pending.setAdapter(pendingListAdapter);
                } else {
                    my_recycler_view_pending.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    my_recycler_view_rejected.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    no_data.setText("You have no pending applicants to show at this time.");
                }
                rl_accepted.setBackgroundResource(R.drawable.white_bg);
                rl_rejected.setBackgroundResource(R.drawable.white_bg);
                rl_pending.setBackgroundColor(Color.parseColor("#5190d7"));
                txt_accepted.setTextColor(Color.parseColor("#295481"));
                txt_rejected.setTextColor(Color.parseColor("#295481"));
                txt_pending.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });

        rl_accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Accepted List n Adapter
                if (accepted_candidates.size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    my_recycler_view_pending.setVisibility(View.GONE);
                    my_recycler_view_rejected.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                    acceptedListAdapter = new AcceptedListAdapter(getApplicationContext(), accepted_candidates);
                    recyclerView.setAdapter(acceptedListAdapter);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    my_recycler_view_pending.setVisibility(View.GONE);
                    my_recycler_view_rejected.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    no_data.setText("You have no accepted applicants to show at this time.");
                }
                rl_accepted.setBackgroundColor(Color.parseColor("#5190d7"));
                rl_rejected.setBackgroundResource(R.drawable.white_bg);
                rl_pending.setBackgroundResource(R.drawable.white_bg);
                txt_accepted.setTextColor(Color.parseColor("#FFFFFF"));
                txt_rejected.setTextColor(Color.parseColor("#295481"));
                txt_pending.setTextColor(Color.parseColor("#295481"));
            }
        });

        rl_rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Rejected List n Adapter
                if (rejected_candidates.size() > 0) {
                    my_recycler_view_rejected.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    my_recycler_view_pending.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                    rejectedListAdapter = new RejectedListAdapter(getApplicationContext(), rejected_candidates);
                    my_recycler_view_rejected.setAdapter(rejectedListAdapter);
                } else {
                    my_recycler_view_rejected.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    my_recycler_view_pending.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    no_data.setText("You have no rejected applicants to show at this time");
                }
                rl_accepted.setBackgroundResource(R.drawable.white_bg);
                rl_rejected.setBackgroundColor(Color.parseColor("#5190d7"));
                rl_pending.setBackgroundResource(R.drawable.white_bg);
                txt_accepted.setTextColor(Color.parseColor("#295481"));
                txt_rejected.setTextColor(Color.parseColor("#FFFFFF"));
                txt_pending.setTextColor(Color.parseColor("#295481"));
            }
        });

    }

    //****************************************UI Initializations************************************
    private void init() {
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(Job_Status.this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText(job_title);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        my_recycler_view_pending = (RecyclerView) findViewById(R.id.my_recycler_view_pending);
        my_recycler_view_rejected = (RecyclerView) findViewById(R.id.my_recycler_view_rejected);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        my_recycler_view_pending.setLayoutManager(mLayoutManager1);
        mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        my_recycler_view_rejected.setLayoutManager(mLayoutManager2);
        rl_pending = (RelativeLayout) findViewById(R.id.rl_pending);
        rl_accepted = (RelativeLayout) findViewById(R.id.rl_accepted);
        rl_rejected = (RelativeLayout) findViewById(R.id.rl_rejected);
        txt_pending = (TextView) findViewById(R.id.txt_pending);
        txt_accepted = (TextView) findViewById(R.id.txt_accepted);
        txt_rejected = (TextView) findViewById(R.id.txt_rejected);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        no_data = (TextView) findViewById(R.id.no_data);
        mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Leads");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //**************************************Async Task Class****************************************

    class get_all_candidates extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = Get_Candidates(prefs.getString("auth_code", null), job_id);

            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");

                JSONObject data = obj.getJSONObject("data");
                JSONArray pending = data.optJSONArray("pending");
                if (pending.length() != 0) {
                    for (int i = 0; i < pending.length(); i++) {
                        JSONObject jobj = pending.getJSONObject(i);
                        CandidatesModel model = new CandidatesModel();
                        model.user_id = jobj.getString("UserId");
                        model.created_at = jobj.getString("Created_at");
                        String fname = jobj.getString("FirstName");
                        String lname = jobj.getString("LastName");
                        model.name = fname + " " + lname;
                        model.address = jobj.getString("LocationName");
                        model.user_image = jobj.getString("profile_image");
                        all_candidates.add(model);
                        Log.i("List Size: ", "" + all_candidates.size());
                    }
                }

                JSONArray accepted = data.optJSONArray("accepted");
                if (accepted.length() != 0) {
                    for (int i = 0; i < accepted.length(); i++) {
                        JSONObject jobj = accepted.getJSONObject(i);
                        CandidatesModel model = new CandidatesModel();
                        model.user_id = jobj.getString("UserId");
                        model.created_at = jobj.getString("Created_at");
                        String fname = jobj.getString("FirstName");
                        String lname = jobj.getString("LastName");
                        model.name = fname + " " + lname;
                        model.address = jobj.getString("LocationName");
                        model.user_image = jobj.getString("profile_image");
                        accepted_candidates.add(model);
                        Log.i("List Size: ", "" + accepted_candidates.size());
                    }
                }

                JSONArray rejected = data.optJSONArray("rejected");
                if (rejected.length() != 0) {
                    for (int i = 0; i < rejected.length(); i++) {
                        JSONObject jobj = rejected.getJSONObject(i);
                        CandidatesModel model = new CandidatesModel();
                        model.user_id = jobj.getString("UserId");
                        model.created_at = jobj.getString("Created_at");
                        String fname = jobj.getString("FirstName");
                        String lname = jobj.getString("LastName");
                        model.name = fname + " " + lname;
                        model.address = jobj.getString("LocationName");
                        model.user_image = jobj.getString("profile_image");
                        rejected_candidates.add(model);
                        Log.i("List Size: ", "" + rejected_candidates.size());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equalsIgnoreCase("true")) {
                mainLayout.setVisibility(View.VISIBLE);
                if(move){
                    move = false;
                    if (accepted_candidates.size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        my_recycler_view_pending.setVisibility(View.GONE);
                        my_recycler_view_rejected.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                        acceptedListAdapter = new AcceptedListAdapter(getApplicationContext(), accepted_candidates);
                        recyclerView.setAdapter(acceptedListAdapter);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        my_recycler_view_pending.setVisibility(View.GONE);
                        my_recycler_view_rejected.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        no_data.setText("You have no accepted applicants to show at this time.");
                    }
                    rl_accepted.setBackgroundColor(Color.parseColor("#5190d7"));
                    rl_rejected.setBackgroundResource(R.drawable.white_bg);
                    rl_pending.setBackgroundResource(R.drawable.white_bg);
                    txt_accepted.setTextColor(Color.parseColor("#FFFFFF"));
                    txt_rejected.setTextColor(Color.parseColor("#295481"));
                    txt_pending.setTextColor(Color.parseColor("#295481"));
                }else {
                    //Pending List n Adapter
                    if (all_candidates.size() > 0) {
                        my_recycler_view_pending.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        my_recycler_view_rejected.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                        pendingListAdapter = new PendingListAdapter(getApplicationContext(), all_candidates);
                        my_recycler_view_pending.setAdapter(pendingListAdapter);
                    } else {
                        my_recycler_view_pending.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        my_recycler_view_rejected.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        no_data.setText("You have no pending applicants to show at this time.");
                    }
                }
            } else {
                mainLayout.setVisibility(View.VISIBLE);
                my_recycler_view_pending.setVisibility(View.GONE);
                no_data.setVisibility(View.VISIBLE);
            }
        }
    }

    //**************************************Get All Candidates**************************************
    public String Get_Candidates(String auth_code, String JobId) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("JobId", JobId);
        Log.i("Complete", "Url: " + Utils.all_candidates + builder);
        client = new RestClient(Utils.all_candidates + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (accept || reject) {
            accept = false;
            reject = false;
            //Get All Candidates
            all_candidates.clear();
            accepted_candidates.clear();
            rejected_candidates.clear();
            new get_all_candidates().execute();
        }

    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

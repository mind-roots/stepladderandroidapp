package com.stepladder.job.stepladder.Seeker;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.ContactPickerAdapter;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.TokenGeneration.ContactsCompletionView;
import com.stepladder.job.stepladder.TokenGeneration.FilteredArrayAdapter;
import com.stepladder.job.stepladder.TokenGeneration.TokenCompleteTextView;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class Skill_set extends ActionBarActivity implements TokenCompleteTextView.TokenListener {
    RelativeLayout lay_skill_btn;
    ListView l;
    ImageView imageView3;
    EditText edt_tell_potential, edt_skills;
    LinearLayout third;
    Menu newmenu;
    LayoutInflater inflater;
    RestClient client;
    ArrayList<String> list = new ArrayList<String>();
    int position, id = 0;
    TextView textView1, textView2, btn_txt, title, txt_count, title_count;
    ContactsCompletionView completionView;
    ContactPickerAdapter adapter;
    ArrayList<Person> userlist = new ArrayList<Person>();
    String skills, bio, status, skill_name, edit, selectedItem;
    SharedPreferences prefs;
    StringBuilder sb, sb1;
    int count = 0;
    ConnectionDetector con;
    boolean isDuplicate = false;
    android.support.v7.app.AlertDialog.Builder builder;
    String[] arr;
    int mile = -1;
    boolean[] selected_array;
    ArrayList<String> multi_set = new ArrayList<String>();
    ArrayList<String> id_set = new ArrayList<String>();
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skill_set);

        //Get Intent
        Intent intent = getIntent();
        edit = intent.getStringExtra("edit");

        init();

        setFont();

        userlist.clear();

        //Set Skills
        if (edit.equals("true")) {
            DashBoard_Seeker.open = true;
            for (int i = 0; i < Utils.userlist.size(); i++) {
                Person p = new Person(Utils.userlist.get(i).getUserid(), Utils.userlist.get(i).getUsername());
                completionView.addObject(p, "");
                completionView.setTokenListener(Skill_set.this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            }

        }else {
            for (int i = 0; i < Utils.userlist.size(); i++) {
                Person p = new Person(Utils.userlist.get(i).getUserid(), Utils.userlist.get(i).getUsername());
                completionView.addObject(p, "");
                completionView.setTokenListener(Skill_set.this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            }
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Skill_set.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            new skill_req().execute();

        }
        completionView.setKeyListener(null);
        clickevents();

    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Skill Set");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        lay_skill_btn = (RelativeLayout) findViewById(R.id.lay_skill_btn);
        third = (LinearLayout) findViewById(R.id.third);
        edt_tell_potential = (EditText) findViewById(R.id.edt_tell_potential);
        edt_skills = (EditText) findViewById(R.id.autoCompleteTextView1);
        completionView = (ContactsCompletionView) findViewById(R.id.searchView);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
        txt_count = (TextView) findViewById(R.id.txt_count);
        title_count = (TextView) findViewById(R.id.title_count);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        if (edit.equals("true")) {
            btn_txt.setText("Update Skills");
            imageView3.setVisibility(View.INVISIBLE);
            edt_tell_potential.setText(prefs.getString("bio", null));
            edt_tell_potential.setSelection(edt_tell_potential.length());
            txt_count.setText(String.valueOf(edt_tell_potential.length()));
        }else {
            edt_tell_potential.setText(prefs.getString("bio", null));
            edt_tell_potential.setSelection(edt_tell_potential.length());
            txt_count.setText(String.valueOf(edt_tell_potential.length()));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Job Seeker Skill Set");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //***********************Setting Fonts for UI components******************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Skill_set.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Skill_set.this));
        textView1.setTypeface(Utils.setfontlight(Skill_set.this));
        textView2.setTypeface(Utils.setfontlight(Skill_set.this));
        edt_skills.setTypeface(Utils.setfontlight(Skill_set.this));
        edt_tell_potential.setTypeface(Utils.setfontlight(Skill_set.this));
        txt_count.setTypeface(Utils.setfontlight(Skill_set.this));
        title_count.setTypeface(Utils.setfontlight(Skill_set.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        edt_skills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                multi_set.clear();
//                id_set.clear();
//                Arrays.fill(selected_array, Boolean.FALSE);
//                updateTokenConfirmation();
//                skills = sb.toString();
//                Log.i("skills: ",""+skills);
//                if (skills != null) {
//                    String[] ids = skills.split(",");
//                    for (int j = 0; j < userlist.size(); j++) {
//                        if (ids.length > 0) {
//                            for (int i = 0; i < ids.length; i++) {
//                                if (userlist.get(j).getUserid().equals(ids[i])) {
//                                    multi_set.add(userlist.get(j).getUsername());
//                                    id_set.add(userlist.get(j).getUserid());
//                                    selected_array[j] = Boolean.TRUE;
//                                }
//                            }
//                        }
//                    }
//                }
//                MultiDialog(getApplicationContext(), arr, "", selected_array);

                MyDialogSingle(getApplicationContext(),arr,"",edt_skills,mile,"");

            }
        });

        lay_skill_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                updateTokenConfirmation();
                skills = sb.toString();
                skill_name = sb1.toString();

                if (skills.equals("")) {
                    edt_skills.setError("Required Field");
                } else if (edt_tell_potential.length() == 0) {
                    edt_tell_potential.setError("Required Field");
                } else {

                    if (skills != null) {
                        String[] ids = skills.split(",");
                        if (ids.length > 0) {
                            for (int i = 0; i < ids.length; i++) {

                            }
                        }
                    }

                    bio = edt_tell_potential.getText().toString();

                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Skill_set.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new skillset().execute();
                    }
                }
            }
        });

        edt_tell_potential.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, int start, int before,
                                      int count) {

                // TODO Auto-generated method stub
                count = s.length();
                txt_count.setText(String.valueOf(s.length()));
                if (count >= 250) {
                    Utils.hideKeyboard(Skill_set.this);
                } else {


                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });

    }

    //***********************************Skill Set Async Classes************************************
    class skill_req extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Skill_set.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            System.out.println("prefs auth_code" + prefs.getString("auth_code", null));
            String response = Utils.getskills.concat("AuthCode=").concat(prefs.getString("auth_code", null)).concat("&Skills=");
            Log.i("Skill_set***** ", ":" + response);
            client = new RestClient(response);
            String r = null;
            r = client.executeGet();
            Log.i("RRRRRRR***** ", ":" + r);
            try {
                JSONObject jobj = new JSONObject(r);
                status = jobj.getString("status");
                Log.i("status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = jobj.getJSONArray("data");
                    Log.i("Data", ":" + data);
                    if (data.length() > 0) {
                        arr = new String[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jobj1 = data.getJSONObject(i);
                            String Id = jobj1.getString("Id");
                            String Name = jobj1.getString("Name");
                            System.out.println("Name:::Name::::" + Name);
                            Person item = new Person(Id, Name);
                            userlist.add(item);
                            arr[i] = Name;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                selected_array = new boolean[arr.length];
                for (int i = 0; i < selected_array.length; i++) {
                    Arrays.fill(selected_array, Boolean.FALSE);
                }
            } else {
                //dialog("Alert!", error);
            }
        }
    }


    class skillset extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Skill_set.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving Info...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "Skills: " + skills + "Bio: " + bio);
            String response = SetSkills(prefs.getString("auth_code", null), skills, bio);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                if (edit.equals("true")) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("skills", skill_name);
                    editor.putString("bio", bio);
                    editor.commit();
                    dialog("Success!", "Your skills have been updated successfully");
                } else {
                    Intent i = new Intent(Skill_set.this, Confirm_Profile.class);
                    i.putExtra("edit", "false");
                    ActivityStack.activity.add(Skill_set.this);
                    startActivity(i);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("skills", skill_name);
                    editor.putString("bio", bio);
                    editor.putString("confirmed","0");
                    editor.commit();
                }
            } else {
                dialog("Alert!", "Error in saving data!");
            }
        }
    }

    //***************************************Skill Set Method***************************************
    public String SetSkills(String auth_code, String Skills, String Biography) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("Skills", Skills)
                .appendQueryParameter("Biography", Biography);
        Log.i("Complete", "Url: " + Utils.skillset + builder);
        client = new RestClient(Utils.skillset + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //***********************************************My Dialog**************************************

    private void MultiDialog(final Context ctx, final String[] charSequences,
                             final String title, final boolean[] selected_array) {
//        multi_set.clear();
//        id_set.clear();
       // Arrays.fill(selected_array, Boolean.FALSE);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Skill_set.this,
                R.style.DialogTheme);
        builder.setTitle(title);

        builder.setMultiChoiceItems(charSequences, selected_array,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                                        boolean isChecked) {
//                        if (isChecked) {
//                            multi_set.add(charSequences[which]);
//                        }else {
//                            multi_set.remove(charSequences[which]);
//                        }

                    }
                });

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for (int i = 0; i < selected_array.length; i++) {
                            if (selected_array[i]) {
                                multi_set.add(charSequences[i]);
                                id_set.add(userlist.get(i).getUserid());
                            }
                        }

                        if (multi_set.size() == 0) {

                        } else if (multi_set.size() > 3) {
                            Toast.makeText(getApplicationContext(), "You can add upto 3 skills", Toast.LENGTH_SHORT).show();
                        } else {
                            for (int pos = 0; pos < multi_set.size(); pos++) {
                                Person item = new Person(id_set.get(pos), multi_set.get(pos));
                                Utils.userlist.add(item);
                                completionView.addObject(item, "");

                            }

                            dialog.dismiss();

                            completionView.setTokenListener(Skill_set.this);
                            completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
                        }

                    }
                });

//        builder.setNegativeButton("CANCEL",
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        // TODO Auto-generated method stub
//
//                    }
//                });

        builder.show();
    }

    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               String title, final EditText select1, final int value,
                               final String string) {


        builder = new android.support.v7.app.AlertDialog.Builder(Skill_set.this, R.style.DialogTheme);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //mile = which;
                        selectedItem = charSequences[which];
                        for (int pos = 0; pos < arr.length; pos++) {
                            if (selectedItem.equals(arr[pos])) {
                                position = pos;
                                Person item = new Person(userlist.get(pos).getUserid(), userlist.get(pos).getUsername());
                                //Utils.userlist.add(item);
                                if (count >= 3) {
                                    Utils.hideKeyboard(Skill_set.this);
                                    Toast.makeText(getApplicationContext(), "You can add upto 3 skills", Toast.LENGTH_SHORT).show();
                                } else {
                                    updateTokenConfirmation();
                                    skills = sb.toString();
                                    // Toast.makeText(getApplicationContext(),"Value"+skills,Toast.LENGTH_SHORT).show();
                                    String[] ids = skills.split(",");
                                    if (ids.length == 0) {
                                        completionView.addObject(item, "");
                                    } else {
                                        for (int i = 0; i < ids.length; i++) {
                                            Log.i("SSSSSSSSSSSSS", "" + userlist.get(pos).getUserid());
                                            Log.i("DDDDDDDDDDDDD", "" + ids[i]);
                                            if (userlist.get(pos).getUserid().equals(ids[i])) {
                                                isDuplicate = true;
                                                break;
                                            } else {
                                                isDuplicate = false;

                                            }
                                        }
                                        if (isDuplicate) {
                                            Toast.makeText(getApplicationContext(), "Skill already exists", Toast.LENGTH_SHORT).show();
                                            Utils.hideKeyboard(Skill_set.this);
                                        } else {
                                            completionView.addObject(item, "");
                                        }
                                    }
                                }
                            }
                            //select1.setText(selectedItem);
                            dialog.dismiss();
                        }

                        completionView.setTokenListener(Skill_set.this);
                        completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
                    }
                }
        );

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }

        );
        builder.show();
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*************************************Alert Dialog*********************************************

    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //***************************************Token Method*******************************************
    private void updateTokenConfirmation() {
        sb = new StringBuilder();
        sb1 = new StringBuilder();
        Utils.userlist.clear();
        for (Person token : completionView.getObjects()) {
            sb.append(token.getUserid().toString());
            sb.append(",");
            sb1.append(token.getUsername().toString());
            sb1.append(", ");
            Person item = new Person(token.getUserid().toString(), token.getUsername().toString());
            Utils.userlist.add(item);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb1.deleteCharAt(sb1.lastIndexOf(","));

        }
    }

    @Override
    public void onTokenAdded(Object token) {
        updateTokenConfirmation();
        count++;
    }

    @Override
    public void onTokenRemoved(Object token) {
        updateTokenConfirmation();
        count--;
        if (count == 0) {
            completionView.setText("");
        }
    }
}

package com.stepladder.job.stepladder.Employeer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.ContactPickerAdapter;
import com.stepladder.job.stepladder.MainActivity;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.TokenGeneration.ContactsCompletionView;
import com.stepladder.job.stepladder.TokenGeneration.FilteredArrayAdapter;
import com.stepladder.job.stepladder.TokenGeneration.TokenCompleteTextView;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;


public class Skills_Required extends ActionBarActivity implements TokenCompleteTextView.TokenListener {
    RelativeLayout lay_skill_btn;
    EditText autoCompleteTextView_skills;
    LinearLayout ll_add_skills;
    LayoutInflater inflater;
    ArrayList<String> list = new ArrayList<String>();
    SharedPreferences prefs;
    RestClient client;
    StringBuilder sb, sb1;
    String status, skills, skills_name, selectedItem;
    int position;
    public static ArrayList<Person> userlist = new ArrayList<Person>();
    int count, id = 0;
    TextView text, title, skillrequired, btn_txt;
    Menu newmenu;
    public static ContactsCompletionView completionView;
    ConnectionDetector con;
    ArrayAdapter<Person> adapter;
    boolean isDuplicate = false;
    android.support.v7.app.AlertDialog.Builder builder;
    String[] arr;
    int mile = -1;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skills_required);

        init();

        userlist.clear();

        //Set Prefs Data
        if (Utils.userlist.size() > 0) {
            for (int i = 0; i < Utils.userlist.size(); i++) {
                Person p = new Person(Utils.userlist.get(i).getUserid(), Utils.userlist.get(i).getUsername());
                completionView.addObject(p, "");
                completionView.setTokenListener(Skills_Required.this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            }
        }

        //Connection Detection
        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    Skills_Required.this);

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            new skill_req().execute();
        }
        setFont();

        completionView.setKeyListener(null);

        clickevents();


    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        OneSignalClass application = (OneSignalClass) getApplication();
        mTracker = application.getDefaultTracker();
        con = new ConnectionDetector(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Skills Required");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        lay_skill_btn = (RelativeLayout) findViewById(R.id.lay_skill_btn);
        autoCompleteTextView_skills = (EditText) findViewById(R.id.autoCompleteTextView_skills);
        ll_add_skills = (LinearLayout) findViewById(R.id.ll_add_skills);
        completionView = (ContactsCompletionView) findViewById(R.id.searchView);
        skillrequired = (TextView) findViewById(R.id.skillrequired);
        btn_txt = (TextView) findViewById(R.id.btn_txt);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Company Skills Required");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //*******************************************clickevents****************************************
    private void clickevents() {

        autoCompleteTextView_skills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogSingle(getApplicationContext(), arr, "", autoCompleteTextView_skills, mile, "");
            }
        });


        lay_skill_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                updateTokenConfirmation();
                skills = sb.toString();
                skills_name = sb1.toString();
                if (skills.equals("")) {
                    autoCompleteTextView_skills.setError("Required Field");
                } else {
                    //Connection Detection
                    if (!con.isConnectingToInternet()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                Skills_Required.this);

                        alert.setTitle("Internet connection unavailable.");
                        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                        alert.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(
                                                Settings.ACTION_WIRELESS_SETTINGS));
                                    }
                                });

                        alert.show();
                    } else {
                        new skillsreq().execute();
                    }
                }
//                Intent i = new Intent(Skills_Required.this, Final_Details.class);
//                ActivityStack.activity.add(Skills_Required.this);
//                startActivity(i);
            }
        });
    }

    //************************************skill_req AsyncTask Class*********************************
    class skill_req extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Skills_Required.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            System.out.println("prefs auth_code" + prefs.getString("auth_code", null));
            String response = Utils.getskills.concat("AuthCode=").concat("90bbcc02131912715d52e4f6b8b7c5c9")
                    .concat("&Skills=");
            Log.i("Skill_set***** ", ":" + response);
            client = new RestClient(response);
            String r = null;
            r = client.executeGet();
            Log.i("RRRRRRR***** ", ":" + r);
            try {
                JSONObject jobj = new JSONObject(r);
                status = jobj.getString("status");
                Log.i("status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = jobj.getJSONArray("data");
                    Log.i("Data", ":" + data);
                    if (data.length() > 0) {
                        arr = new String[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jobj1 = data.getJSONObject(i);
                            String Id = jobj1.getString("Id");
                            String Name = jobj1.getString("Name");
                            System.out.println("Name:::Name::::" + Name);
                            Person item = new Person(Id, Name);
                            userlist.add(item);
                            arr[i] = Name;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
//                adapter = new ContactPickerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, userlist);
//                autoCompleteTextView_skills.setAdapter(adapter);
            } else {
                //dialog("Alert!", error);
            }
        }
    }

    class skillsreq extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(Skills_Required.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Saving Info...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values:", "Skills: " + skills + "AuthCode: " + prefs.getString("auth_code", null)
                    + "JobId: " + prefs.getString("JobId", null));
            String response = ReqSkills(prefs.getString("auth_code", null), prefs.getString("JobId", null), skills);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                Intent i = new Intent(Skills_Required.this, Final_Details.class);
                ActivityStack.activity.add(Skills_Required.this);
                startActivity(i);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("skills", skills_name);
                edit.commit();
            } else {
                dialog("Alert!", "Error in saving data.");
            }
        }
    }

    //***************************************Skill Required Method**********************************
    public String ReqSkills(String auth_code, String JobId, String JobSkills) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("JobId", JobId)
                .appendQueryParameter("JobSkills", JobSkills);
        Log.i("Complete", "Url: " + Utils.skills_req + builder);
        client = new RestClient(Utils.skills_req + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    public void MyDialogSingle(final Context ctx, final String[] charSequences,
                               String title, final EditText select1, final int value,
                               final String string) {

        builder = new android.support.v7.app.AlertDialog.Builder(Skills_Required.this, R.style.DialogTheme);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // mile = -1;
                        selectedItem = charSequences[which];
                        for (int pos = 0; pos < arr.length; pos++) {
                            if (selectedItem.equals(arr[pos])) {
                                position = pos;
                                Person item = new Person(userlist.get(pos).getUserid(), userlist.get(pos).getUsername());
                                //Utils.userlist.add(item);
                                if (count >= 3) {
                                    Utils.hideKeyboard(Skills_Required.this);
                                    Toast.makeText(getApplicationContext(), "You can add upto 3 skills", Toast.LENGTH_SHORT).show();
                                } else {
                                    updateTokenConfirmation();
                                    skills = sb.toString();
                                    // Toast.makeText(getApplicationContext(),"Value"+skills,Toast.LENGTH_SHORT).show();
                                    String[] ids = skills.split(",");
                                    if (ids.length == 0) {
                                        completionView.addObject(item, "");
                                    } else {
                                        for (int i = 0; i < ids.length; i++) {
                                            Log.i("SSSSSSSSSSSSS", "" + userlist.get(pos).getUserid());
                                            Log.i("DDDDDDDDDDDDD", "" + ids[i]);
                                            if (userlist.get(pos).getUserid().equals(ids[i])) {
                                                isDuplicate = true;
                                                break;
                                            } else {
                                                isDuplicate = false;

                                            }
                                        }
                                        if (isDuplicate) {
                                            Toast.makeText(getApplicationContext(), "Skill already exists", Toast.LENGTH_SHORT).show();
                                            Utils.hideKeyboard(Skills_Required.this);
                                        } else {
                                            completionView.addObject(item, "");
                                        }
                                    }
                                }
                            }
                            //select1.setText(selectedItem);
                            dialog.dismiss();
                        }

                        completionView.setTokenListener(Skills_Required.this);
                        completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
                    }
                }
        );

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                }

        );
        builder.show();
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Skills_Required.this), Typeface.BOLD);
        btn_txt.setTypeface(Utils.setfontlight(Skills_Required.this));
        autoCompleteTextView_skills.setTypeface(Utils.setfontlight(Skills_Required.this));
        skillrequired.setTypeface(Utils.setfontlight(Skills_Required.this));
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //****************************************Token Method******************************************

    @Override
    public void onTokenAdded(Object token) {
        updateTokenConfirmation();
        count++;
    }

    private void updateTokenConfirmation() {
        sb = new StringBuilder("");
        sb1 = new StringBuilder("");
        Utils.userlist.clear();
        for (Person token : completionView.getObjects()) {
            sb.append(token.getUserid().toString());
            sb.append(",");
            sb1.append(token.getUsername().toString());
            sb1.append(", ");
            Person item = new Person(token.getUserid().toString(), token.getUsername().toString());
            Utils.userlist.add(item);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb1.deleteCharAt(sb1.lastIndexOf(","));
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        updateTokenConfirmation();
        count--;
        if (count == 0) {
            completionView.setText("");
        }
    }

}

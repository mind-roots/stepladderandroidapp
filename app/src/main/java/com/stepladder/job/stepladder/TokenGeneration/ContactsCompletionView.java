package com.stepladder.job.stepladder.TokenGeneration;



import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;


/**
 * Sample token completion view for basic contact info
 *
 * Created on 9/12/13.
 * @author mgod
 */
public class ContactsCompletionView extends TokenCompleteTextView<Person> {
    //Context ctx;

    public ContactsCompletionView(Context context) {
        super(context);
       // this.ctx = context;
    }

    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public View getViewForObject(Person person) {

        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout)l.inflate(R.layout.contact_token, (ViewGroup) ContactsCompletionView.this.getParent(), false);
        TextView text = (TextView) view.findViewById(R.id.name);
        text.setTypeface(Utils.setfontlight(getContext()));
        text.setText(person.getUsername());

        return view;
    }

    @Override
    protected Person defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        int index = completionText.indexOf('@');
        if (index == -1) {
            return new Person(completionText, completionText.replace(" ", "") + "");
        } else {
            return new Person(completionText.substring(0, index), completionText);
        }
    }
}

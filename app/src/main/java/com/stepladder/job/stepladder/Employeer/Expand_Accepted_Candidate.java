package com.stepladder.job.stepladder.Employeer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Adapters.AcceptedListAdapter;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Expand;
import com.stepladder.job.stepladder.Seeker.See_More_Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

/**
 * Created by user on 11/4/2015.
 */
public class Expand_Accepted_Candidate extends ActionBarActivity {
    RelativeLayout lay_contact_btn,lay_move_btn;
    Menu newmenu;
    LinearLayout ll_past_jobs, ll_past_line;
    TextView title, txt_name, txt_address, title_skills, txt_skills, title_bio, txt_bio, title_past, txt_emp,txt_move;
    ImageView img_confirm_profile;
    LayoutInflater inflater;
    String current_role,status,set_status,user_id;
    SharedPreferences prefs;
    ProgressBar progress;
    Dialog screenDialog;
    ConnectionDetector con;
    RestClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_accepted_candidate);

        init();

        setFont();

        if (Build.VERSION.SDK_INT < 21) {

        } else {
            setupWindowAnimations();
        }

        //Set Data from Adapter
        txt_name.setText(AcceptedListAdapter.profile_list.get(0).name);
        txt_address.setText(AcceptedListAdapter.profile_list.get(0).address);
        txt_bio.setText(AcceptedListAdapter.profile_list.get(0).bio);
        txt_skills.setText(AcceptedListAdapter.profile_list.get(0).skills);

        //Set Profile Image
        if (AcceptedListAdapter.profile_list.get(0).profile_image != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(AcceptedListAdapter.profile_list.get(0).profile_image)
                    .transform(new RoundedTransformation(2))
                    .noFade().placeholder(R.drawable.ic_user_new).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        }

        //Set Past Work
        if (AcceptedListAdapter.profile_list.get(0).past_work.size() > 0) {
            title_past.setVisibility(View.VISIBLE);
            ll_past_line.setVisibility(View.VISIBLE);
            for (int i = 0; i < AcceptedListAdapter.profile_list.get(0).past_work.size(); i++) {
                Log.i("Current Position", "" + AcceptedListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition);
                if (AcceptedListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition.equals("1")) {
                    current_role = "Current Role Since: "+AcceptedListAdapter.profile_list.get(0).past_work.get(i).StartDate;
                    add_view(AcceptedListAdapter.profile_list.get(0).past_work.get(i).JobTitle, AcceptedListAdapter.profile_list.get(0).past_work.get(i).Company
                            , current_role, AcceptedListAdapter.profile_list.get(0).past_work.get(i).City);
                } else if (AcceptedListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition.equals("0")) {
                    current_role = AcceptedListAdapter.profile_list.get(0).past_work.get(i).StartDate + " - " + AcceptedListAdapter.profile_list.get(0).past_work.get(i).EndDate;
                    add_view(AcceptedListAdapter.profile_list.get(0).past_work.get(i).JobTitle, AcceptedListAdapter.profile_list.get(0).past_work.get(i).Company
                            , current_role, AcceptedListAdapter.profile_list.get(0).past_work.get(i).City);
                }
            }
        } else {
            title_past.setVisibility(View.INVISIBLE);
            ll_past_line.setVisibility(View.INVISIBLE);
        }

        clickevents();

    }

    //****************************************UI Initializations************************************
    private void init() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.action_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_address = (TextView) findViewById(R.id.txt_address);
        img_confirm_profile = (ImageView) findViewById(R.id.img_confirm_profile);
        txt_bio = (TextView) findViewById(R.id.txt_bio);
        txt_emp = (TextView) findViewById(R.id.txt_emp);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        title_bio = (TextView) findViewById(R.id.title_bio);
        title_past = (TextView) findViewById(R.id.title_past);
        title_skills = (TextView) findViewById(R.id.title_skills);
        ll_past_jobs = (LinearLayout) findViewById(R.id.ll_past_jobs);
        ll_past_line = (LinearLayout) findViewById(R.id.ll_past_line);
        lay_contact_btn = (RelativeLayout) findViewById(R.id.lay_contact_btn);
        progress = (ProgressBar) findViewById(R.id.progress);
        lay_move_btn = (RelativeLayout)findViewById(R.id.lay_move_btn);
        txt_move = (TextView)findViewById(R.id.txt_move);

        if(Accepted_Candidate.type!=null){
            if(Accepted_Candidate.type.equals("accepted")){
                title.setText("Accepted Candidates");
                lay_contact_btn.setVisibility(View.VISIBLE);
                lay_move_btn.setVisibility(View.GONE);
            }else if(Accepted_Candidate.type.equals("rejected")){
                title.setText("Rejected Candidates");
                lay_contact_btn.setVisibility(View.GONE);
                lay_move_btn.setVisibility(View.VISIBLE);
            }
        }

    }

    //***********************Setting Fonts for UI components******************
    public void setFont() {
        title.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this), Typeface.BOLD);
        txt_name.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_address.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        title_skills.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        title_past.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        title_bio.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_skills.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_emp.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_bio.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_move.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        lay_contact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(AcceptedListAdapter.profile_list.get(0).email, AcceptedListAdapter.profile_list.get(0).phone);
            }
        });

        lay_move_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Job_Status.move = true;
                Job_Status.accept = true;
                finish();
            }
        });

    }

    //***************************************Add View Method****************************************
    public void add_view(String job_title, String company, String current, String location) {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.past_job_info, null);
        final TextView txt_job_title = (TextView) view.findViewById(R.id.txt_job_title);
        final TextView txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);
        final TextView txt_current_role = (TextView) view.findViewById(R.id.txt_current_role);
        final TextView txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_job_title.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_current_role.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_location.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));

        final ModelUIPastJobs UIModel = new ModelUIPastJobs();
        UIModel.txt_job_title = txt_job_title;
        UIModel.txt_company_name = txt_company_name;
        UIModel.txt_current_role = txt_current_role;
        UIModel.txt_location = txt_location;

        UIModel.txt_job_title.setText(job_title);
        UIModel.txt_company_name.setText(company);
        UIModel.txt_current_role.setText(current);
        UIModel.txt_location.setText(location);

        ll_past_jobs.addView(view);

    }

    //******************************************Dialog Method***************************************
    public void ShowDialog(String value1, String value2) {
        // TODO Auto-generated method stub
        screenDialog = new Dialog(Expand_Accepted_Candidate.this, R.style.DialogTheme);
        screenDialog.show();
        screenDialog.setContentView(R.layout.contact_dialog);
        final TextView txt_email,txt_phone,textView_sub,title_email,title_tel;
        ImageView btn_cross;
        textView_sub = (TextView)screenDialog.findViewById(R.id.textView_sub);
        title_email = (TextView)screenDialog.findViewById(R.id.title_email);
        title_tel = (TextView)screenDialog.findViewById(R.id.title_tel);
        txt_email = (TextView)screenDialog.findViewById(R.id.txt_email);
        txt_phone = (TextView)screenDialog.findViewById(R.id.txt_phone);
        btn_cross = (ImageView)screenDialog.findViewById(R.id.btn_cross);

        //Set Fonts
        textView_sub.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this),Typeface.BOLD);
        title_email.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        title_tel.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_email.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));
        txt_phone.setTypeface(Utils.setfontlight(Expand_Accepted_Candidate.this));

        txt_email.setText(value1);
        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {txt_email.getText().toString()};
                emailIntent.putExtra(Intent.EXTRA_EMAIL,recipients);
                emailIntent.setType("text/html");
                startActivity(Intent.createChooser(emailIntent, "Contact us via"));
            }
        });
        txt_phone.setText(value2);
        txt_phone.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(
                        Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"
                        + txt_phone.getText().toString()));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });
        btn_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenDialog.dismiss();
            }
        });
    }

    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }

        return false;
    }

    //****************************************Transition Methods************************************
    @SuppressLint("NewApi")
    private void setupWindowAnimations() {
        Transition transition;
        transition = buildEnterTransition();
        getWindow().setEnterTransition(transition);
    }

    @SuppressLint("NewApi")
    private Transition buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }



    //*************************************Back Press Method****************************************

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

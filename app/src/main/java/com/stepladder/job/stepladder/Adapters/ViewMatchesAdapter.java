package com.stepladder.job.stepladder.Adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.ActivityTransition.TransitionHelper;
import com.stepladder.job.stepladder.Employeer.Confirm_Job_Posting;
import com.stepladder.job.stepladder.Employeer.See_More_Employeer;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelJobContact;
import com.stepladder.job.stepladder.Model.ModelJobDetails;
import com.stepladder.job.stepladder.Model.ModelViewMatches;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Expand_Card;
import com.stepladder.job.stepladder.Seeker_Fragments.ViewMatchesFragment;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

/**
 * Created by user on 10/20/2015.
 */
public class ViewMatchesAdapter extends RecyclerView.Adapter<ViewMatchesAdapter.MHolder> implements Animation.AnimationListener {
    LayoutInflater inflater;
    List<ModelViewMatches> data = Collections.emptyList();
    ArrayList<ModelJobContact> contact_list = new ArrayList<>();
    ArrayList<ModelJobDetails> detail_list = new ArrayList<>();
    ArrayList<String> list_contact = new ArrayList<>();
    ArrayList<String> list_detail = new ArrayList<>();
    Context context;
    String pound = "\u00a3", salary, status;
    SharedPreferences prefs;
    RestClient client;
    public Animation animSlideUp;
    ConnectionDetector con;

    public ViewMatchesAdapter(Context ctx, ArrayList<ModelViewMatches> list) {
        this.context = ctx;
        inflater = LayoutInflater.from(ctx);
        this.data = list;
        prefs = context.getSharedPreferences("user_data", context.MODE_PRIVATE);
        animSlideUp = AnimationUtils.loadAnimation(context,
                R.anim.from_middle);
        con = new ConnectionDetector(context);
    }

    @Override
    public MHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.seeker_cardview, parent, false);
        MHolder holder = new MHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MHolder holder, final int position) {

        final ModelViewMatches viewMatches = data.get(position);
        contact_list = viewMatches.job_Contact;
        detail_list = viewMatches.job_Details;

        //Set Job Filled
        if (detail_list.get(position).job_status.equals("closed")) {
            holder.rl_job_filled.setVisibility(View.VISIBLE);
            holder.ll_job_filled.setVisibility(View.VISIBLE);
            holder.ll_job_active.setVisibility(View.GONE);
        } else {
            holder.rl_job_filled.setVisibility(View.GONE);
            holder.ll_job_filled.setVisibility(View.GONE);
            holder.ll_job_active.setVisibility(View.VISIBLE);
        }

        //Set Contact Card Position
        for (int i = 0; i < list_contact.size(); i++) {
            if (list_contact.get(i).equals(detail_list.get(position).job_id)) {
                holder.ll_card_front.setVisibility(View.GONE);
                holder.ll_card_back_contact.setVisibility(View.VISIBLE);
            }
        }

        //Set Detail Card Position
        for (int i = 0; i < list_detail.size(); i++) {
            if (list_detail.get(i).equals(detail_list.get(position).job_id)) {
                holder.ll_card_front.setVisibility(View.GONE);
                holder.ll_card_back_info.setVisibility(View.VISIBLE);
            }
        }


        holder.txt_company_name.setText(detail_list.get(position).job_title);
        holder.progress.setVisibility(View.VISIBLE);
        Picasso.with(context).load(detail_list.get(position).job_image).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                .into(holder.img_Employer, new Callback() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                        holder.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub
                        holder.progress.setVisibility(View.GONE);
                    }
                });

        //Set Details
        holder.title_info_job.setText(detail_list.get(position).job_title);
        if (detail_list.get(position).min.equals("")) {
            if(detail_list.get(position).job_type.equals("Per Annum")){
                salary = "Not disclosed (Yearly)";
                holder.txt_salary.setText("Not disclosed (Hourly)");
            }else if(detail_list.get(position).job_type.equals("Hourly")){
                salary = "Not disclosed (Hourly)";
                holder.txt_salary.setText("Not disclosed (Hourly)");
            }
        } else {
            if (!detail_list.get(position).max.equals("")) {
                salary = priceformat(detail_list.get(position).min) + " - " + priceformat(detail_list.get(position).max)+" per year";
                holder.txt_salary.setText(priceformat(detail_list.get(position).min) + " - " + priceformat(detail_list.get(position).max)+" per year");
            } else {
                salary = decipriceformat(detail_list.get(position).min)+" per hour";
                holder.txt_salary.setText(decipriceformat(detail_list.get(position).min)+" per hour");
            }
        }
        holder.txt_length.setText(detail_list.get(position).job_length);
        holder.txt_details.setText(detail_list.get(position).job_desc);

        //Set Contact
        holder.title_job.setText(detail_list.get(position).job_title);
        holder.txt_business_name.setText(contact_list.get(position).company_name);
        holder.txt_area.setText(contact_list.get(position).location);
        holder.txt_contact_name.setText(contact_list.get(position).name);
        holder.txt_phone_contact.setText(contact_list.get(position).phone);
        holder.txt_email_contact.setText(contact_list.get(position).email);
        holder.txt_phone_contact.setPaintFlags(holder.txt_phone_contact.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.txt_email_contact.setPaintFlags(holder.txt_email_contact.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        holder.ll_expand_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Expand_Card.class);
                intent.putExtra("title", "Contact Details");
                intent.putExtra("company_name", detail_list.get(position).job_title);
                intent.putExtra("business_name",contact_list.get(position).company_name);
                intent.putExtra("job_image", detail_list.get(position).job_image);
                intent.putExtra("email", contact_list.get(position).email);
                intent.putExtra("salary", holder.txt_salary.getText().toString());
                intent.putExtra("phone", contact_list.get(position).phone);
                intent.putExtra("job_length", detail_list.get(position).job_length);
                intent.putExtra("location", contact_list.get(position).location);
                intent.putExtra("contact", contact_list.get(position).name);
                intent.putExtra("details", detail_list.get(position).job_desc);
                context.startActivity(intent);
            }
        });

        holder.ll_expand_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Expand_Card.class);
                intent.putExtra("title", "Job Details");
                intent.putExtra("company_name", detail_list.get(position).job_title);
                intent.putExtra("business_name", contact_list.get(position).company_name);
                intent.putExtra("job_image", detail_list.get(position).job_image);
                intent.putExtra("email", contact_list.get(position).email);
                intent.putExtra("salary", holder.txt_salary.getText().toString());
                intent.putExtra("phone", contact_list.get(position).phone);
                intent.putExtra("job_length", detail_list.get(position).job_length);
                intent.putExtra("location", contact_list.get(position).location);
                intent.putExtra("contact", contact_list.get(position).name);
                intent.putExtra("details", detail_list.get(position).job_desc);
                context.startActivity(intent);
            }
        });

        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            context);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    context.startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new delete(detail_list.get(position).job_id, position).execute();
                }
            }
        });

        holder.ll_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_contact.add(detail_list.get(position).job_id);
                holder.ll_card_front.setVisibility(View.GONE);
                holder.ll_card_back_contact.startAnimation(animSlideUp);
                holder.ll_card_back_contact.setVisibility(View.VISIBLE);
            }
        });

        holder.ll_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_detail.add(detail_list.get(position).job_id);
                holder.ll_card_front.setVisibility(View.GONE);
                holder.ll_card_back_info.startAnimation(animSlideUp);
                holder.ll_card_back_info.setVisibility(View.VISIBLE);
            }
        });

        holder.ll_back_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list_contact.size(); i++) {
                    if (list_contact.get(i).equals(detail_list.get(position).job_id)) {
                        list_contact.remove(i);
                    }
                }
                holder.ll_card_front.setVisibility(View.VISIBLE);
                holder.ll_card_front.startAnimation(animSlideUp);
                holder.ll_card_back_contact.setVisibility(View.GONE);
            }
        });

        holder.ll_back_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list_detail.size(); i++) {
                    if (list_detail.get(i).equals(detail_list.get(position).job_id)) {
                        list_detail.remove(i);
                    }
                }
                holder.ll_card_front.setVisibility(View.VISIBLE);
                holder.ll_card_front.startAnimation(animSlideUp);
                holder.ll_card_back_info.setVisibility(View.GONE);
            }
        });

        holder.txt_email_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {holder.txt_email_contact.getText().toString()};
                emailIntent.putExtra(Intent.EXTRA_EMAIL,recipients);
                emailIntent.setType("text/html");
                context.startActivity(Intent.createChooser(emailIntent, "Send Mail...").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        holder.txt_phone_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(
                        Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"
                        + holder.txt_phone_contact.getText().toString()));
                if (checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(callIntent);
            }
        });

        holder.ll_delete_filled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            context);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    context.startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    new delete(detail_list.get(position).job_id, position).execute();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    class MHolder extends RecyclerView.ViewHolder {
        public LinearLayout ll_contact, ll_details, ll_delete, ll_card_back_contact, ll_back_contact, ll_card_back_info,
                ll_back_info, ll_expand_contact, ll_expand_info, ll_delete_filled, ll_job_filled, ll_job_active;
        public TextView txt_delete, txt_detail, txt_contact, txt_company_name, txt_job_filled, txt_luck,
                txt_phone_contact, txt_email_contact, title_job, expand, title_title, txt_business_name, title_area, txt_area, title_contact, txt_contact_name,
                title_phone, title_email, title_info_job, txt_expand, txt_contact_filled, txt_detail_filled, txt_delete_filled,
                title_salary, txt_salary, title_length, txt_length, title_desc, txt_details;
        public RelativeLayout ll_card_front, rl_job_filled;
        public ProgressBar progress;
        public ImageView img_Employer;

        public MHolder(View v) {
            super(v);
            ll_contact = (LinearLayout) v.findViewById(R.id.ll_contact);
            ll_details = (LinearLayout) v.findViewById(R.id.ll_details);
            ll_delete = (LinearLayout) v.findViewById(R.id.ll_delete);
            ll_card_back_contact = (LinearLayout) v.findViewById(R.id.ll_card_back_contact);
            ll_card_front = (RelativeLayout) v.findViewById(R.id.ll_card_front);
            ll_back_contact = (LinearLayout) v.findViewById(R.id.ll_back_contact);
            ll_card_back_info = (LinearLayout) v.findViewById(R.id.ll_card_back_info);
            ll_back_info = (LinearLayout) v.findViewById(R.id.ll_back_info);
            ll_expand_contact = (LinearLayout) v.findViewById(R.id.ll_expand_contact);
            ll_expand_info = (LinearLayout) v.findViewById(R.id.ll_expand_info);
            txt_company_name = (TextView) v.findViewById(R.id.txt_company_name);
            txt_delete = (TextView) v.findViewById(R.id.txt_delete);
            txt_detail = (TextView) v.findViewById(R.id.txt_detail);
            txt_contact = (TextView) v.findViewById(R.id.txt_contact);
            txt_job_filled = (TextView) v.findViewById(R.id.txt_job_filled);
            txt_luck = (TextView) v.findViewById(R.id.txt_luck);
            txt_phone_contact = (TextView) v.findViewById(R.id.txt_phone_contact);
            txt_email_contact = (TextView) v.findViewById(R.id.txt_email_contact);
            title_job = (TextView) v.findViewById(R.id.title_job);
            expand = (TextView) v.findViewById(R.id.expand);
            title_title = (TextView) v.findViewById(R.id.title_title);
            txt_business_name = (TextView) v.findViewById(R.id.txt_business_name);
            title_area = (TextView) v.findViewById(R.id.title_area);
            txt_area = (TextView) v.findViewById(R.id.txt_area);
            title_contact = (TextView) v.findViewById(R.id.title_contact);
            title_salary = (TextView) v.findViewById(R.id.title_salary);
            txt_salary = (TextView) v.findViewById(R.id.txt_salary);
            title_length = (TextView) v.findViewById(R.id.title_length);
            txt_length = (TextView) v.findViewById(R.id.txt_length);
            title_desc = (TextView) v.findViewById(R.id.title_desc);
            txt_details = (TextView) v.findViewById(R.id.txt_details);
            txt_contact_name = (TextView) v.findViewById(R.id.txt_contact_name);
            title_info_job = (TextView) v.findViewById(R.id.title_info_job);
            progress = (ProgressBar) v.findViewById(R.id.progress);
            img_Employer = (ImageView) v.findViewById(R.id.img_Employer);
            title_phone = (TextView) v.findViewById(R.id.title_phone);
            title_email = (TextView) v.findViewById(R.id.title_email);
            txt_expand = (TextView) v.findViewById(R.id.txt_expand);
            txt_contact_filled = (TextView) v.findViewById(R.id.txt_contact_filled);
            txt_detail_filled = (TextView) v.findViewById(R.id.txt_detail_filled);
            txt_delete_filled = (TextView) v.findViewById(R.id.txt_delete_filled);
            ll_delete_filled = (LinearLayout) v.findViewById(R.id.ll_delete_filled);
            ll_job_filled = (LinearLayout) v.findViewById(R.id.ll_job_filled);
            ll_job_active = (LinearLayout) v.findViewById(R.id.ll_job_active);
            rl_job_filled = (RelativeLayout) v.findViewById(R.id.rl_job_filled);

            //Set Fonts
            txt_business_name.setTypeface(Utils.setfontlight(context));
            txt_contact_name.setTypeface(Utils.setfontlight(context));
            title_info_job.setTypeface(Utils.setfontlight(context));
            title_job.setTypeface(Utils.setfontlight(context));
            title_title.setTypeface(Utils.setfontlight(context));
            txt_company_name.setTypeface(Utils.setfontlight(context));
            txt_contact.setTypeface(Utils.setfontlight(context));
            txt_delete.setTypeface(Utils.setfontlight(context));
            txt_detail.setTypeface(Utils.setfontlight(context));
            txt_job_filled.setTypeface(Utils.setfontlight(context));
            txt_luck.setTypeface(Utils.setfontlight(context));
            title_area.setTypeface(Utils.setfontlight(context));
            txt_area.setTypeface(Utils.setfontlight(context));
            title_contact.setTypeface(Utils.setfontlight(context));
            title_salary.setTypeface(Utils.setfontlight(context));
            txt_salary.setTypeface(Utils.setfontlight(context));
            title_length.setTypeface(Utils.setfontlight(context));
            txt_length.setTypeface(Utils.setfontlight(context));
            title_desc.setTypeface(Utils.setfontlight(context));
            txt_details.setTypeface(Utils.setfontlight(context));
            expand.setTypeface(Utils.setfontlight(context));
            title_phone.setTypeface(Utils.setfontlight(context));
            title_email.setTypeface(Utils.setfontlight(context));
            txt_expand.setTypeface(Utils.setfontlight(context));
            txt_email_contact.setTypeface(Utils.setfontlight(context));
            txt_phone_contact.setTypeface(Utils.setfontlight(context));
            txt_contact_filled.setTypeface(Utils.setfontlight(context));
            txt_detail_filled.setTypeface(Utils.setfontlight(context));
            txt_delete_filled.setTypeface(Utils.setfontlight(context));
        }

    }

    //***************************************Async Task Class***************************************

    class delete extends AsyncTask<Void, Void, Void> {
        String jobId;
        int pos;

        public delete(String job_id, int position) {
            this.jobId = job_id;
            this.pos = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ViewMatchesFragment.progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = DeleteMatches(prefs.getString("auth_code", null), jobId);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ViewMatchesFragment.progress.setVisibility(View.GONE);
            if (status.equals("true")) {
                ViewMatchesFragment.matches_list.remove(pos);
                ViewMatchesFragment.matches_list.clear();
                detail_list.clear();
                contact_list.clear();
                new view_matches().execute();
            } else {
                dialog("Alert!", "Error in deleting");
            }

        }
    }

    //*************************************Delete Matched Method************************************
    public String DeleteMatches(String auth_code, String job_id) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("JobId", job_id);
        Log.i("Complete", "Url: " + Utils.delete_matches + builder);
        client = new RestClient(Utils.delete_matches + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //**************************************Async Task Class****************************************

    class view_matches extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(context);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Loading....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = ViewMatches(prefs.getString("auth_code", null));
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = obj.optJSONArray("data");
                    if (data.length() != 0) {
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jobj = data.getJSONObject(i);

                            //Job Details
                            JSONObject job_details = jobj.getJSONObject("JobDetail");
                            ModelJobDetails details = new ModelJobDetails();
                            details.job_id = job_details.getString("JobId");
                            details.job_title = job_details.getString("JobTitle");
                            details.job_desc = job_details.getString("JobDescription");
                            details.job_length = job_details.getString("JobDuration");
                            details.skills = job_details.getString("JobSkills");
                            details.job_type = job_details.getString("Typed");
                            details.min = job_details.getString("MinAmount");
                            details.max = job_details.getString("MaxAmount");
                            details.job_status = job_details.getString("Status");
                            details.job_image = job_details.getString("Image");
                            detail_list.add(details);
                            Log.i("Details List: ", "" + detail_list.size());

                            //Contact Details
                            JSONObject contact_details = jobj.getJSONObject("ContactDetail");
                            ModelJobContact job_contact = new ModelJobContact();
                            job_contact.company_name = contact_details.getString("CompanyName");
                            String fname = contact_details.getString("FirstName");
                            String lname = contact_details.getString("LastName");
                            job_contact.name = fname + " " + lname;
                            job_contact.phone = contact_details.getString("Phone_number");
                            job_contact.email = contact_details.getString("Email");
                            job_contact.bio = contact_details.getString("Biography");
                            job_contact.sector = contact_details.getString("Sector");
                            job_contact.location = contact_details.getString("LocationName");
                            contact_list.add(job_contact);
                            Log.i("Contact List: ", "" + contact_list.size());

                            //Complete Matches List
                            ModelViewMatches matches = new ModelViewMatches();
                            matches.job_Contact = contact_list;
                            matches.job_Details = detail_list;
                            ViewMatchesFragment.matches_list.add(matches);
                            Log.i("Matches List: ", "" + ViewMatchesFragment.matches_list.size());

                        }
                    }

                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                notifyDataSetChanged();
                if (ViewMatchesFragment.matches_list.size() == 0) {
                    ViewMatchesFragment.viewmatches_recycler_view.setVisibility(View.GONE);
                    ViewMatchesFragment.no_matches.setVisibility(View.VISIBLE);
                }else {
                    ViewMatchesFragment.no_matches.setVisibility(View.GONE);
                    ViewMatchesFragment.ll_recycler.setVisibility(View.VISIBLE);
                }
            } else {
                ViewMatchesFragment.no_matches.setVisibility(View.VISIBLE);
                ViewMatchesFragment.ll_recycler.setVisibility(View.GONE);
            }
        }
    }

    //***********************************Update Method**********************************************
    public String ViewMatches(String auth_code) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code);
        Log.i("Complete", "Url: " + Utils.view_matches + builder);
        client = new RestClient(Utils.view_matches + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    public String decipriceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            //amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }
}

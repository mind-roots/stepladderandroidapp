package com.stepladder.job.stepladder.Employeer_Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.stepladder.job.stepladder.Adapters.EmployeerCardAdapter;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelGetJobs;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DashboardEmployeerFragment extends Fragment {
    View rootView;
    RecyclerView my_recycler_view;
    TextView textView1, textView2, textView3, btn_txt;
    public static LinearLayout ll_post_job, ll_no_jobs;
    public static ProgressBar progressBar;
    RestClient client;
    SharedPreferences prefs;
    ConnectionDetector con;
    String status;
    ArrayList<ModelGetJobs> job_list = new ArrayList<>();
    EmployeerCardAdapter employeerCardAdapter;
    public static RecyclerView current_jobs;
    Menu newmenu;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.dashboard_employeer_fragment, container, false);

        init();

        setFonts();

        DashBoard_Employeer.ic_add_job.setVisibility(View.VISIBLE);

        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    getActivity());

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {

            job_list.clear();
            new get_jobs().execute();
        }

        return rootView;
    }

    //********************************************UI Initialization*********************************
    private void init() {
        // TODO Auto-generated method stub

        my_recycler_view = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        textView1 = (TextView) rootView.findViewById(R.id.textView1);
        textView2 = (TextView) rootView.findViewById(R.id.textView2);
        textView3 = (TextView) rootView.findViewById(R.id.textView3);
        ll_post_job = (LinearLayout) rootView.findViewById(R.id.ll_post_job);
        btn_txt = (TextView) rootView.findViewById(R.id.btn_txt);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        con = new ConnectionDetector(getActivity());
        current_jobs = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        ll_no_jobs = (LinearLayout) rootView.findViewById(R.id.ll_no_jobs);

        Log.i("User Name: ",""+prefs.getString("first_name",null)+prefs.getString("last_name",null));
        textView1.setText("Welcome back, "+prefs.getString("first_name",null)+" "+prefs.getString("last_name",null)+"!");
    }

    //********************************************Set Fonts*****************************************
    public void setFonts() {
        textView1.setTypeface(Utils.setfontlight(getActivity()));
        textView2.setTypeface(Utils.setfontlight(getActivity()));
        textView3.setTypeface(Utils.setfontlight(getActivity()));
        btn_txt.setTypeface(Utils.setfontlight(getActivity()));
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.newmenu = menu;
        inflater.inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(false);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        newmenu.findItem(R.id.action_done).setVisible(false);
        newmenu.findItem(R.id.action_update).setVisible(false);
    }

    //*******************************************Async Task Class***********************************
    class get_jobs extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = GetJobs(prefs.getString("auth_code", null), Utils.get_current_jobs);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");

                if (status.equalsIgnoreCase("true")) {
                    JSONArray data = obj.getJSONArray("data");
                    Log.i("Data Array: ", "" + data);
                    for (int i = 0; i < data.length(); i++) {

                        JSONObject jobj = data.getJSONObject(i);
                        String job_id = jobj.getString("JobId");
                        String job_title = jobj.getString("JobTitle");
                        String Status = jobj.getString("Status");
                        String leads = jobj.getString("Leads");
                        String company_image = jobj.getString("Image");
                        JSONObject company_info = jobj.getJSONObject("CompanyInfo");
                        String company_name = company_info.getString("CompanyName");

                        //Add Data into Model
                        ModelGetJobs model_jobs = new ModelGetJobs();
                        model_jobs.job_id = job_id;
                        model_jobs.job_title = job_title;
                        model_jobs.job_status = Status;
                        model_jobs.leads = leads;
                        model_jobs.company_image = company_image;
                        model_jobs.company_name = company_name;

                        job_list.add(model_jobs);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equalsIgnoreCase("true")) {
                current_jobs.setVisibility(View.VISIBLE);
                ll_no_jobs.setVisibility(View.GONE);
                employeerCardAdapter = new EmployeerCardAdapter(getActivity(), job_list);
                current_jobs.setAdapter(employeerCardAdapter);
                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                current_jobs.setLayoutManager(llm);
            } else {
                current_jobs.setVisibility(View.GONE);
                ll_no_jobs.setVisibility(View.VISIBLE);
            }
        }
    }

    // ****************************************Get Jobs Method***********************************
    private String GetJobs(String auth_code, String url) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("AuthCode",
                auth_code);
        client = new RestClient(url + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }
}

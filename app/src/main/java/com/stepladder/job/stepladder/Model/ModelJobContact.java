package com.stepladder.job.stepladder.Model;

/**
 * Created by user on 12/18/2015.
 */
public class ModelJobContact {
    public String company_name;
    public String name;
    public String phone;
    public String email;
    public String bio;
    public String sector;
    public String location;
}

package com.stepladder.job.stepladder.Employeer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.ActivityTransition.TransitionHelper;
import com.stepladder.job.stepladder.Adapters.PendingListAdapter;
import com.stepladder.job.stepladder.Adapters.ProfileAdapter;
import com.stepladder.job.stepladder.Adapters.SwipeAdapter;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Expand;
import com.stepladder.job.stepladder.Seeker.See_More_Seeker;
import com.stepladder.job.stepladder.Swipe.FlingCardListener;
import com.stepladder.job.stepladder.Swipe.SwipeFlingAdapterView;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 11/19/2015.
 */
public class Pending_Candidates extends ActionBarActivity implements FlingCardListener.ActionDownInterface {
    //  View rootView;
    LayoutInflater inflater;
    Menu newmenu;
    LinearLayout ll_past_jobs, ll_past_line;
    TextView title;
    private SwipeFlingAdapterView flingContainer;
    ProfileAdapter adapter;
    ImageView left, right, expand_activities_button;
    ProgressBar progressBar;
    String status, set_status, user_id;
    SharedPreferences prefs;
    RestClient client;
    public static boolean accept = false, reject = false;
    ConnectionDetector con;
    Tracker t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_seeker_fragment);

        init();

        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
        Log.i("User id: ", "" + user_id);

        adapter = new ProfileAdapter(getApplicationContext(), PendingListAdapter.profile_list);
        flingContainer.setAdapter(adapter);

        clickevents();
    }


    //*******************************************clickevents****************************************
    private void clickevents() {
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectLeft();

            }
        });

        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    left.setBackgroundResource(R.drawable.ic_cross_red);
                }

                return false;
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectRight();

            }
        });

        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    right.setBackgroundResource(R.drawable.ic_tick_green);
                }
                return false;
            }
        });

        expand_activities_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Expand_Pending_Candidate.class);
                in.putExtra("name", PendingListAdapter.profile_list.get(0).name);
                in.putExtra("address", PendingListAdapter.profile_list.get(0).address);
                in.putExtra("bio", PendingListAdapter.profile_list.get(0).bio);
                in.putExtra("skills", PendingListAdapter.profile_list.get(0).skills);
                in.putExtra("profile_image", PendingListAdapter.profile_list.get(0).profile_image);
                transitionTo(in);
            }
        });

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            Pending_Candidates.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {

                    //Track event on google analytics
                    // Build and send an Event.
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("Company")
                            .setAction("Swipe Candidate Rejected")
                            .setLabel("Swipe Candidate Rejected")
                            .build());

                    Job_Status.reject = true;
                    PendingListAdapter.profile_list.remove(0);
                    set_status = "rejected";
                    new SetStatus().execute();
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                    adapter.notifyDataSetChanged();
                    finish();
                }
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                if (!con.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            Pending_Candidates.this);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {

                    //Track event on google analytics
                    // Build and send an Event.
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("Company")
                            .setAction("Swipe Candidate Accepted")
                            .setLabel("Swipe Candidate Accepted")
                            .build());

                    Job_Status.accept = true;
                    PendingListAdapter.profile_list.remove(0);
                    set_status = "accepted";
                    new SetStatus().execute();
                    adapter.notifyDataSetChanged();
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                    finish();
                }
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                if (scrollProgressPercent < 0) {
                    left.setBackgroundResource(R.drawable.ic_cross_red);
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                } else {
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                }
                if (scrollProgressPercent > 0) {
                    right.setBackgroundResource(R.drawable.ic_tick_green);
                    left.setBackgroundResource(R.drawable.ic_cross_grey);
                } else {
                    right.setBackgroundResource(R.drawable.ic_tick_grey);
                }
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);

                adapter.notifyDataSetChanged();
            }
        });

    }


    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        t = ((OneSignalClass) getApplication()).getDefaultTracker();
        con = new ConnectionDetector(Pending_Candidates.this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Potential Candidates");
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        title.setTypeface(Utils.setfontlight(Pending_Candidates.this), Typeface.BOLD);
        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
        left = (ImageView) findViewById(R.id.left);
        right = (ImageView) findViewById(R.id.right);
        expand_activities_button = (ImageView) findViewById(R.id.expand_activities_button);
        ll_past_jobs = (LinearLayout) findViewById(R.id.ll_past_jobs);
        ll_past_line = (LinearLayout) findViewById(R.id.ll_past_line);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    //*************************************Async Task Class*****************************************
    class SetStatus extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String response = JobStatus(prefs.getString("auth_code", null), user_id, Job_Status.job_id, set_status);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (status.equalsIgnoreCase("true")) {
            } else {

            }
        }
    }

    //***************************************Location Method****************************************
    public String JobStatus(String auth_code, String UserId, String JobId, String Creator_Status) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("UserId", UserId)
                .appendQueryParameter("JobId", JobId)
                .appendQueryParameter("Creator_Status", Creator_Status);
        Log.i("Complete", "Url: " + Utils.select_candidate + builder);
        client = new RestClient(Utils.select_candidate + builder);
        String response = null;
        response = client.executePost();
        return response;
    }


    //***********************************onCreateOptionsMenu****************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Handler handler = new Handler();

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    public void run() {
                        if (accept) {
                            flingContainer.getTopCardListener().selectRight();
                            accept = false;
                        } else if (reject) {
                            flingContainer.getTopCardListener().selectLeft();
                            reject = false;
                        }
                    }
                });
            }
        }).start();
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onActionDownPerform() {

    }

    //***************************************transitionTo Method************************************
    @SuppressLint("NewApi")
    protected void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }


}

package com.stepladder.job.stepladder;

import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.onesignal.OneSignal;

import org.json.JSONObject;


/**
 * Created by admin on 2/26/2016.
 */
public class OneSignalClass extends MultiDexApplication {
    public static String additionalMessage = "";
    JSONObject obj;
    public static String type = "home";
    public static boolean isOpen;
    private Tracker mTracker;

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.analytics);
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
//            //Debug app instabug
//            new Instabug.Builder(this, "1d53228cad7b3f0fc75935e94f461f22")
//                    .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
//                    .setShouldShowIntroDialog(false)
//                    .build();
            //Live app instabug
            new Instabug.Builder(this, "4dd0434b44efac9aebdccc8db948b95e")
                    .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                    .setShouldShowIntroDialog(false)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            OneSignal.startInit(this)
                    .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                    .setAutoPromptLocation(true)
                    .init();
            OneSignal.enableNotificationsWhenActive(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ContentValues cv = new ContentValues();
            cv.put("badgecount", 0);
            getContentResolver().update(Uri.parse("content://com.sec.badge/apps"), cv, "package=?", new String[]{getPackageName()});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        /**
         * Callback to implement in your app to handle when a notification is opened from the Android status bar or
         * a new one comes in while the app is running.
         * This method is located in this Application class as an example, you may have any class you wish implement NotificationOpenedHandler and define this method.
         *
         * @param message        The message string the user seen/should see in the Android status bar.
         * @param additionalData The additionalData key value pair section you entered in on onesignal.com.
         * @param isActive       Was the app in the foreground when the notification was received.
         */

        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {

            try {
                if (additionalData != null) {
                    if (additionalData.has("actionSelected"))
                        additionalMessage += "Pressed ButtonID: " + additionalData.getString("actionSelected");
                    isOpen = isActive;
                    additionalMessage = message + "\nFull additionalData:\n" + additionalData.toString();
                    obj = new JSONObject(additionalData.toString());
                    type = obj.optString("type");
                    System.out.println("message" + message);
                    System.out.println("one signal data" + obj);
                    SplashScreen.push = true;
                    System.out.println("values in one signal" + type);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }


        }


    }
}
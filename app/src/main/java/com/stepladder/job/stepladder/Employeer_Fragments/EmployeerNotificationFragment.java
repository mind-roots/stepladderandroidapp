package com.stepladder.job.stepladder.Employeer_Fragments;


import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Location;
import com.stepladder.job.stepladder.Utils.Utils;

public class EmployeerNotificationFragment extends Fragment {
    View rootView;
    TextView txt_appnoti, txt_emailnoti, txt_matches;
    SharedPreferences prefs;
    public static String email_noti = "yes",app_noti = "yes";
    Switch switch_email, switch_app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.employeer_notification_fragment, container, false);

        init();

        setFonts();

        DashBoard_Employeer.newmenu.findItem(R.id.action_update).setVisible(true);


        //Get Prefs Values
        if (prefs.getString("email_noti", null) != null) {
            if (prefs.getString("email_noti", null).equals("yes")) {
                switch_email.setChecked(true);
            } else if (prefs.getString("email_noti", null).equals("no")) {
                switch_email.setChecked(false);
            }
        }
        if (prefs.getString("app_noti", null) != null) {
            if (prefs.getString("app_noti", null).equals("yes")) {
                switch_app.setChecked(true);
            } else if (prefs.getString("app_noti", null).equals("no")) {
                switch_app.setChecked(false);
            }
        }

        clickevents();

        DashBoard_Employeer.ic_add_job.setVisibility(View.GONE);

        return rootView;
    }


    //********************************************UI Initialization*********************************
    private void init() {
        txt_appnoti = (TextView) rootView.findViewById(R.id.txt_appnoti);
        txt_emailnoti = (TextView) rootView.findViewById(R.id.txt_emailnoti);
        txt_matches = (TextView) rootView.findViewById(R.id.txt_matches);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        switch_email = (Switch) rootView.findViewById(R.id.switch_email);
        switch_app = (Switch) rootView.findViewById(R.id.switch_app);
    }

    //********************************************Set Fonts*****************************************
    private void setFonts() {
        txt_appnoti.setTypeface(Utils.setfontlight(getActivity()));
        txt_emailnoti.setTypeface(Utils.setfontlight(getActivity()));
        txt_matches.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
    }

    //********************************************Clickevents***************************************
    private void clickevents() {
        switch_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    email_noti = "yes";
                }else {
                    email_noti = "no";
                }
            }
        });

        switch_app.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    app_noti = "yes";
                }else {
                    app_noti = "no";
                }
            }
        });

    }


}

package com.stepladder.job.stepladder.Seeker_Fragments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stepladder.job.stepladder.Employeer.Job_Details;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.Person;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Confirm_Profile;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Location;
import com.stepladder.job.stepladder.Seeker.Past_Jobs;
import com.stepladder.job.stepladder.Seeker.Profile_photo;
import com.stepladder.job.stepladder.Seeker.Skill_set;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class EditEmployeeProfileFragment extends Fragment {
	View rootView;
	TextView txt_view,txt_skills,txt_job_detail,txt_job,txt_loc,txt_edit,txt_profile;
    LinearLayout ll_edit_photo,ll_location,ll_job_details,ll_skillset,ll_view_profile;
    String status;
    RestClient client;
    SharedPreferences prefs;
    ProgressBar progress;
    StringBuilder skills;
    ConnectionDetector con;
    private Tracker mTracker;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.edit_employee_profile_fragment, container, false);

        init();

        setFonts();

        DashBoard_Seeker.isHome = false;
        DashBoard_Seeker.open = true;
        DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
        DashBoard_Seeker.ll_badge.setVisibility(View.GONE);
        //DashBoard_Seeker.newmenu.findItem(R.id.action_update).setVisible(false);


        if (!con.isConnectingToInternet()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    getActivity());

            alert.setTitle("Internet connection unavailable.");
            alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
            alert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    });

            alert.show();
        } else {
            //Get Past Jobs
            Utils.final_ui_data.clear();
            new get_past_jobs().execute();

        }

        clickevents();

        return rootView;
	}

    private void clickevents() {
        ll_edit_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.pimageUri = null;
                Intent intent = new Intent(getActivity(), Profile_photo.class);
                intent.putExtra("edit","true");
                startActivity(intent);
            }
        });

        ll_job_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get Past Jobs
//                Utils.final_ui_data.clear();
//                new get_past_jobs().execute();

                Intent intent = new Intent(getActivity(), Past_Jobs.class);
                intent.putExtra("edit", "true");
                startActivity(intent);
            }
        });

        ll_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Location.class);
                intent.putExtra("edit", "true");
                startActivity(intent);
            }
        });

        ll_skillset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get Skills
//                Utils.userlist.clear();
//                new get_skills().execute();

                Intent intent = new Intent(getActivity(), Skill_set.class);
                intent.putExtra("edit","true");
                startActivity(intent);
            }
        });

        ll_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.pimageUri = null;
                Intent intent = new Intent(getActivity(), Confirm_Profile.class);
                intent.putExtra("edit", "true");
                startActivity(intent);
            }
        });
    }

    //********************************************UI Initialization*********************************
    private void init() {
        //-------------------------Google Analytics initialization--------------------
        mTracker = ((OneSignalClass) getActivity().getApplication()).getDefaultTracker();
        con = new ConnectionDetector(getActivity());
        txt_edit = (TextView)rootView.findViewById(R.id.txt_edit);
        txt_view = (TextView)rootView.findViewById(R.id.txt_view);
        txt_skills = (TextView)rootView.findViewById(R.id.txt_skills);
        txt_job_detail = (TextView)rootView.findViewById(R.id.txt_job_detail);
        txt_job = (TextView)rootView.findViewById(R.id.txt_job);
        txt_loc = (TextView)rootView.findViewById(R.id.txt_loc);
        txt_profile = (TextView)rootView.findViewById(R.id.txt_profile);
        ll_edit_photo = (LinearLayout)rootView.findViewById(R.id.ll_edit_photo);
        ll_location = (LinearLayout)rootView.findViewById(R.id.ll_location);
        ll_job_details = (LinearLayout)rootView.findViewById(R.id.ll_job_details);
        ll_skillset = (LinearLayout)rootView.findViewById(R.id.ll_skillset);
        ll_view_profile = (LinearLayout)rootView.findViewById(R.id.ll_view_profile);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        progress = (ProgressBar)rootView.findViewById(R.id.progress);
    }
    //********************************************Set Fonts*****************************************
    public void setFonts() {
        txt_edit.setTypeface(Utils.setfontlight(getActivity()));
        txt_job.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_job_detail.setTypeface(Utils.setfontlight(getActivity()));
        txt_loc.setTypeface(Utils.setfontlight(getActivity()));
        txt_profile.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_skills.setTypeface(Utils.setfontlight(getActivity()));
        txt_view.setTypeface(Utils.setfontlight(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();
        mTracker.setScreenName("Edit Employee Profile");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onResume() {
        super.onResume();
        DashBoard_Seeker.open = true;
    }

    class get_past_jobs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("prefs auth_code" + prefs.getString("auth_code", null));
            String response = Utils.get_past_jobs.concat("AuthCode=").concat(prefs.getString("auth_code", null));
            Log.i("Past Jobs ", ":" + response);
            client = new RestClient(response);
            String r = null;
            r = client.executeGet();
            try {
                JSONObject obj = new JSONObject(r);
                status = obj.getString("status");
                if (status.equals("true")) {
                    JSONObject data = obj.getJSONObject("data");
                    //Get Past Work
                    JSONArray Userspastwork = data.optJSONArray("Userspastwork");
                    for (int i = 0; i < Userspastwork.length(); i++) {
                        JSONObject jobj = Userspastwork.getJSONObject(i);
                        ModelUIData UIdata = new ModelUIData();
                        UIdata.JobTitle = jobj.getString("JobTitle");
                        UIdata.Company = jobj.getString("Company");
                        UIdata.City = jobj.getString("City");
                        UIdata.CurrentPosition = jobj.getString("CurrentPosition");
                        UIdata.StartDate = jobj.getString("StartDate");
                        UIdata.EndDate = jobj.optString("EndDate");

                        Utils.final_ui_data.add(UIdata);
                        Log.i("Size: ", "" + Utils.final_ui_data.size());
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!con.isConnectingToInternet()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        getActivity());

                alert.setTitle("Internet connection unavailable.");
                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                alert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                alert.show();
            } else {
                Utils.userlist.clear();
                new get_skills().execute();
            }
        }
    }

    class get_skills extends AsyncTask<Void, Void, Void> {
        Person item;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("prefs auth_code" + prefs.getString("auth_code", null));
            String response = Utils.get_skills.concat("AuthCode=").concat(prefs.getString("auth_code", null));
            Log.i("Skill_set ", ":" + response);
            client = new RestClient(response);
            String r = null;
            r = client.executeGet();
            try {
                JSONObject jobj = new JSONObject(r);
                status = jobj.getString("status");
                Log.i("status", ":" + status);
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = jobj.getJSONObject("data");
                    JSONArray skills = data.getJSONArray("Skills");
                    Log.i("Data", ":" + data);
                    for (int i = 0; i < skills.length(); i++) {
                        JSONObject jobj1 = skills.getJSONObject(i);
                        String Id = jobj1.getString("Id");
                        String Name = jobj1.getString("Name");
                        System.out.println("Name:::Name::::" + Name);
                        item = new Person(Id, Name);
                        Utils.userlist.add(item);
                        Log.i("Skills List: ", "" + Utils.userlist.size());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.setVisibility(View.GONE);
            if (status.equals("true")) {
                skills = new StringBuilder();
                for(int i=0;i<Utils.userlist.size();i++){
                    skills.append(Utils.userlist.get(i).getUsername());
                    skills.append(", ");
                }
                if (skills.length()>0){
                    skills.deleteCharAt(skills.lastIndexOf(","));
                }
//                Intent intent = new Intent(getActivity(), Skill_set.class);
//                intent.putExtra("edit","true");
//                startActivity(intent);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("skills", skills.toString());
                editor.commit();
            }
        }
    }

}

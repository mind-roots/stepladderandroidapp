package com.stepladder.job.stepladder.Adapters;

/**
 * Created by user on 10/27/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stepladder.job.stepladder.Model.ModelJobsCard;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker_Fragments.DashboardSeekerFragment;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by abc on 10/27/2015.
 */

public class SwipeAdapter extends BaseAdapter {
    ArrayList<ModelJobsCard> get_jobs;
    Context ctx;
    ViewHolder holder;
    String job_id, set_status;
    SharedPreferences prefs;
    RestClient client;
    int listsize;

    public SwipeAdapter(Context context, ArrayList<ModelJobsCard> list) {
        this.get_jobs = list;
        this.ctx = context;
        prefs = context.getSharedPreferences("user_data", context.MODE_PRIVATE);
        Log.i("List Size adapter: ", "" + get_jobs.size());
    }

    @Override
    public int getCount() {
        return get_jobs.size();
    }

    @Override
    public Object getItem(int position) {
        return get_jobs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.swipe_view, parent, false);
            holder = new ViewHolder();
            holder.img_Employer = (ImageView) convertView.findViewById(R.id.img_Employer);
            holder.txt_company_name = (TextView) convertView.findViewById(R.id.txt_company_name);
            holder.title_title = (TextView) convertView.findViewById(R.id.title_title);
            holder.txt_job_title = (TextView) convertView.findViewById(R.id.txt_job_title);
            holder.title_salary = (TextView) convertView.findViewById(R.id.title_salary);
            holder.txt_salary = (TextView) convertView.findViewById(R.id.txt_salary);
            holder.title_length = (TextView) convertView.findViewById(R.id.title_length);
            holder.txt_length = (TextView) convertView.findViewById(R.id.txt_length);
            holder.title_area = (TextView) convertView.findViewById(R.id.title_area);
            holder.txt_area = (TextView) convertView.findViewById(R.id.txt_area);
            holder.title_skills = (TextView) convertView.findViewById(R.id.title_skills);
            holder.txt_skills = (TextView) convertView.findViewById(R.id.txt_skills);
            holder.title_desc = (TextView) convertView.findViewById(R.id.title_desc);
            holder.txt_desc = (TextView) convertView.findViewById(R.id.txt_desc);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            setFont();

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Log.i("Pos: ", "" + position);
        final ModelJobsCard jobsCard = get_jobs.get(position);

        holder.txt_company_name.setText(jobsCard.company_name);
        holder.txt_job_title.setText(jobsCard.job_title);
        if (jobsCard.minamount.equals("")) {
            if (jobsCard.job_type.equals("Per Annum")) {
                holder.txt_salary.setText("Not disclosed (Yearly)");
            } else if (jobsCard.job_type.equals("Hourly")) {
                holder.txt_salary.setText("Not disclosed (Hourly)");
            }
        } else {
            if (jobsCard.maxamount.equals("")) {
                holder.txt_salary.setText(decipriceformat(jobsCard.minamount) + " per hour");
            } else {
                holder.txt_salary.setText(priceformat(jobsCard.minamount) + " - " + priceformat(jobsCard.maxamount) + " per year");
            }
        }
        holder.txt_length.setText(jobsCard.job_length);
        holder.txt_area.setText(jobsCard.location);
        holder.txt_skills.setText(jobsCard.skills);
        holder.txt_desc.setText(jobsCard.job_desc);

        Log.i("Company Name: ", "" + jobsCard.company_name);
        Log.i("Image: ", "" + jobsCard.job_image);

        if (DashboardSeekerFragment.width == 540 || DashboardSeekerFragment.height == 960) {
            if (jobsCard.job_image.contains("default-company")) {

            } else {
                holder.img_Employer.setBackground(new BitmapDrawable(ctx.getResources(), jobsCard.image));
                //jobsCard.image = null;
            }
        } else {
            if (jobsCard.job_image.contains("default-company")) {

            } else {
                Glide.with(ctx)
                        .load(jobsCard.job_image)
                        .centerCrop()
                        .placeholder(R.drawable.ic_company)
                        .crossFade()
                        .into(holder.img_Employer);
            }
        }

        return convertView;
    }

    public class ViewHolder {
        public ImageView img_Employer;
        public ProgressBar progressBar;
        public TextView txt_company_name, title_title, txt_job_title, title_salary, txt_salary, title_length, txt_length, title_area, txt_area, title_skills, txt_skills, title_desc, txt_desc;
    }

    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        holder.title_title.setTypeface(Utils.setfontlight(ctx));
        holder.txt_job_title.setTypeface(Utils.setfontlight(ctx));
        holder.title_area.setTypeface(Utils.setfontlight(ctx));
        holder.txt_area.setTypeface(Utils.setfontlight(ctx));
        holder.title_desc.setTypeface(Utils.setfontlight(ctx));
        holder.title_skills.setTypeface(Utils.setfontlight(ctx));
        holder.title_length.setTypeface(Utils.setfontlight(ctx));
        holder.title_salary.setTypeface(Utils.setfontlight(ctx));
        holder.txt_desc.setTypeface(Utils.setfontlight(ctx));
        holder.txt_company_name.setTypeface(Utils.setfontlight(ctx));
        holder.txt_length.setTypeface(Utils.setfontlight(ctx));
        holder.txt_salary.setTypeface(Utils.setfontlight(ctx));
        holder.txt_skills.setTypeface(Utils.setfontlight(ctx));
    }

    //*************************************Async Task Class*****************************************
    class SetStatus extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String response = JobStatus(prefs.getString("auth_code", null), job_id, set_status);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    //***************************************Location Method****************************************
    public String JobStatus(String auth_code, String JobId, String Applier_Status) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("JobId", JobId)
                .appendQueryParameter("Applier_Status", Applier_Status);
        Log.i("Complete", "Url: " + Utils.apply_job + builder);
        client = new RestClient(Utils.apply_job + builder);
        String response = null;
        response = client.executePost();
        return response;
    }

    //*************************************Price Formatting n De-formatting*************************
    public String priceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

    public String decipriceformat(String mprice) {
        // TODO Auto-generated method stub
        String amount;
        try {
            double value = Double.parseDouble(mprice);
            Double price = Double.valueOf(value);
            NumberFormat currencyFormatter = NumberFormat
                    .getCurrencyInstance(Locale.UK);
            amount = currencyFormatter.format(price);
            //amount = amount.split("\\.")[0];
        } catch (Exception e) {
            amount = mprice;
        }
        return amount;
    }

}
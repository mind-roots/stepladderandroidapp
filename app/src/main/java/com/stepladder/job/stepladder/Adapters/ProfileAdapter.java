package com.stepladder.job.stepladder.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stepladder.job.stepladder.Employeer.Expand_Pending_Candidate;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.ModelUserProfile;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.See_More_Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abc on 11/23/2015.
 */
public class ProfileAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<ModelUserProfile> layList;
    Context ctx;
    String current_role;
    ViewHolder holder;

    public ProfileAdapter(Context context, ArrayList<ModelUserProfile> layList) {
        mInflater = LayoutInflater.from(context);
        this.layList = layList;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return layList.size();
    }

    @Override
    public Object getItem(int position) {
        return layList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.profile_view, parent, false);
            holder = new ViewHolder();
            holder.lay_seemore_btn = (RelativeLayout) view.findViewById(R.id.lay_seemore_btn);
            holder.txt_name = (TextView) view.findViewById(R.id.txt_name);
            holder.title_past = (TextView) view.findViewById(R.id.title_past);
            holder.txt_bio = (TextView) view.findViewById(R.id.txt_bio);
            holder.title_bio = (TextView) view.findViewById(R.id.title_bio);
            holder.txt_skills = (TextView) view.findViewById(R.id.txt_skills);
            holder.title_skills = (TextView) view.findViewById(R.id.title_skills);
            holder.txt_address = (TextView) view.findViewById(R.id.txt_address);
            holder.txt_seemore = (TextView) view.findViewById(R.id.txt_seemore);
            holder.ll_past_jobs = (LinearLayout) view.findViewById(R.id.ll_past_jobs);
            holder.ll_past_line = (LinearLayout) view.findViewById(R.id.ll_past_line);
            holder.progress = (ProgressBar) view.findViewById(R.id.progress);
            holder.img_confirm_profile = (ImageView) view.findViewById(R.id.img_confirm_profile);

            setFont();

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();

        }

        final ModelUserProfile profile = layList.get(position);
        holder.txt_name.setText(profile.name);
        holder.txt_bio.setText(profile.bio);
        holder.txt_skills.setText(profile.skills);
        holder.txt_address.setText(profile.address);

        if (profile.past_work.size() > 0) {
            holder.title_past.setVisibility(View.VISIBLE);
            holder.ll_past_line.setVisibility(View.VISIBLE);
            for (int i = 0; i < profile.past_work.size(); i++) {
                Log.i("Current Position", "" + profile.past_work.get(i).CurrentPosition);
                if (profile.past_work.get(i).CurrentPosition.equals("1")) {
                    current_role = "Current Role Since: "+profile.past_work.get(i).StartDate;
                    add_view(profile.past_work.get(i).JobTitle, profile.past_work.get(i).Company
                            , current_role, profile.past_work.get(i).City);
                } else if (profile.past_work.get(i).CurrentPosition.equals("0")) {
                    current_role = profile.past_work.get(i).StartDate + " - " + profile.past_work.get(i).EndDate;
                    add_view(profile.past_work.get(i).JobTitle, profile.past_work.get(i).Company
                            , current_role, profile.past_work.get(i).City);
                }
            }
        } else {
            holder.title_past.setVisibility(View.INVISIBLE);
            holder.ll_past_line.setVisibility(View.INVISIBLE);
        }

        holder.progress.setVisibility(View.VISIBLE);
        Picasso.with(ctx).load(profile.profile_image).noFade().placeholder(R.drawable.ic_user_new).
                transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                .into(holder.img_confirm_profile, new Callback() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                        holder.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub
                        holder.progress.setVisibility(View.GONE);
                    }
                });

        holder.lay_seemore_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ctx, Expand_Pending_Candidate.class);
                in.putExtra("name", profile.name);
                in.putExtra("address", profile.address);
                in.putExtra("bio", profile.bio);
                in.putExtra("skills", profile.skills);
                in.putExtra("profile_image", profile.profile_image);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(in);
            }
        });


        return view;
    }

    private class ViewHolder {
        RelativeLayout lay_seemore_btn;
        TextView txt_seemore, txt_name, txt_address, title_skills, txt_skills, title_bio, txt_bio, title_past;
        LinearLayout ll_past_jobs, ll_past_line;
        ImageView img_confirm_profile;
        ProgressBar progress;
    }

    //*************************************Setting Fonts for UI components**************************
    public void setFont() {
        holder.txt_seemore.setTypeface(Utils.setfontlight(ctx));
        holder.txt_name.setTypeface(Utils.setfontlight(ctx));
        holder.txt_address.setTypeface(Utils.setfontlight(ctx));
        holder.txt_bio.setTypeface(Utils.setfontlight(ctx));
        holder.txt_seemore.setTypeface(Utils.setfontlight(ctx));
        holder.txt_skills.setTypeface(Utils.setfontlight(ctx));
        holder.title_skills.setTypeface(Utils.setfontlight(ctx));
        holder.title_bio.setTypeface(Utils.setfontlight(ctx));
        holder.title_past.setTypeface(Utils.setfontlight(ctx));
    }

    //***************************************Add View Method****************************************
    public void add_view(String job_title, String company, String current, String location) {
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.past_job_info, null);
        final TextView txt_job_title = (TextView) view.findViewById(R.id.txt_job_title);
        final TextView txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);
        final TextView txt_current_role = (TextView) view.findViewById(R.id.txt_current_role);
        final TextView txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_job_title.setTypeface(Utils.setfontlight(ctx), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(ctx));
        txt_current_role.setTypeface(Utils.setfontlight(ctx));
        txt_location.setTypeface(Utils.setfontlight(ctx));

        final ModelUIPastJobs UIModel = new ModelUIPastJobs();
        UIModel.txt_job_title = txt_job_title;
        UIModel.txt_company_name = txt_company_name;
        UIModel.txt_current_role = txt_current_role;
        UIModel.txt_location = txt_location;

        UIModel.txt_job_title.setText(job_title);
        UIModel.txt_company_name.setText(company);
        UIModel.txt_current_role.setText(current);
        UIModel.txt_location.setText(location);

        holder.ll_past_jobs.addView(view);

    }

}
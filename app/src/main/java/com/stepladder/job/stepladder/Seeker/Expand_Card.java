package com.stepladder.job.stepladder.Seeker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 11/19/2015.
 */
public class Expand_Card extends ActionBarActivity {
    ImageView img_Employer;
    LinearLayout ll_contact_details, ll_job_details;
    TextView title, txt_company_name, title_business, txt_business, title_phone, txt_phone,
            title_email, txt_email, title_salary, txt_salary, title_length, txt_length, title_area, txt_area, title_contact, txt_contact, title_desc, txt_desc;
    ProgressBar progress;
    LayoutInflater inflater;
    Menu newmenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_card);

        init();

        setFonts();

        //Get Intent
        Intent intent = getIntent();
        title.setText(intent.getStringExtra("title"));
        DashBoard_Seeker.open = true;

        if (intent.getStringExtra("title").equals("Contact Details")) {
            ll_contact_details.setVisibility(View.VISIBLE);
            ll_job_details.setVisibility(View.GONE);
        } else if (intent.getStringExtra("title").equals("Job Details")) {
            ll_contact_details.setVisibility(View.GONE);
            ll_job_details.setVisibility(View.VISIBLE);
        }

        txt_company_name.setText(intent.getStringExtra("company_name"));
        progress.setVisibility(View.VISIBLE);
        Picasso.with(getApplicationContext()).load(intent.getStringExtra("job_image")).noFade().placeholder(R.drawable.ic_company).rotate(0f, 0f, 0f)
                .into(img_Employer, new Callback() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub
                        progress.setVisibility(View.GONE);
                    }
                });
        txt_business.setText(intent.getStringExtra("business_name"));
        txt_salary.setText(intent.getStringExtra("salary"));
        txt_length.setText(intent.getStringExtra("job_length"));
        txt_area.setText(intent.getStringExtra("location"));
        txt_contact.setText(intent.getStringExtra("contact"));
        txt_desc.setText(intent.getStringExtra("details"));
        txt_phone.setText(intent.getStringExtra("phone"));
        txt_email.setText(intent.getStringExtra("email"));
        txt_phone.setPaintFlags(txt_phone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txt_email.setPaintFlags(txt_email.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        clickevents();
    }

    //****************************************UI Initializations************************************
    private void init() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.set_title, null);
        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        title = (TextView) view.findViewById(R.id.action_title);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        title.setText("Job Details");
        img_Employer = (ImageView) findViewById(R.id.img_Employer);
        progress = (ProgressBar) findViewById(R.id.progress);
        txt_company_name = (TextView) findViewById(R.id.txt_company_name);
        title_business = (TextView) findViewById(R.id.title_business);
        txt_business = (TextView) findViewById(R.id.txt_business);
        title_salary = (TextView) findViewById(R.id.title_salary);
        txt_salary = (TextView) findViewById(R.id.txt_salary);
        title_length = (TextView) findViewById(R.id.title_length);
        txt_length = (TextView) findViewById(R.id.txt_length);
        title_area = (TextView) findViewById(R.id.title_area);
        txt_area = (TextView) findViewById(R.id.txt_area);
        title_contact = (TextView) findViewById(R.id.title_contact);
        txt_contact = (TextView) findViewById(R.id.txt_contact);
        title_desc = (TextView) findViewById(R.id.title_desc);
        txt_desc = (TextView) findViewById(R.id.txt_desc);
        title_phone = (TextView) findViewById(R.id.title_phone);
        txt_phone = (TextView) findViewById(R.id.txt_phone);
        title_email = (TextView) findViewById(R.id.title_email);
        txt_email = (TextView) findViewById(R.id.txt_email);
        ll_job_details = (LinearLayout) findViewById(R.id.ll_job_details);
        ll_contact_details = (LinearLayout) findViewById(R.id.ll_contact_details);
    }

    //**********************************************Set Fonts***************************************
    public void setFonts() {
        title.setTypeface(Utils.setfontlight(Expand_Card.this), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_business.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_business.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_salary.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_salary.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_length.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_length.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_area.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_area.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_contact.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_contact.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_desc.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_desc.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_phone.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_phone.setTypeface(Utils.setfontlight(Expand_Card.this));
        title_email.setTypeface(Utils.setfontlight(Expand_Card.this));
        txt_email.setTypeface(Utils.setfontlight(Expand_Card.this));
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        txt_phone.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    Intent callIntent = new Intent(
                            Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"
                            + txt_phone.getText().toString()));
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
            }
        });

        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {txt_email.getText().toString()};
                emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
                emailIntent.setType("text/html");
                startActivity(Intent.createChooser(emailIntent, "Send Mail..."));
            }
        });
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }


    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(Expand_Card.this, Manifest.permission.CALL_PHONE);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CALL_PHONE);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(Expand_Card.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            Intent callIntent = new Intent(
                    Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"
                    + txt_phone.getText().toString()));
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        Intent callIntent = new Intent(
                                Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"
                                + txt_phone.getText().toString()));
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(callIntent);
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to make call", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}


package com.stepladder.job.stepladder.Employeer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.Adapters.PendingListAdapter;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker_Fragments.DashboardSeekerFragment;
import com.stepladder.job.stepladder.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by user on 12/17/2015.
 */
public class Expand_Pending_Candidate extends ActionBarActivity {
    TextView txt_name, txt_address, title_skills, txt_skills, title_bio, txt_bio, title_past;
    LinearLayout ll_past_jobs, ll_past_line;
    ProgressBar progress;
    ActionBar actionBar;
    View actionview;
    ImageView img_back, img_cross, img_tick, img_confirm_profile;
    String name, address, bio, skills, profile_image, current_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_pending_candidate);

        init();

        setFonts();

        ActionBar();

        if (Build.VERSION.SDK_INT < 21) {

        } else {
            setupWindowAnimations();
        }

        //Get Intent
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        txt_name.setText(name);
        address = intent.getStringExtra("address");
        txt_address.setText(address);
        bio = intent.getStringExtra("bio");
        txt_bio.setText(bio);
        skills = intent.getStringExtra("skills");
        txt_skills.setText(skills);
        profile_image = intent.getStringExtra("profile_image");
        if (profile_image != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(profile_image).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        }

        if (PendingListAdapter.profile_list.get(0).past_work.size() > 0) {
            title_past.setVisibility(View.VISIBLE);
            ll_past_line.setVisibility(View.VISIBLE);
            for (int i = 0; i < PendingListAdapter.profile_list.get(0).past_work.size(); i++) {
                Log.i("Current Position", "" + PendingListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition);
                if (PendingListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition.equals("1")) {
                    current_role = "Current Role Since: "+PendingListAdapter.profile_list.get(0).past_work.get(i).StartDate;
                    add_view(PendingListAdapter.profile_list.get(0).past_work.get(i).JobTitle, PendingListAdapter.profile_list.get(0).past_work.get(i).Company
                            , current_role, PendingListAdapter.profile_list.get(0).past_work.get(i).City);
                } else if (PendingListAdapter.profile_list.get(0).past_work.get(i).CurrentPosition.equals("0")) {
                    current_role = PendingListAdapter.profile_list.get(0).past_work.get(i).StartDate + " - " + PendingListAdapter.profile_list.get(0).past_work.get(i).EndDate;
                    add_view(PendingListAdapter.profile_list.get(0).past_work.get(i).JobTitle, PendingListAdapter.profile_list.get(0).past_work.get(i).Company
                            , current_role, PendingListAdapter.profile_list.get(0).past_work.get(i).City);
                }
            }
        } else {
            title_past.setVisibility(View.INVISIBLE);
            ll_past_line.setVisibility(View.INVISIBLE);
        }

        clickevents();

    }

    //*****************************************UI Initialization************************************
    private void init() {
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_address = (TextView) findViewById(R.id.txt_address);
        txt_bio = (TextView) findViewById(R.id.txt_bio);
        txt_skills = (TextView) findViewById(R.id.txt_skills);
        title_bio = (TextView) findViewById(R.id.title_bio);
        title_past = (TextView) findViewById(R.id.title_past);
        title_skills = (TextView) findViewById(R.id.title_skills);
        ll_past_jobs = (LinearLayout) findViewById(R.id.ll_past_jobs);
        ll_past_line = (LinearLayout) findViewById(R.id.ll_past_line);
        img_confirm_profile = (ImageView) findViewById(R.id.img_confirm_profile);
        progress = (ProgressBar)findViewById(R.id.progress);
    }

    //***************************************Custom ActionBar***************************************
    private void ActionBar() {
        // TODO Auto-generated method stub
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionview = inflater.inflate(R.layout.action_bar_expand, null);
        img_back = (ImageView) actionview.findViewById(R.id.img_back);
        img_cross = (ImageView) actionview.findViewById(R.id.img_cross);
        img_tick = (ImageView) actionview.findViewById(R.id.img_tick);

        actionBar.setCustomView(actionview,
                new android.support.v7.app.ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        actionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    //****************************************Set Fonts Method**************************************
    private void setFonts() {
        txt_name.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        txt_skills.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        txt_bio.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        txt_address.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        title_skills.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        title_past.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        title_bio.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
    }

    //***************************************clickevents Method*************************************
    private void clickevents() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pending_Candidates.reject = true;
                finish();
            }
        });

        img_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pending_Candidates.accept = true;
                finish();
            }
        });
    }

    //***************************************Add View Method****************************************
    public void add_view(String job_title, String company, String current, String location) {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.past_job_info, null);
        final TextView txt_job_title = (TextView) view.findViewById(R.id.txt_job_title);
        final TextView txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);
        final TextView txt_current_role = (TextView) view.findViewById(R.id.txt_current_role);
        final TextView txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_job_title.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this), Typeface.BOLD);
        txt_company_name.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        txt_current_role.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));
        txt_location.setTypeface(Utils.setfontlight(Expand_Pending_Candidate.this));

        final ModelUIPastJobs UIModel = new ModelUIPastJobs();
        UIModel.txt_job_title = txt_job_title;
        UIModel.txt_company_name = txt_company_name;
        UIModel.txt_current_role = txt_current_role;
        UIModel.txt_location = txt_location;

        UIModel.txt_job_title.setText(job_title);
        UIModel.txt_company_name.setText(company);
        UIModel.txt_current_role.setText(current);
        UIModel.txt_location.setText(location);

        ll_past_jobs.addView(view);

    }

    //***************************************transitionTo Method************************************
    @SuppressLint("NewApi")
    private void setupWindowAnimations() {
        Transition transition;
        transition = buildEnterTransition();
        getWindow().setEnterTransition(transition);
    }

    @SuppressLint("NewApi")
    private Transition buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }
}

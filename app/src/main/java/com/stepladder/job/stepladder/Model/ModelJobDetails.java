package com.stepladder.job.stepladder.Model;

/**
 * Created by user on 12/18/2015.
 */
public class ModelJobDetails {
    public String job_id;
    public String job_title;
    public String job_desc;
    public String job_length;
    public String job_type;
    public String skills;
    public String min;
    public String max;
    public String job_status;
    public String job_image;
}

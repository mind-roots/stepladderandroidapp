package com.stepladder.job.stepladder.Seeker_Fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.stepladder.job.stepladder.Employeer.DashBoard_Employeer;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Utils.Utils;

public class NotificationFragment extends Fragment {
    View rootView;
    TextView txt_appnoti, txt_emailnoti, txt_jobs, txt_app, txt_email, txt_matches;
    SharedPreferences prefs;
    public static String email_noti = "yes", app_noti = "yes";
    Switch switch_email, switch_app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.employee_notification_fragment, container, false);

        init();
        setFonts();
        DashBoard_Seeker.open = true;
        DashBoard_Seeker.isHome = false;
       // DashBoard_Seeker.newmenu.findItem(R.id.action_update).setVisible(true);
        DashBoard_Seeker.ic_noti_grey.setVisibility(View.GONE);
        DashBoard_Seeker.ll_badge.setVisibility(View.GONE);

        //Get Prefs Values
        if (prefs.getString("email_noti", null) != null) {
            if (prefs.getString("email_noti", null).equals("yes")) {
                switch_email.setChecked(true);
            } else if (prefs.getString("email_noti", null).equals("no")) {
                switch_email.setChecked(false);
            }
        }
        if (prefs.getString("app_noti", null) != null) {
            if (prefs.getString("app_noti", null).equals("yes")) {
                switch_app.setChecked(true);
            } else if (prefs.getString("app_noti", null).equals("no")) {
                switch_app.setChecked(false);
            }
        }


        clickevents();

        return rootView;
    }

    //********************************************UI Initialization*********************************
    private void init() {
        txt_app = (TextView) rootView.findViewById(R.id.txt_app);
        txt_appnoti = (TextView) rootView.findViewById(R.id.txt_appnoti);
        txt_emailnoti = (TextView) rootView.findViewById(R.id.txt_emailnoti);
        txt_jobs = (TextView) rootView.findViewById(R.id.txt_jobs);
        txt_email = (TextView) rootView.findViewById(R.id.txt_email);
        txt_matches = (TextView) rootView.findViewById(R.id.txt_matches);
        prefs = getActivity().getSharedPreferences("user_data", getActivity().MODE_PRIVATE);
        switch_email = (Switch) rootView.findViewById(R.id.switch_email);
        switch_app = (Switch) rootView.findViewById(R.id.switch_app);
    }

    //********************************************Set Fonts*****************************************
    public void setFonts() {
        txt_email.setTypeface(Utils.setfontlight(getActivity()));
        txt_app.setTypeface(Utils.setfontlight(getActivity()));
        txt_appnoti.setTypeface(Utils.setfontlight(getActivity()));
        txt_emailnoti.setTypeface(Utils.setfontlight(getActivity()));
        txt_jobs.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
        txt_matches.setTypeface(Utils.setfontlight(getActivity()), Typeface.BOLD);
    }

    @Override
    public void onResume() {
        super.onResume();
        DashBoard_Seeker.open = true;
    }
    //********************************************Clickevents***************************************
    private void clickevents() {
        switch_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    email_noti = "yes";
                } else {
                    email_noti = "no";
                }
            }
        });

        switch_app.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app_noti = "yes";
                } else {
                    app_noti = "no";
                }
            }
        });

    }
}

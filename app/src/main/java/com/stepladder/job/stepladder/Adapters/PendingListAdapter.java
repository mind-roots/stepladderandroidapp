package com.stepladder.job.stepladder.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.Employeer.Job_Status;
import com.stepladder.job.stepladder.Employeer.Pending_Candidates;
import com.stepladder.job.stepladder.Model.CandidatesModel;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.ModelUIData;
import com.stepladder.job.stepladder.Model.ModelUIPastJobs;
import com.stepladder.job.stepladder.Model.ModelUserProfile;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 11/19/2015.
 */
public class PendingListAdapter extends RecyclerView.Adapter<PendingListAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    static List<CandidatesModel> data = Collections.emptyList();
    public static ArrayList<ModelUserProfile> profile_list = new ArrayList<>();
    Context context;
    String currentdate, lastday, status;
    RestClient client;
    SharedPreferences prefs;
    ConnectionDetector conn;

    public PendingListAdapter(Context con, ArrayList<CandidatesModel> list) {
        // TODO Auto-generated constructor stub
        this.context = con;
        inflater = LayoutInflater.from(con);
        this.data = list;
        prefs = context.getSharedPreferences("user_data", context.MODE_PRIVATE);
        conn = new ConnectionDetector(context);
    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.item, parent, false);

        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        public TextView name;
        public TextView address;
        public ImageView img_photo;
        public TextView time;
        public ProgressBar progressBar, progress;
        LinearLayout list_layout;

        public MyViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            name = (TextView) itemLayoutView.findViewById(R.id.txt_headline);
            address = (TextView) itemLayoutView.findViewById(R.id.txt_desc);
            time = (TextView) itemLayoutView.findViewById(R.id.txt_time);
            list_layout = (LinearLayout) itemLayoutView
                    .findViewById(R.id.list_layout);
            progressBar = (ProgressBar) itemLayoutView.findViewById(R.id.progressBar);
            progress = (ProgressBar) itemLayoutView.findViewById(R.id.progress);
            img_photo = (ImageView) itemLayoutView.findViewById(R.id.img_icon);

        }

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // TODO Auto-generated method stub
        final CandidatesModel item = data.get(position);
        holder.name.setText(item.name);
        holder.address.setText(item.address);
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date,today,get_date;
        int day1,day2;
        try {
            System.out.println("Get time: " + item.created_at);
            date = inFormat.parse(item.created_at);
            System.out.println("Get date: "
                    + date);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date);
            day2 = cal1.get(Calendar.DAY_OF_MONTH);
            Log.i("Day of date: ",""+day2);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm a");
            Calendar cal = Calendar.getInstance();
            today = cal.getTime();
            System.out.println("Current date: "
                    + today);
            currentdate = dateFormat.format(cal.getTime());
            day1 = cal.get(Calendar.DAY_OF_MONTH);
            Log.i("Day of month: ", "" + day1);
            SimpleDateFormat outFormat = new SimpleDateFormat("EEE");
            if(day1 == day2){
                SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                get_date = dFormat.parse(item.created_at);
                lastday = printFormat.format(get_date);
                Log.i("If today: ",""+lastday);
            }else if(day1-day2 == 1){
                lastday = "yesterday";
            }else {
                lastday = outFormat.format(date);
                System.out.println("Day of week: " + lastday);
            }
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        holder.time.setText(lastday);
        holder.name.setTypeface(Utils.setfontlight(context));
        holder.address.setTypeface(Utils.setfontlight(context));
        holder.time.setTypeface(Utils.setfontlight(context));

        //Set Profile Image
        //holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context).load(item.user_image).noFade().placeholder(R.drawable.ic_user_new).rotate(0f, 0f, 0f)
                .into(holder.img_photo, new Callback() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub
                       // holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub
                        //holder.progressBar.setVisibility(View.GONE);
                    }
                });

        holder.list_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!conn.isConnectingToInternet()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            context);

                    alert.setTitle("Internet connection unavailable.");
                    alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    context.startActivity(new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            });

                    alert.show();
                } else {
                    Log.i("Id: ", "" + item.user_id);
                    profile_list.clear();
                    new get_profile(item.user_id).execute();
                }
            }
        });

    }

    //*******************************************Get User Profile***********************************

    public class get_profile extends AsyncTask<Void, Void, Void> {
        String user_id;

        public get_profile(String user_id) {
            this.user_id = user_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Job_Status.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = getProfile(prefs.getString("auth_code", null), user_id);

            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                JSONObject data = obj.getJSONObject("data");
                ModelUserProfile profile = new ModelUserProfile();
                String fname = data.getString("FirstName");
                String lname = data.getString("LastName");
                profile.name = fname + " " + lname;
                profile.bio = data.optString("Biography");
                profile.skills = data.optString("Skills");
                profile.profile_image = data.optString("profile_image");
                profile.address = data.optString("LocationName");

                JSONArray past_work = data.optJSONArray("userspastworks");

                if (past_work.length() != 0) {
                    profile.past_work.clear();
                    for (int i = 0; i < past_work.length(); i++) {
                        JSONObject jobj = past_work.getJSONObject(i);
                        ModelUIData job_data = new ModelUIData();
                        job_data.JobTitle = jobj.optString("JobTitle");
                        job_data.Company = jobj.optString("Company");
                        job_data.City = jobj.optString("City");
                        job_data.CurrentPosition = jobj.optString("CurrentPosition");
                        job_data.StartDate = jobj.optString("StartDate");
                        job_data.EndDate = jobj.optString("EndDate");
                        profile.past_work.add(job_data);
                        Log.i("List Size: ","Past Job"+profile.past_work.size());
                    }
                }

                profile_list.add(profile);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Job_Status.progressBar.setVisibility(View.GONE);
            Intent intent = new Intent(context, Pending_Candidates.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("user_id",user_id);
            context.startActivity(intent);
        }
    }

    //*******************************************Profile Method*************************************
    public String getProfile(String auth_code, String user_id) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", auth_code)
                .appendQueryParameter("UserId", user_id);
        Log.i("Complete", "Url: " + Utils.get_user_profile + builder);
        client = new RestClient(Utils.get_user_profile + builder);
        String response = null;
        response = client.executeGet();
        return response;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}

package com.stepladder.job.stepladder.Seeker_Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.stepladder.job.stepladder.R;

/**
 * Created by user on 10/23/2015.
 */
public class ItemFragment extends Fragment{
    View rootView;
    boolean mShowingBack = false;
    LinearLayout ll_contact;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.seeker_cardview, container, false);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new CardFrontFragment()).commit();
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        ll_contact = (LinearLayout)rootView.findViewById(R.id.ll_contact);
        ll_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  flipcard();
            }
        });

        return rootView;
    }
    @SuppressWarnings("ResourceType")
    private void flipcard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }

        // Flip to the back.


        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment
        // for the back of
        // the card, uses custom animations, and is part of the fragment
        // manager's back stack.

        getFragmentManager().beginTransaction()

                // Replace the default fragment animations with animator resources
                // representing
                // rotations when switching to the back of the card, as well as animator
                // resources representing rotations when flipping back to the front
                // (e.g. when
                // the system Back button is pressed).
                .setCustomAnimations(R.anim.card_flip_right_in,
                        R.anim.card_flip_right_out,
                        R.anim.card_flip_left_in,
                        R.anim.card_flip_left_out)

                        // Replace any fragments currently in the container view with a
                        // fragment
                        // representing the next page (indicated by the just-incremented
                        // currentPage
                        // variable).
                .replace(R.id.container, new CardBackFragment())

                        // Add this transaction to the back stack, allowing users to
                        // press Back
                        // to get to the front of the card.
                .addToBackStack(null)

                        // Commit the transaction.
                .commit();

        // Defer an invalidation of the options menu (on modern devices, the
        // action bar). This
        // can't be done immediately because the transaction may not yet be
        // committed. Commits
        // are asynchronous in that they are posted to the main thread's message
        // loop.

    }
    public static class CardFrontFragment extends Fragment {
        public CardFrontFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.seeker_cardview, container,
                    false);
        }
    }

    public static class CardBackFragment extends Fragment {
        public CardBackFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.job_contact_info, container,
                    false);
        }
    }
}

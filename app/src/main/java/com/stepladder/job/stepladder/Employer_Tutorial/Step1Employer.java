package com.stepladder.job.stepladder.Employer_Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.R;

/**
 * Created by user on 1/18/2016.
 */
public class Step1Employer extends Activity implements Animation.AnimationListener{
    ImageView img_leads;
    Animation animZoomIn;
    public static String replay = "no";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step1employer);

        img_leads = (ImageView)findViewById(R.id.img_leads);

        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_in);
        animZoomIn.setAnimationListener(this);
        img_leads.startAnimation(animZoomIn);

        img_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Step1Employer.this,Step2Employer.class);
                //ActivityStack.activity.add(Step1Employer.this);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });


    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

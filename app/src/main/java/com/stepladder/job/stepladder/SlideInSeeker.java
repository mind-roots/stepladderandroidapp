package com.stepladder.job.stepladder;


import android.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepladder.job.stepladder.Model.ConnectionDetector;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.Model.RoundedTransformation;
import com.stepladder.job.stepladder.Seeker.DashBoard_Seeker;
import com.stepladder.job.stepladder.Seeker.Profile_photo;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SlideInSeeker extends Activity {
    ImageView cross, img_pick;
    ImageView img_confirm_profile;
    LinearLayout ll_contact_us, ll_share_stepladder, ll_settings, ll_notification, ll_view_matches,
            ll_edit_profile, ll_home_seeker,ll_report;
    TextView txt_contact, txt_share, txt_setting, txt_noti, txt_edit, txt_view, txt_home, txt_username,txt_report;
    ProgressBar progress;
    String status, profile_image;
    ConnectionDetector con;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_in_seeker);

        init();

        setFont();

        if (MainActivity.profile != null) {
            progress.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(MainActivity.profile).noFade().placeholder(R.drawable.ic_user_new).
                    transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                    .into(img_confirm_profile, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub
                            progress.setVisibility(View.GONE);
                        }
                    });
        } else if (prefs.getString("profile_image", null) != null) {
            if (prefs.getString("profile_image", null).contains("default-profile")) {

            } else {
                progress.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(prefs.getString("profile_image", null)).noFade().placeholder(R.drawable.ic_user_new).
                        transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                        .into(img_confirm_profile, new Callback() {

                            @Override
                            public void onSuccess() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                            }
                        });
            }
        } else {

        }

        if (prefs.getString("first_name", null) != null && prefs.getString("last_name", null) != null) {
            txt_username.setText(prefs.getString("first_name", null) + " " + prefs.getString("last_name", null));
        }

        clickevents();
    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        // TODO Auto-generated method stub

        ll_home_seeker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent();
                in.putExtra("fragment_name", "home");
                setResult(RESULT_OK, in);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        cross.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DashBoard_Seeker.open = true;
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        ll_view_matches.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent();
                in.putExtra("fragment_name", "view_jobs");
                setResult(RESULT_OK, in);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ll_edit_profile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent();
                in.putExtra("fragment_name", "edit_profile");
                setResult(RESULT_OK, in);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ll_notification.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent();
                in.putExtra("fragment_name", "notification");
                setResult(RESULT_OK, in);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ll_settings.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent();
                in.putExtra("fragment_name", "settings");
                setResult(RESULT_OK, in);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ll_share_stepladder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Check out the Step Ladder App. It is like Tinder, but for jobs! Download it for your mobile here: https://stepladderapp.com/");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        ll_contact_us.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                startActivity(Intent.createChooser(emailIntent, "Contact us via"));
            }
        });


        img_pick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        img_confirm_profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

    }

    //****************************************UI Initializations************************************
    private void init() {
        // TODO Auto-generated method stub
        con = new ConnectionDetector(SlideInSeeker.this);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        progress = (ProgressBar) findViewById(R.id.progress);
        cross = (ImageView) findViewById(R.id.btn_cross);
        ll_contact_us = (LinearLayout) findViewById(R.id.ll_contact_us);
        ll_share_stepladder = (LinearLayout) findViewById(R.id.ll_share_stepladder);
        ll_settings = (LinearLayout) findViewById(R.id.ll_settings);
        ll_notification = (LinearLayout) findViewById(R.id.ll_notification);
        ll_view_matches = (LinearLayout) findViewById(R.id.ll_view_matches);
        ll_edit_profile = (LinearLayout) findViewById(R.id.ll_edit_profile);
        img_confirm_profile = (ImageView) findViewById(R.id.img_confirm_profile);
        img_pick = (ImageView) findViewById(R.id.image_pick);
        ll_home_seeker = (LinearLayout) findViewById(R.id.ll_home_seeker);
        txt_contact = (TextView) findViewById(R.id.txt_contact);
        txt_edit = (TextView) findViewById(R.id.txt_edit);
        txt_home = (TextView) findViewById(R.id.txt_home);
        txt_noti = (TextView) findViewById(R.id.txt_noti);
        txt_setting = (TextView) findViewById(R.id.txt_setting);
        txt_share = (TextView) findViewById(R.id.txt_share);
        txt_username = (TextView) findViewById(R.id.txt_username);
        txt_view = (TextView) findViewById(R.id.txt_view);
    }

    //***********************************Setting Fonts for UI components****************************
    public void setFont() {
        txt_view.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_username.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_share.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_setting.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_contact.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_edit.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_home.setTypeface(Utils.setfontlight(SlideInSeeker.this));
        txt_noti.setTypeface(Utils.setfontlight(SlideInSeeker.this));
    }
    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Profile_photo.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Profile_photo.selected_image);
            Intent in = new Intent(SlideInSeeker.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            //Connection Detection
            if (!con.isConnectingToInternet()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        SlideInSeeker.this);

                alert.setTitle("Internet connection unavailable.");
                alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
                alert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                alert.show();
            } else {
                new Change_Image().execute();
            }
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    // **************************************Change Profile Photo***********************************
    class Change_Image extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            RestClient client = new RestClient(Utils.profile_image + "AuthCode=" + prefs.getString("auth_code", null));
            Log.i("Image", "Path:" + Utils.pimageUri);
            String image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
            String response = client.postimage(image_path);
            Log.i("Response:", "" + response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.optString("status");
                Log.i("Status:", "" + status);
                JSONObject data = obj.getJSONObject("data");
                profile_image = data.optString("profile_image");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (status.equals("true")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("profile_image", profile_image);
                editor.commit();
                Picasso.with(getApplicationContext()).load(Utils.pimageUri).noFade().placeholder(R.drawable.ic_user_new).
                        transform(new RoundedTransformation(2)).rotate(0f, 0f, 0f)
                        .into(img_confirm_profile, new Callback() {

                            @Override
                            public void onSuccess() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub
                                progress.setVisibility(View.GONE);
                            }
                        });
            } else {
                progress.setVisibility(View.GONE);
                dialog("Alert!", "Error in uploading image.");
            }
        }
    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new android.app.AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**************************************************
     * Allow Permissions
     ***********************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(SlideInSeeker.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(SlideInSeeker.this, android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add(android.Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(SlideInSeeker.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Toast.makeText(getApplicationContext(), "You are not allowed to take image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}

package com.stepladder.job.stepladder.Employeer;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stepladder.job.stepladder.ActivityStack;
import com.stepladder.job.stepladder.Employeer_Fragments.ContactUsFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.DashboardEmployeerFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.EditEmployeerProfileFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.EmployeerNotificationFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.EmployeerSettingsFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.PostJobFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.ShareStepLadderFragment;
import com.stepladder.job.stepladder.Employeer_Fragments.ViewCurrentJobsFragment;
import com.stepladder.job.stepladder.Model.RestClient;
import com.stepladder.job.stepladder.OneSignalClass;
import com.stepladder.job.stepladder.R;
import com.stepladder.job.stepladder.Seeker.Location;
import com.stepladder.job.stepladder.SlideInEmployeer;
import com.stepladder.job.stepladder.Utils.Utils;

import org.json.JSONObject;

public class DashBoard_Employeer extends ActionBarActivity {
    ActionBar actionBar;
    View actionview;
    public static ImageView ic_drawer, ic_noti_grey, ic_add_job;
    public static RelativeLayout ll_badge;
    TextView action_title;
    Fragment fragment;
    public static Menu newmenu;
    RestClient client;
    String status, email_noti, app_noti;
    public static boolean replace = false;
    public static boolean open = false;
    SharedPreferences prefs;
    public static ProgressDialog dia;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_employeer);

        ActionBar();

        fragment = new ViewCurrentJobsFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);

        clickevents();

    }

    //*******************************************clickevents****************************************
    private void clickevents() {
        // TODO Auto-generated method stub
        ic_drawer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                replace = true;
                Intent in = new Intent(DashBoard_Employeer.this, SlideInEmployeer.class);
                startActivityForResult(in, 1);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });

        ic_add_job.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                open = true;
                replace = true;
                Utils.cimageUri = null;
                Utils.userlist.clear();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("jobtitle", null);
                editor.putString("jobdescription", null);
                editor.putString("jobduration", null);
                editor.putString("from", null);
                editor.putString("to", null);
                editor.putString("job_type", null);
                editor.putString("job_pos", null);
                editor.commit();
                Intent intent = new Intent(DashBoard_Employeer.this, Job_Details.class);
                ActivityStack.activity.add(DashBoard_Employeer.this);
                startActivity(intent);
            }
        });
    }

    //****************************************ActionBar Method**************************************
    private void ActionBar() {
        // TODO Auto-generated method stub
        dia = new ProgressDialog(DashBoard_Employeer.this);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionview = inflater.inflate(R.layout.action_bar, null);
        ic_drawer = (ImageView) actionview.findViewById(R.id.ic_drawer);
        ic_add_job = (ImageView) actionview.findViewById(R.id.ic_add_job);
        ic_noti_grey = (ImageView) actionview.findViewById(R.id.ic_noti_grey);
        ll_badge = (RelativeLayout) actionview.findViewById(R.id.ll_badge);
        ic_noti_grey.setVisibility(View.GONE);
        ll_badge.setVisibility(View.GONE);
        action_title = (TextView) actionview.findViewById(R.id.action_title);
        action_title.setTypeface(Utils.setfontlight(DashBoard_Employeer.this), Typeface.BOLD);
        actionBar.setCustomView(actionview,
                new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            ContentValues cv = new ContentValues();
            cv.put("badgecount", 0);
            getContentResolver().update(Uri.parse("content://com.sec.badge/apps"), cv, "package=?", new String[]{getPackageName()});

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notifManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**************************************onActivityResult****************************************
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String fragment_name = data.getStringExtra("fragment_name");
            System.out.println("fragment_name: " + fragment_name);

            if (fragment_name.equals("view_jobs")) {
                newmenu.findItem(R.id.action_update).setVisible(false);
                action_title.setText("Current Jobs");
                fragment = new ViewCurrentJobsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("post_jobs")) {
                open = true;
                Utils.cimageUri = null;
                Utils.userlist.clear();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("jobtitle", null);
                editor.putString("jobdescription", null);
                editor.putString("jobduration", null);
                editor.putString("from", null);
                editor.putString("to", null);
                editor.putString("job_type", null);
                editor.commit();
                Intent intent = new Intent(DashBoard_Employeer.this, Job_Details.class);
                ActivityStack.activity.add(DashBoard_Employeer.this);
                startActivity(intent);
            } else if (fragment_name.equals("notification")) {
                action_title.setText("Notifications");
                fragment = new EmployeerNotificationFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("edit_employeer")) {
                action_title.setText("Edit Employer Profile");
                fragment = new EditEmployeerProfileFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("settings")) {
                action_title.setText("Settings");
                fragment = new EmployeerSettingsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else if (fragment_name.equals("share_ladder")) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "I just posted a job on the Step Ladder App. It is like Tinder, but for jobs! Download it for your mobile here: https://stepladderapp.com/");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else if (fragment_name.equals("contact_us")) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                startActivity(Intent.createChooser(emailIntent, "Contact us via"));
            } else if (fragment_name.equals("home")) {
                action_title.setText("Step Ladder");
                fragment = new DashboardEmployeerFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.skip_menu, newmenu);
        newmenu.findItem(R.id.action_expand).setVisible(false);
        newmenu.findItem(R.id.action_skip).setVisible(false);
        newmenu.findItem(R.id.action_done).setVisible(false);
        newmenu.findItem(R.id.action_update).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_update) {
            new update_noti().execute();
        }
        return false;
    }

    //*************************************Async Task Class*****************************************
    class update_noti extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating notification....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("Values: ", "Authcode: " + prefs.getString("auth_code", null)
                    + "Email: " + EmployeerNotificationFragment.email_noti + "App: " + EmployeerNotificationFragment.app_noti);
            String response = update(prefs.getString("auth_code", null), EmployeerNotificationFragment.email_noti, EmployeerNotificationFragment.app_noti);
            Log.i("Response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");
                if (status.equalsIgnoreCase("true")) {
                    JSONObject data = obj.getJSONObject("data");
                    email_noti = data.getString("Match_Email_Notification");
                    app_noti = data.getString("Match_App_Notification");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equalsIgnoreCase("true")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("email_noti", email_noti);
                editor.putString("app_noti", app_noti);
                editor.commit();
            } else {
                dialog("Alert!", "Couldn't update notification");
            }
        }
    }


    //***********************************Update Method**********************************************
    public String update(String authcode, String email_noti, String app_noti) {
        Uri.Builder builder = new Uri.Builder().appendQueryParameter(
                "AuthCode", authcode)
                .appendQueryParameter("Match_Email_Notification", email_noti)
                .appendQueryParameter("Match_App_Notification", app_noti);
        Log.i("Complete", "Url: " + Utils.update_notification + builder);
        client = new RestClient(Utils.update_notification + builder);
        String response = null;
        response = client.executePutMethod();
        return response;
    }

    //***********************************Back Press Method******************************************
    @Override
    public void onBackPressed() {
        exitdialog();
    }

    //************************************************Back Dialog***********************************
    private void exitdialog() {
        // TODO Auto-generated method stub

        AlertDialog.Builder alert = new AlertDialog.Builder(DashBoard_Employeer.this);

        alert.setTitle("Alert!");
        alert.setMessage("Are you sure you want to exit from this app?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                System.exit(0);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        alert.show();

    }

    //*************************************Alert Dialog*********************************************
    public void dialog(String title, String msg) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }
}
